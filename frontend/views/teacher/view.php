<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = '查看讲师';
$this->params['breadcrumbs'][] = ['label' => '讲师管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-view">

    <p>
        <?= Html::a('返回', Url::to(['/teacher/index']), ['class' => 'btn btn-primary']) ?>
    </p>
	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          	'teacherName',
            'companyName',
            'title',
            'intro',
        	[
        				'label'=>'头像',
        				'format'=>'html',
        				'value'=>function($model){
        				if(isset($model['avatar'])) {
        					return Html::img($model->avatar->attachFullPath,['style'=>'heigth:100px;width:100px;']);
        				}
    			}
    		],
        ]
    ]) ?>
</div>
