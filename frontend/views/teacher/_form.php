<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-from">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teacherName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'companyName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'intro')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'avatarId')->hiddenInput()->hint('建议尺寸：200*200像素') ?>

	<?php 
		echo FileInput::widget([
				'name' => 'uploadfile',
				'options'=>[
						'multiple'=>false,
						'accept' => 'image/*'
				],
				'pluginOptions' => [
						'showClose' => false,
						'initialPreview'=>[
								isset($model->avatar) ? $model->avatar->attachFullPath : null
						],
						'initialPreviewAsData'=>true,
						'initialPreviewShowDelete'=>false,
						'initialCaption'=> isset($model->avatar) ? $model->avatar->orgFileName : null,
						'initialPreviewConfig' => [
								['caption' => isset($model->avatar) ? $model->avatar->orgFileName : null, 'size' => isset($model->avatar) ? $model->avatar->attachSize * 1024 *1024 : null],
						],
						'uploadUrl' => Url::toRoute(['course-main/upload']),
						'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
						'dropZoneTitle' => '请上传图片',
						'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
						'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
				],
				'pluginEvents' => [
						'fileuploaded' => "function(event, data, previewId, index) {
											$('#teachersearch-avatarid').val(data.response['id']);
		 								}",
						'fileremoved' => "function(event, data, previewId, index) {
											$('#teachersearch-avatarid').val('');
		 								}",
						'change' => "function(event, data, previewId, index) {
											$('#teachersearch-avatarid').val('');
		 								}",
				],
		]);
	?>
	
	<br>
    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
