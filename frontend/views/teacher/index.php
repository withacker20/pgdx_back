<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = '讲师管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('新增讲师', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				
    				[
    						'header'=>'讲师姓名',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return $model['teacherName'];
                            },
                            
    				],
    				[
    						'attribute' => '公司',
    						'content' => function($model){
    							return Html::tag('span',Html::encode($model['companyName']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
    				[
    						'attribute' => '职务',
    						'content' => function($model){
    							return Html::tag('span',Html::encode($model['title']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
    				[
    						'attribute' => '介绍',
    						'content' => function($model){
    							return Html::tag('span',Html::encode($model['intro']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
    				[
    						'attribute' => '录入时间',
    						'value' => function($model){
    						if(!is_null($model['createTime'])){
	    							return date('Y-m-d H:i:s',$model['createTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                            'options' => [
                            		'width' => '25%'
                            ]
                    ],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{update}',
    					'buttons' => [
    						'update' => function ($url, $model, $key) {
    						return Html::a('修改', ['update', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
    						}
    					],
    					'options' => [
    							'width' => 5
    					]
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{delete}',
    					'buttons' => [
    							'delete' => function ($url, $model, $key) {
    								return Html::a('删除', ['delete', 'id' => $model['id']], 
    								['class'=>'btn btn-sm btn-danger','data' => [
	    								'confirm' => '你确定删除吗?',
	    								'method' => 'post',
    									],
    								]);
    								}
    					],
    					'options' => [
    						'width' => 5
    						]
    				]
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
