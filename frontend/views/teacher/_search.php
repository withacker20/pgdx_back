<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-search">

    <?php $form = ActiveForm::begin(['id'=>'teacher-form',
        'action' => ['index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>录入时间</label>';
		echo DatePicker::widget([
			'name' => 'TeacherSearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'TeacherSearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'teacherName') ?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("teacher-form");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
