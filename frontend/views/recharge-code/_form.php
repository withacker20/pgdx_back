<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rechargeCode-from">

   <?php $form = ActiveForm::begin(['validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

    <?= $form->field($model, 'codeCount')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

	<?= $form->field($model, 'expireDate')->widget(DatePicker::classname(), [ 
		'options' => ['placeholder' => ''], 
		'pluginOptions' => [ 
			'autoclose' => true, 
			'todayHighlight' => true, 
			'format' => 'yyyy-mm-dd', 
		] 
	])->hint("有效期当日23:59:59后失效"); ?>
	
	<br>
    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
		<?= Html::a('取消', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
