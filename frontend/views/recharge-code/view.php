<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = '查看充值码';
$this->params['breadcrumbs'][] = ['label' => '充值码管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rechargecode-view">

    <p>
        <?= Html::a('返回', Url::to(['/recharge-code/index']), ['class' => 'btn btn-primary']) ?>
    </p>
	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          	'amount',
            'codeCount',
            [
                'label' => '有效期(当日23:59:59失效)',
                'value' => function ($model) {
                    if (isset($model['expireDate'])) {
                        return date('Y-m-d', $model['expireDate'] / 1000);
                    } else {
                        return "";
                    }
                }
            ],
        ]
    ]) ?>
</div>
