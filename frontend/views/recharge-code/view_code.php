<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = '查看充值码';
$this->params['breadcrumbs'][] = ['label' => '充值码管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rechargecode-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rechargeCode',
            [
                'label' => '创建时间',
                'value' => function ($model) {
                    if (isset($model['createTime'])) {
                        return date('Y-m-d H:i:s', $model['createTime'] / 1000);
                    } else {
                        return "";
                    }
                }
            ],
          	'id',
          	'amount',
            [
                'label' => '有效期(当日23:59:59失效)',
                'value' => function ($model) {
                    if (isset($model['expireDate'])) {
                        return date('Y-m-d', $model['expireDate'] / 1000);
                    } else {
                        return "";
                    }
                }
            ],
            [
                'label' => '当前状态',
                'value' => function ($model) {
                    if ($model['codeStatus'] == '1') {
                        return '已核销' . '(' . date('Y-m-d H:i:s', $model['verifyTime'] / 1000) . " 用户: " . $model['username'] . ")";
                    } else if ($model['codeStatus'] == '2') {
                        return '已作废';
                    } else if ($model['expireDate'] / 1000 > time()){
                        return "有效期内";
                    } else {
                        return "已过期";
                    }
                }
            ],
        ]
    ]) ?>
</div>
