<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rechargecode-search">

    <?php $form = ActiveForm::begin(['id'=>'rechargecode-form',
        'action' => ['index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

    <?= $form->field($model, 'rechargeCode') ?>

    <div class="form-group">
        <?= Html::Button('查询使用记录', ['onclick'=>'searchCode();','class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("rechargecode-form");']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	<?php
		Modal::begin([
			'id' => 'lookupModal',
			'header' => '<h4 class="modal-title">充值码使用记录</h4>',
		]);

		Modal::end();
	?>
</div>
<?php $this->beginBlock('javascript-block') ?>

function searchCode() {
	var value = $('#rechargecodesearch-rechargecode').val();
    if (value == '') {
		alert('请输入充值码');
	} else {
		$.get('<?=Url::toRoute(['recharge-code/view-code']) ?>', {code:value},
			function (data) {
				$('#lookupModal').find('.modal-body').html(data);
				$('#lookupModal').modal('show');
			}
		);
	}
}
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
