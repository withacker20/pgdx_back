<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = '充值码管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rechargeCode-index">

    <?php Pjax::begin(); ?>
	<?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('创建充值码', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
					[
						'attribute'=>'批次',
						'value' => function($model){    						
							return $model['id'];
						},
						
					],
					[
							'attribute' => '创建时间',
							'value' => function($model){
							if(!empty($model['createTime'])){
									return date('Y-m-d H:i:s',$model['createTime']/1000);
								}else{
									return '';
								}
							},
					],
				
    				[
    						'attribute'=>'总份数',
    						'value' => function($model){    						
    							return $model['codeCount'];
                            },
                            
    				],
    				[
    						'attribute' => '已核销份数',
    						'value' => function($model){
    							return $model['verifyCount'];
                            },
                    ],
    				[
    						'attribute' => '每份面额(元)',
    						'value' => function($model){
    							return $model['amount'];
                            },
                    ],
    				[
    						'attribute' => '预算总额(元)',
    						'value' => function($model){
    							return $model['amount'] * $model['codeCount'];
                            },
                    ],
    				[
    						'attribute' => '有效期(当日23:59:59失效)',
    						'value' => function($model){
    							if(!empty($model['expireDate'])){
	    							return date('Y-m-d',$model['expireDate']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                    ],
    				[
						'header'=>'操作',
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{cancel}&nbsp;{download}',
    					'buttons' => [
    						'cancel' => function ($url, $model, $key) {
								if($model['batchStatus'] == '1'){
									return "已作废";
								} else if(!empty($model['expireDate'])){
									if ($model['expireDate']/1000 > time()) {
										return Html::a('作废', ['cancel', 'id' => $model['id']],
										['class' => 'btn btn-sm btn-danger', 'data' => [
											'confirm' => '作废充值码将导致本批次所有充值码都无效，如果活动已经开启，将造成极大影响！点击确定，将该批次充值码作废',
											'method' => 'post',
										],
										]);
									} else {
										return '已过期';
									}
								} else {
									return '';
								}
							},
							'download' => function ($url, $model, $key) {
									if($model['batchStatus'] == '0' && !empty($model['expireDate']) && $model['expireDate']/1000 > time()){
											return Html::a('下载', ['download', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
									} 								
								}
								
    					],
    				],
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
