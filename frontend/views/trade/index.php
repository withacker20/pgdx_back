<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = '交易流水';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trade-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				
    				[
    						'header'=>'交易号',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return $model['tradeNumber'];
                            },
                            
    				],
    				[
    						'attribute' => '产品名称',
    						'content' => function($model){
    							return Html::tag('span',Html::encode($model['productName']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
    				[
    						'attribute' => '金额',
    						'content' => function($model){
    							return $model['tradeAmount'];
                            },
                    ],
    				[
    						'attribute' => '购买时间',
    						'value' => function($model){
    						if(!empty($model['createTime'])){
	    							return date('Y-m-d H:i:s',$model['createTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                    ],
                    [
                    		'attribute' => '用户昵称',
                    		'content' => function($model){
                    			return Html::tag('span',$model['username'],['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
                    [
                    		'attribute' => '手机号',
                    		'content' => function($model){
                    			return Html::tag('span',$model['phoneNumber'],['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
                    [
                    		'attribute' => '支付方式',
                    		'value' => function($model){
	                    		if($model['payType'] == '2000'){
		                    		return 'Android余额';
	                    		} else if($model['payType'] == '1000'){
		                    		return 'Apple余额';
	                    		} else if($model['payType'] == '2001'){
		                    		return '支付宝';
	                    		} else if($model['payType'] == '2002'){
	                    			return '微信';
	                    		} else {
		                    		return 'IOS';
		                    	}
                    		},
                    ],
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
			'pagination' => $page,
			'firstPageLabel' => '首页', 
			'lastPageLabel' => '尾页',
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
