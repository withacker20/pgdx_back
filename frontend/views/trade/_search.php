<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trade-search">

    <?php $form = ActiveForm::begin(['id'=>'trade-form',
        'action' => ['index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>购买时间</label>';
		echo DatePicker::widget([
			'name' => 'TradeSearch[tradeTimeStart]',
			'value' => $model->tradeTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'TradeSearch[tradeTimeEnd]',
			'value2' => $model->tradeTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>

    <?= $form->field($model, 'tradeNumber') ?>
    <?= $form->field($model, 'productName') ?>
    <?= $form->field($model, 'phoneNumber') ?>
    <?= $form->field($model, 'username') ?>
	<?= $form->field($model, 'payType')->dropDownList(array(''=>'所有','1000'=>'Apple余额','2000'=>'Android余额','2001'=>'支付宝', '2002'=>'微信', '2003'=>'IOS'))?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("trade-form");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
