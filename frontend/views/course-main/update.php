<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CourseMain */

if ($type == '0') {
    $this->params['breadcrumbs'][] = ['label' => '精品课程', 'url' => ['index']];
} else {
    $this->title = '添加';
    $this->params['breadcrumbs'][] = ['label' => '大咖说', 'url' => ['index', 'type' => '1']];
}
$this->params['breadcrumbs'][] = '更新';
$this->title = '更新';
?>
<div class="course-main-update">

    <?php if ($type =='0') {
        echo $this->render('_form', [
            'model' => $model,
            'teacherSelect' => $teacherSelect
        ]);
    } else {
        echo $this->render('daka_form', [
            'model' => $model,
            'teacherSelect' => $teacherSelect
        ]);
    }?>

</div>
