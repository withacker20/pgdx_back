<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CourseMainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-main-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'course-main-search',
    	'validateOnChange'=> false,
    	'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

    <?php
       
		echo '<label>发布时间</label>';
		echo DatePicker::widget([
			'name' => 'CourseMainSearch[publishTimeStart]',
			'value' => $model->publishTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'CourseMainSearch[publishTimeEnd]',
			'value2' => $model->publishTimeEnd,
		    'pluginOptions' => [
		        'format' => 'yyyy/mm/dd',
		        'clearBtn' => true,
		        'autoclose' => true,
		        'todayHighlight' => true
		    ]
		]);
	?>

    <?= $form->field($model, 'courseMainName')?>

 	<?= $form->field($model, 'courseMainStatus')->dropDownList(array(''=>'所有','1'=>'已发布','0'=>'未发布','2'=>'已下架'))?>
 
    <?= $form->field($model, 'teacherId')->dropDownList(ArrayHelper::map($teacherSelect,'id','teacherName'))?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("course-main-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
