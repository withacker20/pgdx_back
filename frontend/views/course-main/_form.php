<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);
AppAsset::addCss($this,'@web/assets/ueditor/themes/default/css/ueditor.css');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.config.js');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.all.js');
AppAsset::addScript($this,'@web/assets/ueditor/lang/zh-cn/zh-cn.js');

?>

<div class="course-main-form">

    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
	<?= $form->field($model, 'courseMainType')->hiddenInput(['value'=>'0'])->label(false) ?>
    <?= $form->field($model, 'productId')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'courseMainName')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'teacherId')->dropDownList(ArrayHelper::map($teacherSelect,'id','teacherName'))?>
    <?= $form->field($model, 'courseMainRemark')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'courseMainDesc')->textarea(['maxlength' => true,'rows'=>6,'id'=>'courseMainDesc','class'=>'col-sm-1 col-md-12','style'=>'margin-top: 10px;padding:0;margin:20px 0;width:100%;height:200px;border: none;'])?>
    
	<?php 
	if(Yii::$app->session['datasource']== 'app'){
    ?>
    	<?= $form->field($model, 'amount')->textInput() ?>
	    <!-- <?= $form->field($model, 'appleProductId')->hiddenInput()->label(false) ?> -->
	    <?= $form->field($model, 'shareTimes')->textInput()?>
	    <?= $form->field($model, 'shareTitle')->textInput(['maxlength' => true]) ?>
	    <?= $form->field($model, 'shareSummary')->textarea(['maxlength' => true]) ?>
    <?php 
    	}
    ?>
    
    <?= $form->field($model, 'orderNumber')->textInput(['placeholder' => "前3展现在首页"]) ?>

    <?= $form->field($model, 'coverId')->hiddenInput()->hint('建议尺寸：750*420像素') ?>
    
    <?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'image/*'
			],
			'pluginOptions' => [
					'showClose' => false,
					'initialPreview'=>[
							isset($model->attach) ? $model->attach->attachFullPath : null
					],
					'initialPreviewAsData'=>true,
					'initialPreviewShowDelete'=>false,
					'initialCaption'=> isset($model->attach) ? $model->attach->orgFileName : null,
					'initialPreviewConfig' => [
							['caption' => isset($model->attach) ? $model->attach->orgFileName : null, 'size' => isset($model->attach) ? $model->attach->attachSize * 1024 *1024 : null],
					],
					'uploadUrl' => Url::toRoute(['course-main/upload']),
					'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
					'dropZoneTitle' => '请上传图片',
					'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
					'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val(data.response['id']);
		 								}",
					'fileremoved' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val('');
		 								}",
					'change' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val('');
		 								}",
			],
		]);
	?>
	<br>
	
	<?= $form->field($model, 'pptId')->hiddenInput() ?>
	
	<?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'application/pdf,application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation',
			],
			'pluginOptions' => [
					'initialPreview'=>[
							isset($model->ppt) ? $model->ppt->attachFullPath : null
					],
					'initialPreviewAsData'=>true,
					'initialPreviewShowDelete'=>false,
					'initialPreviewFileType'=> isset($model->ppt) && strrchr($model->ppt->attachFullPath,'.pdf')=='.pdf' ? "pdf" : "other",
					'initialCaption'=> isset($model->ppt) ? $model->ppt->orgFileName : null,
					'initialPreviewConfig' => [
							['caption' => isset($model->ppt) ? $model->ppt->orgFileName : null, 'size' => isset($model->ppt) ? $model->ppt->attachSize * 1024 *1024 : null],
					],
					'allowedFileExtensions'=> ['ppt', 'pptx','pdf'],
					'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
					'uploadUrl' => Url::toRoute(['course-main/upload']),
					'maxFileCount' => 1,
					'maxFileSize' => 10240,
					'dropZoneTitle' => '请上传文件',
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-pptid').val(data.response['id']);
		 								}",
					'fileremoved' => "function(event, data, previewId, index) {
											$('#coursemainsearch-pptid').val('');
		 								}",
					'fileclear' => "function(event, data, previewId, index) {
											$('#coursemainsearch-pptid').val('');
		 								}",
					'change' => "function(event, data, previewId, index) {
											$('#coursemainsearch-coverid').val('');
		 								}",
			],
		]);
	?>
	
	<br>
    
    <div class="form-group">
        <?= Html::submitButton('保存并添加小课', ['onclick'=>'checkCourseMainDesc();', 'class' => 'btn btn-success']) ?>
        <?= Html::a('取消', ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('javascript-block') ?>
		function checkCourseMainDesc() {
			var desc = $("textarea[name='CourseMainSearch[courseMainDesc]']").val();
			if (desc == '') {
				$('.field-courseMainDesc').removeClass('has-success');
				$('.field-courseMainDesc').addClass('has-error');
				$('.field-courseMainDesc div.help-block').html("课程介绍不能为空");
			} else {
				$('.field-courseMainDesc').removeClass('has-error');
				$('.field-courseMainDesc').addClass('has-success');
				$('.field-courseMainDesc div.help-block').html("");
			}
		}
		function showShareTime() {
			$('#coursemainsearch-sharetimes').removeAttr("disabled");
			$('.field-coursemainsearch-sharetimes').show();
		}
		function hideShareTime() {
			$('#coursemainsearch-sharetimes').val('');
			$('#coursemainsearch-sharetimes').attr('disabled', true)
			$('.field-coursemainsearch-sharetimes').hide();
		}
        $(function () {
        	var ue = UE.getEditor('courseMainDesc', {maximumWords: 1024});
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
