<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '大咖说';
$this->params['breadcrumbs'][] = $this->title;
?>
<ul id="myTab" class="nav nav-tabs">
    <li>
        <a href="<?= Url::toRoute(['course-main/index']) ?>">
            精品课程
        </a>
    </li>
    <li class="active"><a href="#daka">大咖说</a></li>
</ul>
<div class="course-main-index">
<br>

    <?php echo $this->render('daka_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('添加课程', ['create', "type"=>'1'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?php if (Yii::$app->session['datasource'] == 'app') {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => '顺序',
                    'value' => function ($model) {
                        return $model['orderNumber'];
                    },
                ],
                [
                    'header' => '课程名称',
                    'class' => 'yii\grid\Column',
                    'content' => function($model){    						
                        return Html::tag('span',Html::encode($model['courseMainName']),['style'=>'width:100px', 'class'=>'ellipsis']);
                    },

                ],
                [
                    'attribute' => '价格',
                    'value' => function ($model) {
                        if (isset($model['amount'])) {
                            return $model['amount'];
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'attribute' => '日期',
                    'value' => function ($model) {
                        if (isset($model['courseMainDate'])) {
                            return date('Y-m-d', $model['courseMainDate'] / 1000);
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'attribute' => '状态',
                    'value' => function ($model) {
                        switch ($model['courseMainStatus']) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => '时长',
                    'value' => function ($model) {
                        return floor($model['ppt']['duration'] / 60) . '分'. ($model['ppt']['duration'] % 60).'秒';
                    },
                ],
                [
                    'attribute' => '发布时间',
                    'value' => function ($model) {
                        if (!empty($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{publish}&nbsp;{update}&nbsp;{view}&nbsp;{delete}',
                    'buttons' => [
                        'publish' => function ($url, $model, $key) {
                            if ($model['courseMainStatus'] == '0' || $model['courseMainStatus'] == '2') {
                                return Html::a('发布', ['publish', 'id' => $model['id'], 'type'=>'1'], ['class' => 'btn btn-sm btn-primary']);
                            } else if ($model['courseMainStatus'] == '1') {
                                return Html::a('下架', ['depublish', 'id' => $model['id'], 'type'=>'1'], ['class' => 'btn btn-sm btn-primary']);
                            }
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('修改', ['update', 'id' => $model['id'], 'type'=>'1'], ['class' => 'btn btn-sm btn-primary']);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('预览', ['view', 'id' => $model['id'], 'type'=>'1'], ['class' => 'btn btn-sm btn-info']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', ['delete', 'id' => $model['id'], 'type'=>'1'],
                                ['class' => 'btn btn-sm btn-danger', 'data' => [
                                    'confirm' => '你确定删除这条信息吗?',
                                    'method' => 'post',
                                ],
                                ]);
                        }
                    ],
                ],
            ]

        ]);
    }  ?>
    <?php
    echo LinkPager::widget([
        'pagination' => $page,
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>