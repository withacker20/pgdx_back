<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CourseMain */

$this->title = $model->courseMainName;
$this->params['breadcrumbs'][] = ['label' => '大咖说', 'url' => ['index', 'type' => '1']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-main-view">
    <p>
        <?= Html::a('返回', Url::to(['/course-main/index', 'type' => '1']), ['class' => 'btn btn-primary']) ?>
    </p>

    <?php
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'courseMainName',
                [
                    'label' => '日期',
                    'value' => function ($model) {
                        if (!empty($model['courseMainDate'])) {
                            return date('Y-m-d', $model['courseMainDate'] / 1000);
                        } else {
                            return "";
                        }
                    }
                ],
                [
                    'label' => '视频简介',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['courseMainDesc'];
                    }
                ],
                'amount',
                'shareTitle',
                'shareSummary',
                'orderNumber',
                [
                    'label' => '状态',
                    'value' => function ($model) {
                        switch ($model->courseMainStatus) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                    }
                ],
                [
                    'label' => '发布时间',
                    'value' => function ($model) {
                        if (!empty($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return "";
                        }
                    }
                ],
                [
                    'label' => '首页视频封面',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (!empty($model['attach'])) {
                            return Html::img($model->attach->attachFullPath, ['style' => 'heigth:100px;width:100px;']);
                        }
                    }
                ],
                [
                    'label' => '详情页视频封面',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (!empty($model['detailAttach'])) {
                            return Html::img($model->detailAttach->attachFullPath, ['style' => 'heigth:100px;width:100px;']);
                        }
                    }
                ],
            ],
        ]);
    ?>
    <strong>视频文件</strong><br>
    <video src="<?=$model->ppt->attachFullPath?>" controls="controls">您的浏览器不支持 video 标签。</video>
</div>
