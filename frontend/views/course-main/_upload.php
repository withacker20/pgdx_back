<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


$this->title = '课程发布';
?>
<?php Pjax::begin(); ?>
<?php $form_upload = ActiveForm::begin(['id'=>'cm_cover_up_form','action'=>Url::toRoute(['course-main/upload']),'options' => ['enctype' => 'multipart/form-data']]) ?>

<?php echo Url::toRoute(['course-main/upload']); ?>

<?= $form_upload->field($model, 'cover')->fileInput() ?>

<?= $form_upload->field($model, 'id')->hiddenInput(['value'=>$model->id])?>

<?php 
	echo FileInput::widget([
		'name' => 'attachment',
		'options'=>[
				'multiple'=>true,
		],
		'pluginOptions' => [
				//'uploadUrl' => Url::to(['/site/file-upload']),
				'uploadUrl' => Url::toRoute(['course-main/upload']),
				'uploadExtraData' => [
						'album_id' => 20,
						'cat_id' => 'Nature'
				],
				'maxFileCount' => 10
		]
	]);
?>

<?= Html::submitButton('上传', ['class' => 'btn btn-success']) ?>

 <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
 
<?php ActiveForm::end() ?>
<?php Pjax::end(); ?>
<?php $this->beginBlock('javascript-block') ?>
        $(function () {
            $('form#cm_cover_up_form11').on('beforeSubmit',function(e){
            	alert('111');
            	var $form = $(this);
                $.ajax({
                    url: $form.attr('action'),
                    type: 'post',
                    data: $form.serialize(),
                    success: function (data) {
                    	alert('22');
                        alert(data);
                    },
                    error:function(e){
                    	alert('333');
                    }
                });
            }).on('submit', function (e) {
                e.preventDefault();
            });
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
<?php 
	/*$requestUrl = Url::toRoute(['course-main/index']);
	$this->registerJs("
		$(document).ready(function(){
			alert('aa');
			$('form#cm_cover_up_form').on('beforeSubmit',function(e){
				alert('bbb');
				var form = $(this);
				alert($(form).attr('action')+$(form).serialize());
                $.ajax({
                    url: $(form).attr('action'),
                    type: 'post',
                    data: $(form).serialize(),
                    success: function (data) {
                        alert(data['success']);
                    }
                });
			}).on('submit', function (e) {
				alert('ccc');
                e.preventDefault();
            });	        
		});
			
	    ",View::POS_READY);
	    */
?>
