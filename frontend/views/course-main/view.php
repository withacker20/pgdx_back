<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CourseMain */

$this->title = $model->courseMainName;
$this->params['breadcrumbs'][] = ['label' => '精品课程', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-main-view">
    <p>
        <?= Html::a('返回', Url::to(['/course-main/index']), ['class' => 'btn btn-primary']) ?>
    </p>

    <?php
    if (Yii::$app->session['datasource'] == 'app') {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'courseMainName',
                [
                    'label' => '讲师',
                    'value' => function ($model) {
                        return $model->teacher->teacherName;
                    }
                ],
                'courseMainRemark',
                [
                    'label' => '课程介绍',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['courseMainDesc'];
                    }
                ],
                'amount',
                'shareTimes',
                'shareTitle',
                'shareSummary',
                'orderNumber',
                [
                    'label' => '状态',
                    'value' => function ($model) {
                        switch ($model->courseMainStatus) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                    }
                ],
                [
                    'label' => '发布时间',
                    'value' => function ($model) {
                        if (isset($mode['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return "";
                        }
                    }
                ],
                [
                    'label' => '封面图片',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (isset($model['attach'])) {
                            return Html::img($model->attach->attachFullPath, ['style' => 'heigth:100px;width:100px;']);
                        }
                    }
                ],
            ],
        ]);
    } else {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'courseMainName',
                [
                    'label' => '讲师',
                    'value' => function ($model) {
                        return $model->teacher->teacherName;
                    }
                ],
                [
                    'label' => '课程介绍',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['courseMainDesc'];
                    }
                ],
                'orderNumber',
                [
                    'label' => '状态',
                    'value' => function ($model) {
                        switch ($model->courseMainStatus) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                    }
                ],
                [
                    'label' => '发布时间',
                    'value' => function ($model) {
                        if (isset($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return "";
                        }
                    }
                ],
                [
                    'label' => '封面图片',
                    'format' => 'html',
                    'value' => function ($model) {
                        if (isset($model['attach'])) {
                            return Html::img($model->attach->attachFullPath, ['style' => 'heigth:100px;width:100px;']);
                        }
                    }
                ],
            ],
        ]);
    }
    ?>
</div>
