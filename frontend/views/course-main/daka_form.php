<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use kartik\date\DatePicker;

AppAsset::register($this);
AppAsset::addCss($this,'@web/assets/ueditor/themes/default/css/ueditor.css');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.config.js');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.all.js');
AppAsset::addScript($this,'@web/assets/ueditor/lang/zh-cn/zh-cn.js');

AppAsset::addCss($this,'@web/assets/webuploader-0.1.5/webuploader.css');
AppAsset::addScript($this,'@web/assets/webuploader-0.1.5/webuploader.js');


?>

<div class="course-main-form">

    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'courseMainType')->hiddenInput(['value'=>'1'])->label(false) ?>
    <?= $form->field($model, 'courseMainName')->textInput(['maxlength' => 28])->label('视频名称') ?>
    <?= $form->field($model, 'courseMainDate')->widget(DatePicker::classname(), [ 
		'options' => ['placeholder' => ''], 
		'pluginOptions' => [ 
			'autoclose' => true, 
			'todayHighlight' => true, 
			'format' => 'yyyy-mm-dd', 
		] 
	]); ?>
    
    <?= $form->field($model, 'courseMainDesc')->textarea(['maxlength' => true,'rows'=>6,'id'=>'courseMainDesc','class'=>'col-sm-1 col-md-12','style'=>'margin-top: 10px;padding:0;margin:20px 0;width:100%;height:200px;border: none;'])->label("视频简介")?>
	
    <?= $form->field($model, 'amount')->textInput() ?>
	<?= $form->field($model, 'shareTitle')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'shareSummary')->textarea(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'orderNumber')->textInput(['placeholder' => "前4展现在首页"]) ?>

    <?= $form->field($model, 'coverId')->hiddenInput()->hint('建议尺寸：428*240像素')->label('首页视频封面') ?>
    
    <?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'image/*'
			],
			'pluginOptions' => [
					'showClose' => false,
					'initialPreview'=>[
							isset($model->attach) ? $model->attach->attachFullPath : null
					],
					'initialPreviewAsData'=>true,
					'initialPreviewShowDelete'=>false,
					'initialCaption'=> isset($model->attach) ? $model->attach->orgFileName : null,
					'initialPreviewConfig' => [
							['caption' => isset($model->attach) ? $model->attach->orgFileName : null, 'size' => isset($model->attach) ? $model->attach->attachSize * 1024 *1024 : null],
					],
					'uploadUrl' => Url::toRoute(['course-main/upload']),
					'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
					'dropZoneTitle' => '请上传图片',
					'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
					'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val(data.response['id']);
		 								}",
					'fileremoved' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val('');
		 								}",
					'change' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-coverid').val('');
		 								}",
			],
		]);
	?>
	<br>

	<?= $form->field($model, 'detailCoverId')->hiddenInput()->hint('建议尺寸：750*420像素') ?>
	<?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'image/*'
			],
			'pluginOptions' => [
					'showClose' => false,
					'initialPreview'=>[
							isset($model->detailAttach) ? $model->detailAttach->attachFullPath : null
					],
					'initialPreviewAsData'=>true,
					'initialPreviewShowDelete'=>false,
					'initialCaption'=> isset($model->detailAttach) ? $model->detailAttach->orgFileName : null,
					'initialPreviewConfig' => [
							['caption' => isset($model->detailAttach) ? $model->detailAttach->orgFileName : null, 'size' => isset($model->detailAttach) ? $model->detailAttach->attachSize * 1024 *1024 : null],
					],
					'uploadUrl' => Url::toRoute(['course-main/upload']),
					'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
					'dropZoneTitle' => '请上传图片',
					'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
					'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-detailcoverid').val(data.response['id']);
		 								}",
					'fileremoved' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-detailcoverid').val('');
		 								}",
					'change' => "function(event, data, previewId, index) { 
											$('#coursemainsearch-detailcoverid').val('');
		 								}",
			],
		]);
	?>
	<br>
	
	<?= $form->field($model, 'pptId')->hiddenInput()->label('视频文件')->hint('建议大小：70M以内') ?>
	
	<?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'video/*'
			],
				'pluginOptions' => [
						'allowedFileExtensions'=> ['mp4'],
						'allowedPreviewTypes' => ['video'],
						'uploadUrl' => Url::toRoute(['course/upload-audio']),
						'maxFileCount' => 1,
						'dropZoneTitle' => '请上传视频',
						'progressUploadThreshold' => '99',
						'maxFileSize'=>71680,
						'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
				],
				'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
						$('#coursemainsearch-pptid').val(data.response['id']);
					 }",
					'fileremoved' => "function(event, data, previewId, index) { 
						$('#coursemainsearch-pptid').val('');
					 }",
					'change' => "function(event, data, previewId, index) { 
						$('#coursemainsearch-pptid').val('');
					 }",
				],
		]);
	?>
	
	<br>
    
    <div class="form-group">
        <?= Html::submitButton('保存', ['onclick'=>'checkCourseMainDesc();', 'class' => 'btn btn-success']) ?>
        <?= Html::a('取消', ['index', 'type' => '1'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php $this->beginBlock('javascript-block') ?>
		function checkCourseMainDesc() {
			var desc = $("textarea[name='CourseMainSearch[courseMainDesc]']").val();
			if (desc == '') {
				$('.field-courseMainDesc').removeClass('has-success');
				$('.field-courseMainDesc').addClass('has-error');
				$('.field-courseMainDesc div.help-block').html("简介不能为空");
			} else {
				$('.field-courseMainDesc').removeClass('has-error');
				$('.field-courseMainDesc').addClass('has-success');
				$('.field-courseMainDesc div.help-block').html("");
			}
		}

        $(function () {
        	var ue = UE.getEditor('courseMainDesc', {maximumWords: 1024});
        });

<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
