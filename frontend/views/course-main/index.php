<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseMainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '精品课程';
$this->params['breadcrumbs'][] = $this->title;
?>
<ul id="myTab" class="nav nav-tabs">
    <li class="active">
        <a href="#home">
            精品课程
        </a>
    </li>
    <li><a href="<?= Url::toRoute(['course-main/index','type'=>'1']) ?>">大咖说</a></li>
</ul>
<div class="course-main-index">

    <br>

    <?php echo $this->render('_search', ['model' => $searchModel, 'teacherSelect' => $teacherSelect]); ?>
    <p>
        <?= Html::a('添加课程', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?php if (Yii::$app->session['datasource'] == 'app') {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => '顺序',
                    'value' => function ($model) {
                        return $model['orderNumber'];
                    },
                ],
                [
                    'header' => '课程名称',
                    'class' => 'yii\grid\Column',
                    'content' => function ($model) {
                        return Html::a(Html::encode($model['courseMainName']), Url::to(['/course/view-subs', 'courseMainId' => $model['id']]), ['title' => $model['courseMainName'], 'class' => 'ellipsis', 'style' => 'width:200px;']);
                    },

                ],
                [
                    'attribute' => '讲师',
                    'value' => function ($model) {
                        return $model['teacher']['teacherName'];
                    },
                ],
                [
                    'attribute' => '状态',
                    'value' => function ($model) {
                        switch ($model['courseMainStatus']) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => '包含课时',
                    'value' => function ($model) {
                        return round($model['duration'] / 60) . '分钟';
                    },
                ],
                [
                    'attribute' => '价格',
                    'value' => function ($model) {
                        return $model['amount'];
                    },
                ],
                [
                    'attribute' => '发布时间',
                    'value' => function ($model) {
                        if (!empty($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{publish}&nbsp;{update}&nbsp;{view}&nbsp;{delete}',
                    'buttons' => [
                        'publish' => function ($url, $model, $key) {
                            if ($model['courseMainStatus'] == '0' || $model['courseMainStatus'] == '2') {
                                return Html::a('发布', ['publish', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                            } else if ($model['courseMainStatus'] == '1') {
                                return Html::a('下架', ['depublish', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                            }
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('修改', ['update', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('预览', ['view', 'id' => $model['id']], ['class' => 'btn btn-sm btn-info']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', ['delete', 'id' => $model['id']],
                                ['class' => 'btn btn-sm btn-danger', 'data' => [
                                    'confirm' => '你确定删除这条主课信息吗?',
                                    'method' => 'post',
                                ],
                                ]);
                        }
                    ],
                ],
            ]

        ]);
    } else {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => '顺序',
                    'options' => ['width' => '5'],
                    'value' => function ($model) {
                        return $model['orderNumber'];
                    },
                ],
                [
                    'header' => '课程名称',
                    'class' => 'yii\grid\Column',
                    'content' => function ($model) {
                        return Html::a(Html::encode($model['courseMainName']), Url::to(['/course/view-subs', 'courseMainId' => $model['id']]), ['title' => $model['courseMainName'], 'class' => 'ellipsis', 'style' => 'width:200px;']);
                    },

                ],
                [
                    'attribute' => '讲师',
                    'value' => function ($model) {
                        return $model['teacher']['teacherName'];
                    },
                ],
                [
                    'attribute' => '状态',
                    'value' => function ($model) {
                        switch ($model['courseMainStatus']) {
                            case 0:
                                return '未发布';
                            case 1:
                                return '已发布';
                            case 2:
                                return '已下架';
                        }
                        return '';
                    },
                ],
                [
                    'attribute' => '包含课时',
                    'value' => function ($model) {
                        return round($model['duration'] / 60) . '分钟';
                    },
                ],
                [
                    'attribute' => '发布时间',
                    'options' => ['width' => '10'],
                    'value' => function ($model) {
                        if (!empty($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return '';
                        }
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{publish}&nbsp;{update}&nbsp;{view}&nbsp;{delete}',
                    'buttons' => [
                        'publish' => function ($url, $model, $key) {
                            if ($model['courseMainStatus'] == '0' || $model['courseMainStatus'] == '2') {
                                return Html::a('发布', ['publish', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                            } else if ($model['courseMainStatus'] == '1') {
                                return Html::a('下架', ['depublish', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                            }
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('修改', ['update', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                        },
                        'view' => function ($url, $model, $key) {
                            return Html::a('预览', ['view', 'id' => $model['id']], ['class' => 'btn btn-sm btn-info']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('删除', ['delete', 'id' => $model['id']],
                                ['class' => 'btn btn-sm btn-danger', 'data' => [
                                    'confirm' => '你确定删除这条主课信息吗?',
                                    'method' => 'post',
                                ],
                                ]);
                        }
                    ],
                ],
            ]

        ]);
    } ?>
    <?php
    echo LinkPager::widget([
        'pagination' => $page,
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>