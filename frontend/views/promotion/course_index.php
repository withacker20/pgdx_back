<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '实名认证';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="course-index-promotion">

        <?php Pjax::begin(); ?>
        <?php echo $this->render('course_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['header' => '选择',
                    'class' => 'yii\grid\RadioButtonColumn',
                    'radioOptions' => function ($model) {
                        return [
                            'value' => $model['id'],
                            'class' => 'testradio',
                        ];
                    }
                ],
                [
                    'attribute' => '文章标题',
                    'value' => function ($model) {
                        return $model['courseMainName'];
                    },
                ],
                [
                    'attribute' => '发布时间',
                    'value' => function ($model) {
                        if (!is_null($model['publishTime'])) {
                            return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ]); ?>
        <?php
        echo LinkPager::widget([
            'pagination' => $page,
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
<?php $this->beginBlock('javascript-block') ?>
    $(function () {
        $('.testradio').on('click',function(){
        $('#promotionsearch-refname').val($(this).parent().next().html());
        $('#promotionsearch-refid').val($(this).val());
        $('#promotionsearch-reftype').val("course");
        $('#course-modal').modal('hide');
    });
    $('.pagination').find('a').on('click',function(){
    var href = $(this).attr("test");
    console.log(href);
    $.get(href, {},
    function (data) {
    $('#course-modal').find('.modal-body').html(data);
    }
    );
    });
    $('.pagination').find('a').each(function(){
    $(this).attr("test",$(this).attr("href"));
    $(this).attr("href","javascript:void(0);");
    })
    });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>