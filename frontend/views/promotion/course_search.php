<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-search-promotion">

    <?php $form = ActiveForm::begin(['id'=>'course-search-promotion',
        'action' => ['promotion/course-index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>发布时间</label>';
		echo DatePicker::widget([
			'id' => 'publishDate',
			'name' => 'CourseMainSearch[publishTimeStart]',
			'value' => $model->publishTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'CourseMainSearch[publishTimeEnd]',
			'value2' => $model->publishTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'courseMainName') ?>

    <div class="form-group">
        <?= Html::button('查询', ['id'=>'searchBtn','class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("course-search-promotion");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('javascript-block') ?>
        $(function () {
        	$('#searchBtn').on('click',function(){
            	$.get('<?=Url::toRoute('promotion/course-index') ?>', $("#course-search-promotion").serialize(),
		            function (data) {
		                $('#course-modal').find('.modal-body').html(data);
		            }  
        		);
            });
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
