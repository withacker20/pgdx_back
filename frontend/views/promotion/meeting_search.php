<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="meeting-search-promotion">

    <?php $form = ActiveForm::begin(['id'=>'meeting-search-promotion',
        'action' => ['promotion/meeting-index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>开始时间</label>';
		echo DatePicker::widget([
			'id' => 'publishDate',
			'name' => 'MeetingSearch[startTimeStart]',
				'value' => $model->startTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'MeetingSearch[startTimeEnd]',
				'value2' => $model->startTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'name') ?>
    

    <div class="form-group">
        <?= Html::button('查询', ['id'=>'searchBtn','class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("meeting-search-promotion");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('javascript-block') ?>
        $(function () {
        	$('#searchBtn').on('click',function(){
            	$.get('<?=Url::toRoute('message/nmeeting-index') ?>', $("#meeting-search-promotion").serialize(),
		            function (data) {
		                $('#meeting-modal').find('.modal-body').html(data);
		            }  
        		);
            });
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
