<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = "APP推广位";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotion-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
        	[
        		'label'=>'推广icon',
        		'format'=>'html',
        		'value'=>function($model){
        			if(isset($model['icon'])) {
        				return Html::img($model->icon->attachFullPath,['style'=>'heigth:50px;width:50px;']);
        			}
		    	}
		    ],
		    'refName',
		    'refUrl',
        ],
    ]) ?>
    <p>
        <?= Html::a('修改', Url::to(['/promotion/update']), ['class' => 'btn btn-primary']) ?>
    </p>
</div>
