<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\bootstrap\Modal;

?>

<div class="promotion-form">

    <?php $form = ActiveForm::begin(['validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

    <?= $form->field($model, 'iconId')->hiddenInput()->hint("50px * 50px") ?>
    
    <?php
    echo FileInput::widget([
    		'name' => 'uploadfile',
    		'options'=>[
    				'multiple'=>false,
    				'accept' => 'image/*'
    		],
    		'pluginOptions' => [
    				'showClose' => false,
    				'initialPreview'=>[
    						isset($model->icon) ? $model->icon->attachFullPath : null
    				],
    				'initialPreviewAsData'=>true,
    				'initialPreviewShowDelete'=>false,
    				'initialCaption'=> isset($model->icon) ? $model->icon->orgFileName : null,
    				'initialPreviewConfig' => [
    						['caption' => isset($model->icon) ? $model->icon->orgFileName : null, 'size' => isset($model->icon) ? $model->icon->attachSize * 1024 *1024 : null],
    				],
    				'uploadUrl' => Url::toRoute(['course-main/upload']),
    				'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
    				'dropZoneTitle' => '请上传图片',
    				'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
    				'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
    		],
    		'pluginEvents' => [
    				'fileuploaded' => "function(event, data, previewId, index) {
											$('#promotionsearch-iconid').val(data.response['id']);
		 								}",
    				'fileremoved' => "function(event, data, previewId, index) {
											$('#promotionsearch-iconid').val('');
		 								}",
    				'change' => "function(event, data, previewId, index) {
											$('#promotionsearch-iconid').val('');
		 								}",
    		],
    ]);
    ?>
    <br>

    <div class="form-group">
        <?= Html::button('文章库', ['onclick' => 'showArticle();', 'class' => 'btn btn-default test']) ?>
        <?= Html::button('课程库', ['onclick' => 'showCourse();', 'class' => 'btn btn-default test']) ?>
        <?= Html::button('会议库', ['onclick' => 'showMeeting();', 'class' => 'btn btn-default test']) ?>
    </div>

    <?= $form->field($model, 'refId')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'refType')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'refName')->textInput() ?>

    <?= Html::checkbox("seflUrl", $model->urlFlag,['id'=>'seflUrl','onclick'=>"clickCheckBox(this);"]) ?>
    
    <?= Html::label("自定义链接","promotionsearch-refurl") ?>
    
    <?= $form->field($model, 'refUrl')->textInput(['disabled' => true])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
        <?= Html::a('返回', Url::to(['/promotion/view']), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
Modal::begin([
    'id' => 'article-modal',
    'header' => '<h4 class="modal-title">文章库</h4>',
    'size' => 'modal-lg',
]);

Modal::end();
?>
<?php
Modal::begin([
    'id' => 'course-modal',
    'header' => '<h4 class="modal-title">课程库</h4>',
    'size' => 'modal-lg',
]);

Modal::end();
?>
<?php
Modal::begin([
    'id' => 'meeting-modal',
    'header' => '<h4 class="modal-title">会议库</h4>',
    'size' => 'modal-lg',
]);

Modal::end();
?>

<?php $this->beginBlock('javascript-block') ?>
function clickCheckBox(obj) {
	$('.has-error').removeClass('has-error');
	$('.help-block').html('');
	$('#promotionsearch-reftype').val('self');
	if ($(obj).prop('checked') === true) {
		$('#promotionsearch-refname').attr('disabled', true);
		$('#promotionsearch-refname').val('');
		$('#promotionsearch-refid').val('');
		$('#promotionsearch-refurl').removeAttr("disabled");
		$('.test').attr('disabled', true);
	} else {
		$('#promotionsearch-refurl').attr('disabled', true);
		$('#promotionsearch-refurl').val('');
		$('#promotionsearch-refname').removeAttr("disabled");
		$('.test').removeAttr("disabled");
	}
}
function showArticle() {
    $.get('<?= Url::toRoute('promotion/news-index') ?>', {},
        function (data) {
            $('#article-modal').find('.modal-body').html(data);
            $('#article-modal').modal('show');
        }
	);
    $('.field-promotionsearch-refname').show();
    $('#promotionsearch-refurl').val('');

    $('#promotionsearch-refurl').attr('disabled', true);
    $('#promotionsearch-refname').removeAttr("disabled");
}

function showCourse() {
    $.get('<?= Url::toRoute('promotion/course-index') ?>', {},
        function (data) {
            $('#course-modal').find('.modal-body').html(data);
            $('#course-modal').modal('show');
        }
	);
    $('.field-promotionsearch-refname').show();
    $('#promotionsearch-refurl').val('');

    $('#promotionsearch-refurl').attr('disabled', true);
    $('#promotionsearch-refname').removeAttr("disabled");
}

function showMeeting() {
    $.get('<?= Url::toRoute('promotion/meeting-index') ?>', {},
        function (data) {
            $('#meeting-modal').find('.modal-body').html(data);
            $('#meeting-modal').modal('show');
        }
	);
    $('.field-promotionsearch-refname').show();
    $('#promotionsearch-refurl').val('');

    $('#promotionsearch-refurl').attr('disabled', true);
    $('#promotionsearch-refname').removeAttr("disabled");
}
$(function () {
	if ('<?= $model->urlFlag?>' == '1') {
		$('#promotionsearch-refname').attr('disabled', true);
		$('#promotionsearch-refurl').removeAttr("disabled");
		$('.test').attr('disabled', true);
	}
});
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
