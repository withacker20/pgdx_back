<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
?>
<div class="meeting-index-promotion">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('meeting_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['header' => '选择',
                'class' => 'yii\grid\RadioButtonColumn',
                'headerOptions' => ['width' => '8%'],
                'radioOptions' => function ($model) {
                    return [
                        'value' => $model['id'],
                        'class' => 'testradio',
                    ];
                }
            ],
            [
                'attribute' => '会议标题',
                'value' => function ($model) {
                    return $model['name'];
                },
            ],
            [
                'attribute' => '开始时间',
                'value' => function ($model) {
                    if (!is_null($model['startTime'])) {
                        return date('Y-m-d H:i:s', $model['startTime']);
                    } else {
                        return '';
                    }
                },
            ],
        ],
    ]); ?>
    <?php
    echo LinkPager::widget([
        'pagination' => $page,
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
<?php $this->beginBlock('javascript-block') ?>
        $(function () {
            $('.testradio').on('click',function(){
            	$('#promotionsearch-refname').val($(this).parent().next().html());
            	$('#promotionsearch-refid').val($(this).val());
            	$('#promotionsearch-reftype').val("meeting");
            	$('#meeting-modal').modal('hide');
            });
            $('.pagination').find('a').on('click',function(){
            	var href = $(this).attr("test");
            	$.get(href, {},
		            function (data) {
		                $('#meeting-modal').find('.modal-body').html(data);
		            }  
        		);
            });
            $('.pagination').find('a').each(function(){
            	$(this).attr("test",$(this).attr("href"));
            	$(this).attr("href","javascript:void(0);");
            })
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>