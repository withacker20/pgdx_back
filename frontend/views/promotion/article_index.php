<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '实名认证';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="article-index-message">

        <?php Pjax::begin(); ?>
        <?php echo $this->render('article_search', ['model' => $searchModel, 'categoryList' => $categoryList]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['header' => '选择',
                    'class' => 'yii\grid\RadioButtonColumn',
                    'headerOptions' => ['width' => '8%'],
                    'radioOptions' => function ($model) {
                        return [
                            'value' => $model['article_id'],
                            'class' => 'testradio',
                        ];
                    }
                ],
                [
                    'attribute' => '文章标题',
                    'value' => function ($model) {
                        return $model['title'];
                    },
                ],
                [
                    'attribute' => '发布时间',
                    'value' => function ($model) {
                        if (!is_null($model['pubtime'])) {
                            return date('Y-m-d H:i:s', $model['pubtime']);
                        } else {
                            return '';
                        }
                    },
                ],
            ],
        ]); ?>
        <?php
        echo LinkPager::widget([
            'pagination' => $page,
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
<?php $this->beginBlock('javascript-block') ?>
$(function () {
    $('.testradio').on('click',function(){
        $('#promotionsearch-refname').val($(this).parent().next().html());
        $('#promotionsearch-refid').val($(this).val());
        $('#promotionsearch-reftype').val("article");
        $('#article-modal').modal('hide');
    });
    $('.pagination').find('a').on('click',function(){
        var href = $(this).attr("test");
        console.log(href);
        $.get(href, {},
            function (data) {
                $('#article-modal').find('.modal-body').html(data);
            }
        );
    });
    $('.pagination').find('a').each(function(){
        $(this).attr("test",$(this).attr("href"));
        $(this).attr("href","javascript:void(0);");
    })
});
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>