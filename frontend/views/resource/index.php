<?php

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '附件列表';
$this->params['breadcrumbs'][] = ['label' => '资料库管理', 'url' => ['/resource-base/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">
	
	<?= DetailView::widget([
			'model' => $resourceBase,
        'attributes' => [
            'baseName',
        ],
    ]) ?>
    <p>
        <?= Html::a('添加附件', ['create', 'baseId'=>$model->baseId],
        		['class' => 'btn btn-primary']);?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
	    		[
	    				'attribute' => '顺序',
	    				'content' => function ($model) {
		    				return Editable::widget([
		    						'name' => 'ResourceFileSearch[fileOrder]',
		    						'ajaxSettings' => ['url' => Url::toRoute(['resource/update', 'id' => $model['id'], 'baseId' => $model['baseId']])],
		    						'asPopover' => true,
		    						'value' => $model['fileOrder'],
		    						'header' => '顺序号',
		    						'size' => 'md',
		    						'options' => ['class' => 'form-control']
		    				]);
	    				},
	    		],
			    [
			    		'attribute' => '文件名称',
			    		'value' => function($model){
			    		return $model['attach']['orgFileName'];
			    		},
			    ],
        		[
        				'class' => 'yii\grid\ActionColumn',
        				'template' => '{update}',
        				'buttons' => [
        						'update' => function ($url, $model, $key) {
        						return Html::a('修改', ['update', 'id' => $model['id']],
        								['class'=>'btn btn-sm btn-info']);
			    		}
			    		],
        				'options' => [
        						'width' => 5
        				]
        				],
        		[
        				'class' => 'yii\grid\ActionColumn',
        				'template' => '{delete}',
        				'buttons' => [
        						'delete' => function ($url, $model, $key) {
        						return Html::a('删除', ['delete', 'id' => $model['id']],
        								['class'=>'btn btn-sm btn-danger','data' => [
        												'confirm' => '你确定删除这个附件吗?',
        												'method' => 'post',
        								],
        						]);
        				}
        		],
        				'options' => [
        						'width' => 5
        				]
        	],
            
        ],
    ]); ?>
    
    <div class="form-group">

<?= Html::a('返回',['/resource-base/index'],['class' => 'btn btn-default']) ?>
</div>
    
</div>

