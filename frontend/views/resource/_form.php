<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-base-form">

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

<?= $form->field($model, 'attachId')->hiddenInput() ?>

<?php 
		echo FileInput::widget([
				'name' => 'uploadfile',
				'options'=>[
						'multiple'=>false,
						'accept' => 'application/pdf',
				],
				'pluginOptions' => [
						'showClose' => false,
						'initialPreview'=>[
								isset($model->icon) ? $model->icon->attachFullPath : null
						],
						'initialPreviewAsData'=>true,
						'initialPreviewShowDelete'=>false,
						'initialCaption'=> isset($model->icon) ? $model->icon->orgFileName : null,
						'initialPreviewConfig' => [
								['caption' => isset($model->icon) ? $model->icon->orgFileName : null, 'size' => isset($model->icon) ? $model->icon->attachSize * 1024 *1024 : null],
						],
						'uploadUrl' => Url::toRoute(['resource/upload']),
						'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
						'dropZoneTitle' => '请上传文件',
						'allowedFileExtensions'=> ['pdf'],
						'allowedPreviewTypes' => ['pdf'],
						'maxFileSize' => 10240,
						'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
				],
				'pluginEvents' => [
						'fileuploaded' => "function(event, data, previewId, index) {
											$('#resourcefilesearch-attachid').val(data.response['id']);
		 								}",
						'fileremoved' => "function(event, data, previewId, index) {
											$('#resourcefilesearch-attachid').val('');
		 								}",
						'change' => "function(event, data, previewId, index) {
											$('#resourcefilesearch-attachid').val('');
		 								}",
				],
		]);
	?>
	
	<br>

<?= $form->field($model, 'fileOrder')->textInput() ?>

<?= Html::hiddenInput('ResourceFileSearch[baseId]', $model['baseId']) ?>

<div class="form-group">
<?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>

<?= Html::a('返回',['/resource/index', 'baseId' => $model->baseId],['class' => 'btn btn-default']) ?>
</div>

 
<?php ActiveForm::end() ?>
</div>
