<?php
$this->title = '更新附件';
$this->params['breadcrumbs'][] = ['label' => '资料库管理', 'url' => ['/resource-base/index']];
$this->params['breadcrumbs'][] = ['label' => '附件列表', 'url' => ['/resource/index', 'baseId'=>$model->baseId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-file-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
