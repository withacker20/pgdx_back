<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Boarding */

$this->title = '添加轮播';
$this->params['breadcrumbs'][] = ['label' => '轮播列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boarding-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
