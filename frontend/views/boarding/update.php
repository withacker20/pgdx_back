<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Boarding */

$this->params['breadcrumbs'][] = ['label' => '轮播列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改轮播';
?>
<div class="boarding-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
