<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Boarding */

$this->title = '预览轮播';
$this->params['breadcrumbs'][] = ['label' => '轮播列表', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boarding-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'boardingName',
            'boardingOrder',
            [
                'label' => '发布状态',
                'value' => function ($model) {
                    switch ($model->boardingStatus) {
                        case 0:
                            return '未发布';
                        case 1:
                            return '已发布';
                    }
                }
            ],
            [
                'label' => '轮播类型',
                'value' => function ($model) {
                    switch ($model->boardingType) {
                        case 'cm':
                            return '精品课程';
                        case 'cm1':
                            return '大咖说';
                        case 'n':
                            return '图文';
                    }
                }
            ],
            [
                'label' => '关联信息',
                'value' => function ($model) {
                    if ($model->boardingType == 'n') {
                        return $model->refObject->newsTitle;
                    } else {
                        return $model->refObject->courseMainName;
                    }
                }
            ],
            [
                'label' => '发布时间',
                'value' => function ($model) {
                    if (isset($model['publishTime'])) {
                        return date('Y-m-d H:i:s', $model['publishTime'] / 1000);
                    } 
                }
            ],
            [
            		'label'=>'封面图片',
            		'format'=>'html',
            		'value'=>function($model){
            		if(isset($model['attach'])) {
            			return Html::img($model->attach->attachFullPath,['style'=>'heigth:100px;width:100px;']);
            		}
            }
            ],
        ],
    ]) ?>

    <p>
        <?= Html::a('返回', Url::to(['/boarding/index']), ['class' => 'btn btn-primary']) ?>
    </p>
</div>
