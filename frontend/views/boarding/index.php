<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoardingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '轮播列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boarding-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('添加轮播', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				[
    						'attribute' => '顺序',
    						'value' => function($model){
    							return $model['boardingOrder'];
                            },
                    ],
                    [
                    		'header' => '轮播名称',
                            'class' => 'yii\grid\Column',
                    		'content' => function($model){
                    			return Html::tag('span',Html::encode($model['boardingName']),['title'=>$model['boardingName'],'style'=>'width:150px', 'class'=>'ellipsis']);
                            },
                            ],
                    [
                    		'attribute' => '状态',
                    		'value' => function($model){
                    			switch ($model['boardingStatus']){
	                    			case 0:return '未发布';
	                    			case 1:return '已发布';
                    			}
                    			return '';
                            },
                    ],
                    [
                    		'attribute' => '发布时间',
                    		'value' => function($model){
                    		if(!is_null($model['publishTime'])){
                    			return date('Y-m-d H:i:s',$model['publishTime']/1000);
                    		}else{
                    			return '';
                    		}
                            },
                    ],
                    [
                    		'class' => 'yii\grid\ActionColumn',
                    		'template' => '{update}',
                    		'buttons' => [
                    				'update' => function ($url, $model, $key) {
                    				if($model['boardingStatus']=='0'){
                    					return Html::a('发布', ['publish', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
                    				}else if($model['boardingStatus']=='1'){
                    					return Html::a('撤销发布', ['depublish', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
                    				}
                    		}
                    		],
                   ],
                   [
                   		'class' => 'yii\grid\ActionColumn',
                   		'template' => '{update}',
                   		'buttons' => [
                   				'update' => function ($url, $model, $key) {
                   					return Html::a('修改', ['update', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
                   		}
                   		],
                   		],
                   		[
                   				'class' => 'yii\grid\ActionColumn',
                   				'template' => '{update}',
                   				'buttons' => [
                   						'update' => function ($url, $model, $key) {
                   						return Html::a('预览', ['view', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
                   		}
                   		],
                   		],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{delete}',
    					'buttons' => [
    							'delete' => function ($url, $model, $key) {
    								return Html::a('删除', ['delete', 'id' => $model['id']], 
    								['class'=>'btn btn-sm btn-danger','data' => [
	    								'confirm' => '你确定删除这条轮播吗?',
	    								'method' => 'post',
    									],
    								]);
    								}
    					],
    				]
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
