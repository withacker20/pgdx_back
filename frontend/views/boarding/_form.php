<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Boarding */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boarding-form">

    <?php $form = ActiveForm::begin(['id'=>'boarding-form','enableAjaxValidation' => true,'validateOnBlur'=>false,'validateOnChange'=>false,]); ?>

	<input type="hidden" id="bid" value="<?php echo $model->id?>"/>
	<input type="hidden" id="rid" value="<?php echo $model->refId?>"/>
	<input type="hidden" id="refName" name="BoardingSearch[refName]"/>
	
    <?= $form->field($model, 'boardingName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'boardingOrder')->textInput() ?>

    <?= $form->field($model, 'boardingStatus')->dropDownList(array(''=>'--请选择--','1'=>'是','0'=>'否')) ?>

    <?= $form->field($model, 'boardingType')->dropDownList(array(''=>'--请轮播类型--','cm'=>'精品课程', 'cm1'=>'大咖说','n'=>'新闻'),[
    		'onchange'=>'
					$.post("index.php?r=boarding/drop-down-list&boardingType="+$(this).val(),function(data){
						$("#boardingsearch-refid").html("<option value=\"\">请选择关联对象</option>");
						for(var key in data){
							$("#boardingsearch-refid").append("<option value=\""+key+"\">"+data[key]+"</option>");
						}
					});
				',
    ]) ?>
    
    <?= $form->field($model, 'refId')->dropDownList(array(''=>'请选择关联对象'),[
    		'onchange'=>'
						$("#refName").val($("#boardingsearch-refid").val());
				',
    ]) ?>

    <?= $form->field($model, 'attachId')->hiddenInput()->hint('建议尺寸：750*336像素') ?>
    
    <?php 
		echo FileInput::widget([
				'name' => 'uploadfile',
				'options'=>[
						'multiple'=>false,
						'accept' => 'image/*'
				],
				'pluginOptions' => [
						'showClose' => false,
						'initialPreview'=>[
								isset($model->attach) ? $model->attach->attachFullPath : null
						],
						'initialPreviewAsData'=>true,
						'initialPreviewShowDelete'=>false,
						'initialCaption'=> isset($model->attach) ? $model->attach->orgFileName : null,
						'initialPreviewConfig' => [
								['caption' => isset($model->attach) ? $model->attach->orgFileName : null, 'size' => isset($model->attach) ? $model->attach->attachSize * 1024 *1024 : null],
						],
						'uploadUrl' => Url::toRoute(['boarding/upload']),
						'uploadAsync'=> true,
						'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
						'dropZoneTitle' => '请上传图片',
						'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
						'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
				],
				'pluginEvents' => [
						'fileuploaded' => "function(event, data, previewId, index) {
											$('#boardingsearch-attachid').val(data.response['id']);
		 								}",
						'fileremoved' => "function(event, data, previewId, index) {
											$('#boardingsearch-attachid').val('');
		 								}",
						'change' => "function(event, data, previewId, index) {
											$('#coursemainsearch-coverid').val('');
		 								}",
				],
		]);
	?>
	<br/>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs('
       $(document).ready(
		function() {
			if ($("#bid").val()) {
				var btype = $("#boardingsearch-boardingtype").val();
				var rid = $("#rid").val();
				$.post("index.php?r=boarding/drop-down-list&boardingType="
						+ btype, function(data) {

					$("#boardingsearch-refid").html(
							"<option value=\"\">请选择关联对象</option>");
					for ( var key in data) {

						if (key == rid) {
							$("#boardingsearch-refid").append(
									"<option selected value=\"" + key + "\">" + data[key]
											+ "</option>");
						} else {
							$("#boardingsearch-refid").append(
									"<option value=\"" + key + "\">" + data[key]
											+ "</option>");
						}
					}

				});
			}
		});
    ');
    ?>
