<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoardingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boarding-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'boarding-search',
        'options' => [
            'data-pjax' => 1
        ],
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

    <?= $form->field($model, 'boardingName') ?>


    <?php // echo $form->field($model, 'boarding_status') ?>

    <?php // echo $form->field($model, 'publish_time') ?>

    <?php // echo $form->field($model, 'ref_id') ?>

    <?php // echo $form->field($model, 'boarding_type') ?>

    <?php // echo $form->field($model, 'attach_id') ?>

    <?php // echo $form->field($model, 'isdel') ?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
		<?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("boarding-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
