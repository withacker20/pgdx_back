<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php echo $this->render('wx_search', ['model' => $userSearch]); ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => '序号',],
            [
                'attribute' => '用户昵称',
                'value' => function ($model) {
                    return base64_decode($model['weinickname']);
                },
            ],
            [
                'attribute' => '真实姓名',
                'value' => function ($model) {
                	return $model['realName'];
                },
            ],
            [
                'attribute' => '部门',
                'value' => function ($model) {
                    if ($model['organization']['orgName']) {

                        return $model['organization']['orgName'];
                    } else {
                        return '';
                    }
                },
            ],
            [
                'attribute' => '认证状态',
                'value' => function ($model) {
                    if ($model['isReal'] == '0') {
                        return '未认证';
                    } else if ($model['isReal'] == '1') {
                        return '已认证';
                    } else if ($model['isReal'] == '2') {
                        return '认证未通过';
                    } else if ($model['isReal'] == '3') {
                        return '认证中';
                    } else {
                        return '';
                    }
                },
            ],
            [
                'attribute' => '思考权限状态',
                'value' => function ($model) {
                    if ($model['thinkAccess'] == '0') {
                        return '关闭';
                    } else {
                        return '开启';
                    }
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '认证',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        if ($model['isReal'] == '3') {
                            return Html::a('通过', ['real-pass', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']) . ' ' . Html::a('不通过', ['real-nopass', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                        } else {
                            '';
                        }
                    }
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '思考权限操作',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        if ($model['thinkAccess'] == '0') {
                            return Html::a('开启', ['think-access', 'id' => $model['id'], 'thinkAccess' => '1'], ['class' => 'btn btn-sm btn-primary']);
                        } else {
                            return Html::a('关闭', ['think-access', 'id' => $model['id'], 'thinkAccess' => '0'], ['class' => 'btn btn-sm btn-primary']);
                        }
                    }
                ],
                'options' => [
                    'width' => '5'
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'header' => '禁止用户登录',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if ($model['userStatus'] == '0') {
                            return Html::a('禁用', ['disable-user', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                        } else if ($model['userStatus'] == '1') {
                            return Html::a('启用', ['enable-user', 'id' => $model['id']], ['class' => 'btn btn-sm btn-primary']);
                        } else {
                            return '';
                        }
                    },
                ],
                'options' => [
                    'width' => 5
                ]
            ],
        ],
    ]); ?>
    <?php
    echo LinkPager::widget([
        'pagination' => $page,
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>