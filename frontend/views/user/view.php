<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title = '用户详情';
$this->params['breadcrumbs'][] = ['label' => '用户管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-ldap-view">
	<p>
        <?= Html::a('返回', Url::to(['/user/index']), ['class' => 'btn btn-primary']) ?>
    </p>

    <h3><?= Html::encode('基本信息') ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cn',
          [
                'label'=>'头像',
                'format'=>'html',
                'value'=>function($model){
  if(isset($model['headimgurl'])) {
    return Html::img($model->headimgurl,['style'=>'heigth:100px;width:100px;']);
  }
}
],
[
		'label'=>'性别',
		'value'=>function($model){
		if($model['sex'] == '1') {
			return '男';
		} else if($model['sex'] == '2') {
			return '女';
		} else {
			return '';
		}
}
],
            'birthdayyear',
          [
                'label'=>'绑定微信',
                'value'=>function($model){
  if (isset($model['unionid'])) {
  	return '是';
  } else {
  	return '否';
  }
}
],
          [
                'label'=>'连续学习天数',
                'value'=>function($model){
  return '';
}
],
          [
                'label'=>'使用总时长',
                'value'=>function($model){
  return '';
}
],
        ],
    ]) ?>
    <h3><?= Html::encode('会员信息') ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          [
                'label'=>'购买日期',
                'format'=>'html',
                'value'=>function($model){
  return '';
}
],
          [
                'label'=>'到期日期',
                'format'=>'html',
                'value'=>function($model){
  return '';
}
],
          [
                'label'=>'套餐',
                'format'=>'html',
                'value'=>function($model){
  return '';
}
],
        ],
    ]) ?>
    <h3><?= Html::encode('认证信息') ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'realname',
            'realcompany',
            'realtype',
            'realposition',
          [
                'label'=>'认证状态',
                'format'=>'html',
                'value'=>function($model){
  if ($model['realstatus'] == '1') {
    return "认证中";
  } else if ($model['realstatus'] == '2') {
    return "认证通过";
  } else if ($model['realstatus'] == '3') {
    return "认证不通过";
  } else {
    return "未认证";
  }
}
],
        ],
    ]) ?>
    <h3><?= Html::encode('消费记录') ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          [
                'label'=>'账户余额',
                'value'=>function($model){
  return '';
}
],
        ],
    ]) ?>
    
    <?= GridView::widget([
    		'dataProvider' => $model->purchaseList,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'序号'],
        	[
	        			'attribute' => '购买时间',
	        			'value' => function($model){
	        			if(!is_null($model['buyTime'])){
        					return date('Y-m-d H:i:s',$model['buyTime']/1000);
	        			}else{
        				    return '';
        				}
		    },
		    ],
		    [
		    		'attribute' => '产品',
		    		'value' => function($model){
		    		return $model['productName'];
		    		},
		    ],
		    [
		    		'attribute' => '金额',
		    		'value' => function($model){
		    		return $model['amount'];
		    },
		    ],
		    [
		    		'attribute' => '渠道',
		    		'value' => function($model){
		    		if($model['payType'] == '2000'){
		    			return '余额';
		    		}else if($model['payType'] == '2001'){
		    			return '支付宝';
		    		} else if($model['payType'] == '2002'){
		    			return '微信';
		    		} else {
		    			return 'IOS';
		    		}
			    },
		    ],
        ],
    ]); ?>
</div>