<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'user-search',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>
    
     <?php
       
		echo '<label>注册时间</label>';
		echo DatePicker::widget([
			'name' => 'UserLDAPSearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'UserLDAPSearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>

    <?= $form->field($model, 'cn') ?>
    <?= $form->field($model, 'realstatus')->dropDownList([''=>'所有','0'=>'未认证','1'=>'认证中','2'=>'已认证','3'=>'未通过']) ?>
    

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("user-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
