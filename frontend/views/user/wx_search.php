<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['wx-index'],
        'method' => 'get',
        'id' => 'wx-user-search',
        'validateOnChange' => false,
        'validateOnSubmit' => false,
        'validateOnBlur' => false,
    ]); ?>

    <?php
       
		echo '<label>注册时间</label>';
		echo DatePicker::widget([
			'name' => 'UserSearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'UserSearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		        //'format' => 'yyyy-M-dd',
		        'todayHighlight' => true
		    ]
		]);
	?>

    <?= $form->field($model, 'weinickname') ?>
    <?= $form->field($model, 'type')->dropDownList([''=>'--请选择--','0'=>'品观大学用户','2'=>'新青年用户','1'=>'后台用户',]) ?>
    <?= $form->field($model, 'isReal')->dropDownList([''=>'--请选择--','1'=>'已实名','0'=>'未实名']) ?>
    <?= $form->field($model, 'thinkAccess')->dropDownList([''=>'--请选择--','1'=>'开启','0'=>'关闭']) ?>
    
    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default', 'onclick' => 'resetSearchForm("wx-user-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>