<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '实名认证';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
	
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
        		['class' => 'yii\grid\SerialColumn','header'=>'序号','headerOptions' => ['width' => '9%']],
        		[
        				'attribute' => '昵称',
        				'value' => function($model){
        				if (isset($model['weinickname'])) {
        					return $model['weinickname'];
        				} else if (isset($model['cn'])){
        					return $model['cn'];
        				} else {
        					return '';
        				}
        		},
        		],
        		[
        				'attribute' => '手机号',
        				'value' => function($model){
        				if (isset($model['telephoneNumber'])) {
        					return $model['telephoneNumber'];
        				} else {
        					return '';
        				}
        		},
        		],
        		[
        				'attribute' => '真实姓名',
        				'value' => function($model){
        				if (isset($model['realname'])) {
        					return $model['realname'];
        				} else {
        					return '';
        				}
        		},
        		],
        		[
        				'attribute' => '公司名称',
        				'value' => function($model){
        				if (isset($model['realcompany'])) {
        					return $model['realcompany'];
        				} else {
        					return '';
        				}
        		},
        		],
		    [
		    		'attribute' => '公司类型',
		    		'value' => function($model){
		    		if (isset($model['realtype'])) {
		    			return $model['realtype'];
		    		} else {
		    			return '';
		    		}
		    },
		    ],
		    [
		    		'attribute' => '职位',
		    		'value' => function($model){
		    		if(isset($model['realposition'])){
		    			return $model['realposition'];
		    			}else{
	    					return '';
	    				}
		    },
		    ],
		    [
		    		'header'=>'名片/执照',
		    		'class' => 'yii\grid\Column',
		    		'content' => function($model){
		    		if (isset($model['realimgurl'])) {
		    			return Html::img($model['realimgurl'],['class'=>'littleImg','data-imgurl'=>$model['realimgurl'],'data-toggle' => 'modal','data-target' => '#view-img','style'=>'width:100px;height:50px;']);
		    		}
		    },
		    
		    ],
		    [
		    		'header'=>'认证状态',
		    		'class' => 'yii\grid\Column',
		    		'content' => function($model){
		    		if ($model['realstatus'] == '1') {
		    			return "认证中";
		    		} else if ($model['realstatus'] == '2') {
		    			return "认证通过";
		    		} else if ($model['realstatus'] == '3') {
		    			return "认证不通过";
		    		} else {
		    			return "未认证";
		    		}
		    },
		    
		    ],
		    
		    [
		    		'class' => 'yii\grid\ActionColumn',
		    		'header'=>'认证操作',
		    		'template' => '{pass}&nbsp;{nopass}',
		    		'buttons' => [
		    				'pass' => function ($url, $model, $key) {
			    				if($model['realstatus']=='1'){
			    					return Html::a('通过', ['real-pass','uid'=>$model['uid']], ['class' => 'btn btn-primary']);
			    				}
		    				},
		    				'nopass' => function ($url, $model, $key) {
		    				if($model['realstatus']=='1'){
		    					return Html::a('不通过', ['real-nopass','uid'=>$model['uid']], ['class' => 'btn btn-primary']);
		    				}
		    				},
		    		],
		    ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>