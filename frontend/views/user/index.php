<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '用户管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
	<?php  echo $this->render('_search', ['model' => $userSearch]); ?>
	
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'序号'],
        	[
	        			'attribute' => '用户昵称',
	        			'value' => function($model){
	        			if (isset($model['cn'])){
	        				return $model['cn'];
	        			} else {
	        				return '';
	        			}
		    },
		    ],
		    [
		    		'attribute' => '手机号',
		    		'value' => function($model){
		    			return $model['telephoneNumber'];
		    		},
		    ],
		    [
		    		'attribute' => '注册时间',
		    		'value' => function($model){
// 		    		if(!is_null($model['createTime'])){
// 		    			return date('Y-m-d H:i:s',$model['createTime']/1000);
// 		    		}else{
// 		    			return '';
// 		    		}
			return '';
		    },
		    ],
		    [
		    		'attribute' => '认证状态',
		    		'value' => function($model){
		    		if ($model['realstatus'] == '1') {
		    			return "认证中";
		    		} else if ($model['realstatus'] == '2') {
		    			return "认证通过";
		    		} else if ($model['realstatus'] == '3') {
		    			return "认证不通过";
		    		} else {
		    			return "未认证";
		    		}
			    },
		    ],
		    [
		    		'attribute' => '身份',
		    		'value' => function($model){
		    		return '';
		    },
		    ],
		    [
		    		'attribute' => '购买时间',
		    		'value' => function($model){
// 		    		if(!is_null($model['createTime'])){
// 		    			return date('Y-m-d H:i:s',$model['createTime']/1000);
// 		    		}else{
// 		    			return '';
// 		    		}
return '';
		    },
		    ],
		    [
		    		'attribute' => '到期时间',
		    		'value' => function($model){
		    		return '';
		    },
		    ],
		    [
		    		'attribute' => '账户余额',
		    		'value' => function($model){
		    		return '';
		    },
		    ],
		    [
		    		'class' => 'yii\grid\ActionColumn',
		    		'header'=>'',
		    		'template' => '{update}&nbsp;{view}',
		    		'buttons' => [
		    				'update' => function ($url, $model, $key) {
			    					return Html::a('禁用', ['enable-teacher'], ['class'=>'btn btn-sm btn-warning']);
		    				},
		    				'view' => function ($url, $model, $key) {
		    				return Html::a('查看详情', ['view', 'id'=>$model['uid']], ['class'=>'btn btn-sm btn-info']);
		    				}
		    		],
		    ],
		    
        ],
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
