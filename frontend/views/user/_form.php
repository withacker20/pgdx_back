<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['class'=>'']); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weiid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weiphoto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weinickname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weisex')->textInput() ?>

    <?= $form->field($model, 'weiprovince')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weicity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weicountry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weiprivilege')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unionid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_teacher')->textInput() ?>

    <?= $form->field($model, 'real_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'introduction')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'statistics_id')->textInput() ?>

    <?= $form->field($model, 'org_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
