<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-from">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'teacherName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'companyName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'intro')->textArea(['rows' => '6','maxlength' => true]) ?>
	
	<?= $form->field($model, 'avatarId')->hiddenInput() ?>
	<?php 
    	if($model->avatar != null){
    ?>
    		<img src="<?=$model->avatar->attachFullPath ?>" style="heigth:100px;width:100px;"></img>
    <?php 
    	}
    ?>
	<?php 
		echo FileInput::widget([
			'name' => 'uploadfile',
			'options'=>[
					'multiple'=>false,
					'accept' => 'image/jpeg,image/png',
			],
			'pluginOptions' => [
					'allowedFileExtensions'=> ['jpg','png'],
					'uploadUrl' => Url::toRoute(['course-main/upload']),
					'uploadAsync'=> true,
					'uploadExtraData' => [
							'album_id' => 20,
							'cat_id' => 'Nature'
					],
					'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
					'browseLabel' => '浏览',
					'removeLabel' => '删除',
					'uploadLabel' => '上传',
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) { 
											$('#teachersearch-avatarid').val(data.response['id']);
		 								}",
			],
		]);
	?>
	
	<br>
    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
