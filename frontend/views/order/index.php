<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = '订单管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				
    				[
    						'header'=>'订单号',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return $model['orderNumber'];
                            },
                            
    				],
    				[
    						'attribute' => '产品名称',
    						'content' => function($model){
    							return Html::tag('span',Html::encode($model['product']['productName']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                    ],
    				[
    						'attribute' => '金额',
    						'content' => function($model){
    							return $model['totalAmount'];
                            },
                    ],
    				[
    						'attribute' => '购买时间',
    						'value' => function($model){
    						if(!is_null($model['createTime'])){
	    							return date('Y-m-d H:i:s',$model['createTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                    ],
					[
						'attribute' => '用户昵称',
						'content' => function($model){
								return Html::tag('span',Html::encode($model['username']),['style'=>'width:100px', 'class'=>'ellipsis']);
					},
			],
			[
						'attribute' => '手机号',
						'content' => function($model){
								return $model['phoneNumber'];
					},
			],
                    [
                    		'attribute' => '状态',
                    		'value' => function($model){
	                    		if($model['orderStatus'] == '1000'){
		                    		return '待付款';
	                    		}else if($model['orderStatus'] == '1001'){
		                    		return '已付款';
		                    	} else if($model['orderStatus'] == '1003'){
		                    		return '已过期';
		                    	} else {
		                    		return '已取消';
		                    	}
                    		},
                    ],
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
			'pagination' => $page,
			'firstPageLabel' => '首页', 
			'lastPageLabel' => '尾页',
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
