<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

$this->title = '查看讲师';
$this->params['breadcrumbs'][] = ['label' => '讲师管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('返回', Url::to(['/teacher/index']), ['class' => 'btn btn-primary']) ?>
    </p>
	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          	'teacherName',
            'companyName',
            'title',
            'intro',
        ],
    		'options' => ['class' => 'table table-striped table-bordered detail-view', 'style'=>'word-break:break-all;'],
    ]) ?>
    <?php 
    	if($model->avatar != null){
    ?>
    		<img src="<?=$model->avatar->attachFullPath ?>" style="heigth:100px;width:100px;"></img>
    <?php 
    	}
    ?>
</div>
