<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin(['id'=>'order-form',
        'action' => ['index'],
        'id' => 'order-search',
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>购买时间</label>';
		echo DatePicker::widget([
			'name' => 'OrderSearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'OrderSearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'orderNumber') ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'phoneNumber') ?>
    <?= $form->field($model, 'productName') ?>
    <?= $form->field($model, 'orderStatus')->dropDownList(array(''=>'所有','1000'=>'待付款','1001'=>'已付款'))?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("order-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
