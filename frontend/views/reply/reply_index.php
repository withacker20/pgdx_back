<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use kartik\editable\Editable;

$this->title = '留言回复';
$this->params['breadcrumbs'][] = ['label' => '留言管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-index">
    <h1>课程名称：<?= Html::encode($courseMain['courseMainName']) ?></h1>
    <?php echo $this->render('reply_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'style' => 'overflow: auto; word-wrap: break-word;'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => '序号'],
            [
                'header' => '留言内容',
                'class' => 'yii\grid\Column',
                'content' => function ($model) {
                	return Html::tag("div", Html::encode(base64_decode($model['replyContent'])), ['title' => base64_decode($model['replyContent']), 'style' => 'width:200px;']);
                },
            ],
            [
                'attribute' => '留言时间',
                'options' => ['width' => '10'],
                'value' => function ($model) {
                    if (!is_null($model['createTime'])) {
                        return date('Y-m-d H:i:s', $model['createTime'] / 1000);
                    } else {
                        return '';
                    }
                },
            ],
            [
                'attribute' => '留言用户',
                'value' => function ($model) {
                    return base64_decode($model['user']['weinickname']);
                },
            ],
            [
                'attribute' => '状态',
                'content' => function ($model) {
                    if ($model['replyStatus'] == '0') {
                        return Html::tag('span', '未回复', ['id' => 'status' . $model['id']]);
                    } else {
                        return Html::tag('span', '已回复', ['id' => 'status' . $model['id']]);
                    }
                },
            ],
            [
                'header' => '回复内容',
                'class' => 'yii\grid\Column',
                'content' => function ($model) {
                    return Editable::widget([
                        'name' => 'ReplySearch[replyContent]',
                        'pluginEvents' => ["editableSuccess" => "addReply"],
                        'ajaxSettings' => ['url' => isset($model['replyList'][0]) ? Url::toRoute(['reply/update', 'id' => $model['replyList'][0]['id'], 'questoinId' => $model['id'],'courseMainId' => $model['courseMainId']]) : Url::toRoute(['reply/create', 'questoinId' => $model['id'],'courseMainId' => $model['courseMainId']])],
                        'asPopover' => true,
                    		'value' => isset($model['replyList'][0]) ? Html::encode(base64_decode($model['replyList'][0]['replyContent'])) : '',
                        'header' => '回复内容',
                        'size' => 'md',
                        'inputType' => Editable::INPUT_TEXTAREA,
                        'editableValueOptions' => ['class' => 'kv-editable-value kv-editable-link ellipsis', 'style' => 'width:100px;'],
                    	'options' => ['class' => 'form-control','maxlength'=>1024]
                    ]);
                },
            ],
            [
                'attribute' => '回复时间',
                'options' => ['width' => '10'],
                'content' => function ($model) {
                    if (isset($model['replyList'][0])) {
                        return Html::tag('span', date('Y-m-d H:i:s', $model['replyList'][0]['updateTime'] / 1000), ['id' => 'updateTime' . $model['id']]);
                    } else {
                        return Html::tag('span', '', ['id' => 'updateTime' . $model['id']]);
                    }
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        return Html::a('删除', ['delete', 'id' => $model['id'], 'courseMainId' => $model['courseMainId']],
                            ['class' => 'btn btn-sm btn-danger', 'data' => [
                                'confirm' => '你确定删除这条留言吗?',
                                'method' => 'post',
                            ],
                            ]);
                    }
                ],
            ]
        ],
    ]); ?>

    <?php
    echo LinkPager::widget([
        'pagination' => $page,
    ]);
    ?>

</div>
<?php $this->beginBlock('javascript-block') ?>
function addReply(event, val, form, data) {
$('#status'+data.id).html('已回复');
$('#updateTime'+data.id).html(data.updateTime);
}
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
