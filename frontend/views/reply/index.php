<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '留言管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-index">
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
        		['class' => 'yii\grid\SerialColumn','header'=>'序号'],
        		[
        				'header'=>'课程名称',
        				'class' => 'yii\grid\Column',
        				'content' => function($model){
        				return Html::a(Html::encode($model['courseMainName']),Url::to(['/reply/reply-index','courseMainId'=>$model['id']]), ['title'=>$model['courseMainName'],'class'=>'ellipsis','style'=>'width:200px;']);
			    },
			    ],
			    [
			    		'attribute' => '讲师',
			    		'value' => function($model){
			    		return $model['teacher']['teacherName'];
			    },
			    ],
			    [
			    		'attribute' => '留言数量',
			    		'value' => function($model){
			    		return $model['replyCount'];
			    },
			    ],
			    [
			    		'attribute' => '发布时间',
			    		'value' => function($model){
			    		if(!is_null($model['publishTime'])){
			    			return date('Y-m-d H:i:s',$model['publishTime']/1000);
			    		}else{
			    			return '';
			    		}
			    },
			    ],
			    [
			    		'attribute' => '最新留言时间',
			    		'value' => function($model){
			    		if(!is_null($model['replyLatestTime'])){
			    			return date('Y-m-d H:i:s',$model['replyLatestTime']/1000);
			    		}else{
			    			return '';
			    		}
			    },
			    ],
			    [
			    		'class' => 'yii\grid\ActionColumn',
			    		'template' => '{update}',
			    		'buttons' => [
			    				'update' => function ($url, $model, $key) {
			    				if ($model['replyCount'] > 0) {
			    					return Html::a('管理留言', ['reply-index', 'courseMainId' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
			    				}
			    		}
			    		],
			    ],
        ],
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    
    <?php Pjax::end(); ?>
</div>
