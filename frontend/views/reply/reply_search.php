<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ReplySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reply-index-search">

    <?php $form = ActiveForm::begin([
        'action' => ['reply-index'],
        'method' => 'get',
        'id' => 'reply-index-search',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


   <?php
       
		echo '<label>留言发布时间</label>';
		echo DatePicker::widget([
			'name' => 'ReplySearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'ReplySearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
	
	<br>
	
	<?= $form->field($model, 'replyStatus')->dropDownList(array(''=>'所有','0'=>'未回复','1'=>'已回复'))?>



    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("reply-index-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
