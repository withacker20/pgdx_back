<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ReplySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reply-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'reply-search',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>


   <?php
       
		echo '<label>发布时间</label>';
		echo DatePicker::widget([
			'name' => 'CourseMainSearch[publishTimeStart]',
			'value' => $model->publishTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'CourseMainSearch[publishTimeEnd]',
			'value2' => $model->publishTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>


    <?= $form->field($model, 'courseMainName') ?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("reply-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
