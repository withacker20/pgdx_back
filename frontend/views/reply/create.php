<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Reply */

$this->title = 'Create Reply';
$this->params['breadcrumbs'][] = ['label' => 'Replies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reply-create">


    <?= $this->render('_form', [
        'model' => $model,
    		'question' => $question,
    ]) ?>

</div>
