<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AppAsset;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?=DetailView::widget ( [ 'model' => $question,'attributes' => [ 
    		[ 'label' => '留言内容','format' => 'html','value' => function ($model) {return base64_decode ( $model ['replyContent'] );} ],
    		[
    				'label'=>'留言时间',
    				'value'=>function($model){
    				return date('Y-m-d H:i:s',$model['createTime']/1000);
    		}
    		],
    		[
    				'label'=>'留言用户',
    				'value'=>function($model){
    				return base64_decode($model['user']['weinickname']);
    		}
    		],
    		[
    				'label'=>'留言状态',
    				'value'=>function($model){
    				if($model['replyStatus'] == '0') {
    					return '未回复';
    				} else {
    					return '已回复';
    				}
    		}
    		],
    		] ] )?>

    <?= $form->field($model, 'replyContent')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'userId')->hiddenInput(['value'=>1])->label(false)?>



    <div class="form-group">
        <?= Html::submitButton('回复', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

	<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
</div>
