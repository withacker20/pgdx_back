<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

$this->title = '消息管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('新增消息', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				[
    						'header'=>'消息标题',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return Html::tag('span',Html::encode($model['msgTitle']),['style'=>'width:100px', 'class'=>'ellipsis']);
                            },
                            
    				],
    				[
    						'attribute' => '录入时间',
    						'value' => function($model){
	    						if(!is_null($model['createTime'])){
	    							return date('Y-m-d H:i:s',$model['createTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                    ],
    				[
    						'attribute' => '类型',
    						'value' => function($model){
	    						if($model['msgType'] == 'news'){
	    							return '资讯';
	    						}else if ($model['msgType'] == 'course'){
	    							return '课程';
	    						} else if ($model['msgType'] == 'meeting'){
	    							return '零售峰会';
	    						} else if ($model['msgType'] == 'pg') {
	    							return '品观';
	    						}
                            },
                    ],
    				[
    						'attribute' => '设备',
    						'value' => function($model){
    						if($model['deviceType'] == 'android'){
	    							return 'Android';
	    						}else if ($model['deviceType'] == 'ios'){
	    							return 'IOS';
	    						} else {
	    							return 'IOS/Android';
	    						}
                            },
                    ],
                    [
                    		'attribute' => '发送状态',
                    		'value' => function($model){
                    		if ($model['msgStatus'] == 'send') {
                    			return "立即发送";
                    		} else if ($model['msgStatus'] == 'wait') {
                    			return "定时发送";
                    		} else if ($model['msgStatus'] == 'fail'){
                    			return "发送失败";
                    		} else if ($model['msgStatus'] == 'init'){
                    			return "未发送";
                    		}
                    },
                    ],
    				[
    						'attribute' => '推送时间',
    						'value' => function($model){
    						if(!is_null($model['sendTime'])){
	    							return date('Y-m-d H:i:s',$model['sendTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                    ],
//     				[
//     						'attribute' => '到达人数',
//     						'value' => function($model){
//     							return $model['arrive'];
//                             },
//                     ],
//     				[
//     						'attribute' => '点击人数',
//     						'value' => function($model){
//     							return $model['click'];
//                             },
//                     ],
                    [
                    		'attribute' => '用户类型',
                    		'value' => function($model){
	                    		if($model['userType'] == 'all'){
	                    			return '全员';
	                    		}else if ($model['userType'] == 'vip'){
	                    			return 'VIP';
	                    		} else {
	                    			return '普通用户';
	                    		}
                    		},
                    ],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{send}&nbsp;{update}&nbsp;{view}&nbsp;{delete}',
    					'buttons' => [
    						'send' => function ($url, $model, $key) {
    							if ($model['msgStatus'] == 'init' || $model['msgStatus'] == 'fail') {
    								return Html::a('发送', ['#'], ['data-toggle' => 'modal','data-target' => '#send-option','data-id' => $model['id'],'class'=>'btn btn-sm btn-primary send-button']);
    							} else if ($model['msgStatus'] == 'wait' && $model['sendTime']/1000 > time()) {
	    							return Html::a('取消发送', ['cancel-send', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
	    						}
	    					},
	    					'update' => function ($url, $model, $key) {
	    						if ($model['msgStatus'] == 'init') {
	    							return Html::a('修改', ['update', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
	    						}
	    					},
	    					'view' => function ($url, $model, $key) {
	    					if ($model['msgStatus'] != 'init') {
		    					return Html::a('预览', ['view', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
	    					}
	    					},
	    					'delete' => function ($url, $model, $key) {
	    						return Html::a('删除', ['delete', 'id' => $model['id']],
	    							['class'=>'btn btn-sm btn-danger','data' => [
	    									'confirm' => '你确定删除吗?',
	    									'method' => 'post',
	    							],
	    							]);
	    					}
    					],
    				],
    		]
      
    ]); ?>
    
    <?php 
        Modal::begin([
            'id' => 'send-option',
            'header' => '<h4 class="modal-title">消息发送</h4>',
            
        ]); 
        
        echo $this->render('send_form', ['model' => $model]);
        Modal::end(); 
    ?>
    
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>