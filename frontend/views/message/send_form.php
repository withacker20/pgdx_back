<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-from">

    <?php $form_send = ActiveForm::begin(['id'=>'send-form','action'=>Url::toRoute(['message/send']),'validateOnChange'=> false,
    		'validateOnBlur'=>false,'enableAjaxValidation' => true]) ?>
	
	<?= $form_send->field($model, 'id')->hiddenInput(['id'=>'sendId'])->label(false)?>

	<?= $form_send->field($model, 'msgStatus')->radioList(['send'=>'直接发送','wait'=>'定时发送'],['onchange'=>'changeSend($(this))'])->label(false)->hint('定时发送时间只能设置当前时间5分钟以后的时间，但在设定的时间之前可取消。')?>
	
	<?= $form_send->field($model, 'sendTime')->widget(DateTimePicker::classname(), [
	        'pluginOptions' => [
	            'autoclose' => true,
	            'showSecond' => false,
	            'mode' => 'time',
	            'format' => 'yyyy-mm-dd hh:ii:ss',
	            'startDate' => date('Y-m-d H:i', time() + 300),
//         		'endDate' => date('Y-m-d H:i', strtotime( date( "Y" ). "-" . date( "m" ). "-" . date( "t" ))),
	        	
	        ]
	])->label(false);
	?>
	
	<?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
	 
	<?php ActiveForm::end() ?>

</div>
<?php $this->beginBlock('javascript-block') ?>
        $(function () {
            $('.send-button').on('click',function(){
            	$("#sendId").val($(this).data('id'));
            });
            $('.hint-block').hide();
            $('.field-messagesearch-sendtime').hide();
            $('#messagesearch-sendtime').attr('disabled', true);
        });
        function changeSend() {
	        if ($('.hint-block').is(':hidden')) {
	        	$('.hint-block').show();
	        	$('.field-messagesearch-sendtime').show();
	        	$('#messagesearch-sendtime').removeAttr("disabled");
	        } else {
	        	$('.hint-block').hide();
	        	$('.field-messagesearch-sendtime').hide();
	        	$('#messagesearch-sendtime').attr('disabled', true);
	        }
        }
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
