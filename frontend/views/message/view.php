<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CourseMain */

$this->title = $model->msgTitle;
$this->params['breadcrumbs'][] = ['label' => '消息管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-main-view">
    <p>
        <?= Html::a('返回', Url::to(['/message/index']), ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'msgTitle',
        	[
        		'label'=>'类型',
        		'value'=>function($model){
        		if($model['msgType'] == 'news'){
        			return '资讯';
        		}else if ($model['msgType'] == 'course'){
        			return '课程';
        		} else if ($model['msgType'] == 'meeting'){
        			return '零售峰会';
        		} else if ($model['msgType'] == 'pg') {
        			return '品观';
        		}
    			}
    		],
            'summary',
            'msgContent',
            'refName',
            'refUrl',
        	[
        		'label'=>'接收设备',
        		'value'=>function($model){
        			if ($model['deviceType'] == 'ios') {
        				return "IOS";
        			} else if ($model['deviceType'] == 'android') {
        				return "Android";
        			} else {
        				return "IOS/Android";
        			}
    			}
    		],
        	[
        		'label'=>'用户类型',
        		'value'=>function($model){
        			if ($model['userType'] == 'vip') {
        				return "VIP";
        			} else if ($model['userType'] == 'normal') {
        				return "普通用户";
        			} else {
        				return "全员";
        			}
    			}
    		],
        	[
        		'label'=>'发送状态',
        		'value'=>function($model){
        		if ($model['msgStatus'] == 'send') {
        			return "立即发送";
        		} else if ($model['msgStatus'] == 'wait') {
        			return "定时发送";
        		} else if ($model['msgStatus'] == 'fail'){
        			return "发送失败";
        		} else if ($model['msgStatus'] == 'init'){
        			return "未发送";
        		}
    			}
    		],
            [
            		'label'=>'发送时间',
            		'value'=>function($model){
            			if(isset($model['sendTime'])) {
            				return date('Y-m-d H:i:s',$model['sendTime']/1000);
            			}
    				}
    		],
            [
            		'label'=>'封面图片',
            		'format'=>'html',
            		'value'=>function($model){
            			if(isset($model['attach'])) {
            				return Html::img($model->attach->attachFullPath,['style'=>'heigth:100px;width:100px;']);
            			}
    				}
    		],
        ],
    ]) ?>
</div>
