<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;
?>

<div class="message-from">

    <?php $form = ActiveForm::begin(['validateOnChange'=> false,
        'validateOnBlur'=>false,]); ?>

    <?= $form->field($model, 'msgTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'msgType')->dropDownList(array(''=>'请选择','news'=>'资讯','course'=>'精品课程','coursedaka'=>'大咖说','meeting'=>'零售峰会','pg'=>'品观'),['onchange'=>'changeType();'])?>

    <?= $form->field($model, 'summary')->textArea(['rows' => '6','maxlength' => true])?>
    <?= $form->field($model, 'msgContent')->textArea(['rows' => '6','maxlength' => true])?>

    <?= $form->field($model, 'attachId')->hiddenInput() ?>
    <?php
    echo FileInput::widget([
    		'name' => 'uploadfile',
    		'options'=>[
    				'multiple'=>false,
    				'accept' => 'image/*'
    		],
    		'pluginOptions' => [
    				'showClose' => false,
    				'initialPreview'=>[
    						isset($model->attach) ? $model->attach->attachFullPath : null
    				],
    				'initialPreviewAsData'=>true,
    				'initialPreviewShowDelete'=>false,
    				'initialCaption'=> isset($model->attach) ? $model->attach->orgFileName : null,
    				'initialPreviewConfig' => [
    						['caption' => isset($model->attach) ? $model->attach->orgFileName : null, 'size' => isset($model->attach) ? $model->attach->attachSize * 1024 *1024 : null],
    				],
    				'uploadUrl' => Url::toRoute(['course-main/upload']),
    				'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
    				'dropZoneTitle' => '请上传图片',
    				'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
    				'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
    		],
    		'pluginEvents' => [
    				'fileuploaded' => "function(event, data, previewId, index) {
											$('#messagesearch-attachid').val(data.response['id']);
		 								}",
    				'fileremoved' => "function(event, data, previewId, index) {
											$('#messagesearch-attachid').val('');
		 								}",
    				'change' => "function(event, data, previewId, index) {
											$('#messagesearch-attachid').val('');
		 								}",
    		],
    ]);
    ?>


    <?= $form->field($model, 'refId')->hiddenInput()->label(false) ?>
    <?= Html::radio("selfUrl", $model->nameFlag,['id'=>'selfUrl1','onclick'=>"clickRadio(this);"]) ?>
    <?= Html::label("落地页(课程/文章/会议)","promotionsearch-refname") ?>
    <?= $form->field($model, 'refName')->textInput(['disabled'=>true])->label(false)?>

    <?= Html::radio("selfUrl", $model->urlFlag,['id'=>'selfUrl2','onclick'=>"clickRadio(this);"]) ?>
    <?= Html::label("自定义链接","promotionsearch-refurl") ?>
    <?= $form->field($model, 'refUrl')->textInput(['disabled'=>true])->label(false)?>

    <?= $form->field($model, 'deviceType')->radioList(['all'=>'所有','ios'=>'IOS','android'=>'Android'])?>

    <?= $form->field($model, 'userType')->radioList(['all'=>'全体用户','vip'=>'VIP','normal'=>'普通用户'])?>

    <?= $form->field($model, 'submitType')->hiddenInput()->label(false) ?>

    <br>
    <div class="form-group">
        <?= Html::submitButton('保存', ['onclick'=>'$("#messagesearch-submittype").val("");','class' => 'btn btn-success']) ?>
        <?= Html::submitButton('保存并发送', ['onclick'=>'$("#messagesearch-submittype").val("send");','class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
Modal::begin([
    'id' => 'lookupModal',
    'size' => 'modal-lg',
    'header' => '<h4 class="modal-title">文章库</h4>',
]);

Modal::end();
?>
    
<?php $this->beginBlock('javascript-block') ?>
$(function () {
	if ($('#messagesearch-msgtype').val() == '') {
		$('input[name="selfUrl"]').attr('disabled', true);
	}
    $('.send-button').on('click',function(){
        $("#sendId").val($(this).data('id'));
    });
    if ('<?= $model->urlFlag?>' == 1) {
    	disableName();
    }
    if ('<?= $model->nameFlag?>' == 1) {
    	disableUrl();
    }
    changeType();
    
});

function disableUrl() {
	$('#selfUrl1').attr('checked', 'checked');
	$('#messagesearch-refname').removeAttr("disabled");
    $('#messagesearch-refurl').attr('disabled', true);
    $('#messagesearch-refurl').val('');
}

function disableName() {
	$('#selfUrl2').attr('checked', 'checked');
	$('#messagesearch-refurl').removeAttr("disabled");
    $('#messagesearch-refname').attr('disabled', true)
    $('#messagesearch-refname').val('');
    $('#messagesearch-refid').val('');
}
function clickRadio(obj) {
    if ($(obj).attr('id') == 'selfUrl1') {
        disableUrl();
    } else {
        disableName();
    }
}

function changeType() {
    if ($('#messagesearch-msgtype').val() == 'course') {
        $('.modal-title').html('精品课程');
        $('#messagesearch-refname').unbind("focus");
        $('#messagesearch-refname').on('focus',function(){
            $.get('<?=Url::toRoute('message/course-index') ?>', {},
                function (data) {
                    $('#lookupModal').find('.modal-body').html(data);
                    $('#lookupModal').modal('show');
                }
        	);
        });
        $('input[name="selfUrl"]').removeAttr("disabled");
        disableUrl();
    } else if ($('#messagesearch-msgtype').val() == 'coursedaka') {
        $('.modal-title').html('大咖说');
        $('#messagesearch-refname').unbind("focus");
        $('#messagesearch-refname').on('focus',function(){
            $.get('<?=Url::toRoute(['message/course-index', 'type' => '1']) ?>', {},
                function (data) {
                    $('#lookupModal').find('.modal-body').html(data);
                    $('#lookupModal').modal('show');
                }
        	);
        });
        $('input[name="selfUrl"]').removeAttr("disabled");
        disableUrl();
    } else if ($('#messagesearch-msgtype').val() == 'news') {
        $('.modal-title').html('文章库');
        $('#messagesearch-refname').unbind("focus");
        $('#messagesearch-refname').on('focus',function(){
            $.get('<?=Url::toRoute('message/news-index') ?>', {},
                function (data) {
                    $('#lookupModal').find('.modal-body').html(data);
                    $('#lookupModal').modal('show');
                }
        	);
        });
        $('input[name="selfUrl"]').removeAttr("disabled");
        disableUrl();
    } else if ($('#messagesearch-msgtype').val() == 'meeting') {
        $('.modal-title').html('会议库');
        $('#messagesearch-refname').unbind("focus");
        $('#messagesearch-refname').on('focus',function(){
            $.get('<?=Url::toRoute('message/meeting-index') ?>', {},
                function (data) {
                    $('#lookupModal').find('.modal-body').html(data);
                    $('#lookupModal').modal('show');
                }
        	);
        });
        $('input[name="selfUrl"]').removeAttr("disabled");
        disableUrl();
    } else if ($('#messagesearch-msgtype').val() == 'pg') {
        disableName();
    } else {
        $('#messagesearch-refname').unbind("focus");
        $('#messagesearch-refurl').attr('disabled', true);
        $('#messagesearch-refname').attr('disabled', true);
        $('#messagesearch-refname').val('');
        $('#messagesearch-refid').val('');
        $('input[name="selfUrl"]').attr('disabled', true);
    }
}
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
