<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'message-search',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>录入时间</label>';
		echo DatePicker::widget([
			'name' => 'MessageSearch[createTimeStart]',
			'value' => $model->createTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'MessageSearch[createTimeEnd]',
			'value2' => $model->createTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
	<?php
       
		echo '<label>推送时间</label>';
		echo DatePicker::widget([
			'name' => 'MessageSearch[sendTimeStart]',
			'value' => $model->sendTimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'MessageSearch[sendTimeEnd]',
			'value2' => $model->sendTimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'msgTitle') ?>
    <?= $form->field($model, 'msgType')->dropDownList(array(''=>'所有','news'=>'资讯','course'=>'课程','meeting'=>'零售峰会','pg'=>'品观'),['onchange'=>'test();'])?>
    <?= $form->field($model, 'msgStatus')->dropDownList(array(''=>'所有','send'=>'立即发送','wait'=>'定时发送','init'=>'未发送','fail'=>'发送失败'),['onchange'=>'test();'])?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("message-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
