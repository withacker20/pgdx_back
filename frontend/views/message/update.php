<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->params['breadcrumbs'][] = ['label' => '消息管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = '修改消息';
?>
<div class="message-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
