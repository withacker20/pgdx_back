<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-search-message">

    <?php $form = ActiveForm::begin(['id'=>'article-search-message',
        'action' => ['message/news-index'],
        'method' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

	<?php
       
		echo '<label>发布时间</label>';
		echo DatePicker::widget([
			'id' => 'publishDate',
			'name' => 'ArticleSearch[pubtimeStart]',
				'value' => $model->pubtimeStart,
			'type' => DatePicker::TYPE_RANGE,
			'name2' => 'ArticleSearch[pubtimeEnd]',
				'value2' => $model->pubtimeEnd,
		    'pluginOptions' => [
		    		'format' => 'yyyy/mm/dd',
		    		'clearBtn' => true,
		    		'autoclose' => true,
		    		'todayHighlight' => true
		    ]
		]);
	?>
    <?= $form->field($model, 'title') ?>
    
    <?= $form->field($model, 'cid')->dropDownList(ArrayHelper::map($categoryList,'id','name'))?>

    <div class="form-group">
        <?= Html::button('查询', ['id'=>'searchBtn','class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("article-search-message");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('javascript-block') ?>
        $(function () {
        	$('#searchBtn').on('click',function(){
            	$.get('<?=Url::toRoute('message/news-index') ?>', $("#article-search-message").serialize(),
		            function (data) {
		                $('#lookupModal').find('.modal-body').html(data);
		            }  
        		);
            });
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
