<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\widgets\Pjax;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->session['datasource'] ? (Yii::$app->session['datasource'] == 'wx' ? '小程序后台管理系统' : 'APP后台管理系统') : '后台管理系统',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => '首页', 'url' => ['/site/index']],
        // ['label' => '关于我们', 'url' => ['/site/about']],
        // ['label' => '联系我们', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => '注册', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => '登录', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                '注销 (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <!-- 树形菜单 -->
        <?php if (!empty(Yii::$app->user->identity->id) && (Yii::$app->user->identity->id == 3 || Yii::$app->user->identity->id == 2)) { ?>
            <div id="leftnav" class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                    <li role="presentation" class="active">
                        <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#msg_mgr" role="button"
                           aria-expanded="true">消息<span class="caret"></span></a>
                        <ul id="msg_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                            <li role="presentation"><a href="<?= Url::toRoute(['message/index']) ?>">消息管理</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                <?php } ?>

            </ul>
            <ul id="usercollapse2" class="collapse nav nav-pills nav-stacked">
                <li role="presentation"><a href="#">Profile2</a></li>
            </ul>
        </div>
        <?php } else { ?>
            <div id="leftnav" class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="active">
                    <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#course_mgr" role="button"
                       aria-expanded="true">课程管理<span class="caret"></span></a>
                    <ul id="course_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                        <li role="presentation"><a href="<?= Url::toRoute(['course-main/index']) ?>">课程管理</a></li>
                        <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                        <li role="presentation"><a href="<?= Url::toRoute(['reply/index']) ?>">留言管理</a></li>
                        <?php } else { ?>
                        <?php } ?>
                    </ul>
                </li>
                <li role="presentation" class="active">
                    <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#boarding_mgr" role="button"
                       aria-expanded="true">图文管理<span class="caret"></span></a>
                    <ul id="boarding_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                        <li role="presentation"><a href="<?= Url::toRoute(['boarding/index']) ?>">轮播发布</a></li>
                        <li role="presentation"><a href="<?= Url::toRoute(['news/index']) ?>">新闻发布</a></li>
                        <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                            <li role="presentation"><a href="<?= Url::toRoute(['promotion/view']) ?>">APP推广位</a></li>
                        <?php } else { ?>
                        <?php } ?>
                    </ul>
                </li>
                <li role="presentation" class="active">
                    <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#teacher_mgr" role="button"
                       aria-expanded="true">用户管理<span class="caret"></span></a>
                    <ul id="teacher_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                        <li role="presentation"><a href="<?= Url::toRoute(['teacher/index']) ?>">讲师管理</a></li>
                        <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                            <!-- <li role="presentation"><a href="<?= Url::toRoute(['user/index']) ?>">用户管理</a></li>-->
                            <!-- <li role="presentation"><a href="<?= Url::toRoute(['user/real-index']) ?>">实名认证</a></li> -->
                        <?php } else { ?>
                            <li role="presentation"><a href="<?= Url::toRoute(['user/wx-index']) ?>">用户管理</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                <?php } else { ?>
                    <li role="presentation" class="active">
                        <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#resource_mgr"
                           role="button" aria-expanded="true">资料库<span class="caret"></span></a>
                        <ul id="resource_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                            <li role="presentation"><a href="<?= Url::toRoute(['resource-base/index']) ?>">资料库管理</a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                    <li role="presentation" class="active">
	                    <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#trade_mgr" role="button"
	                       aria-expanded="true">交易管理<span class="caret"></span></a>
	                    <ul id="trade_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
	                        <li role="presentation"><a href="<?= Url::toRoute(['order/index']) ?>">订单管理</a></li>
	                        <li role="presentation"><a href="<?= Url::toRoute(['trade/index']) ?>">交易流水</a></li>
                            <li role="presentation"><a href="<?= Url::toRoute(['recharge-code/index']) ?>">充值码</a></li>
	                    </ul>
	                </li>
                <?php } else { ?>
                <?php } ?>
                <?php if (Yii::$app->session['datasource'] == 'app') { ?>
                    <li role="presentation" class="active">
                        <a class="dropdown-toggle list-group-item" data-toggle="collapse" href="#msg_mgr" role="button"
                           aria-expanded="true">消息<span class="caret"></span></a>
                        <ul id="msg_mgr" class="nav nav-pills nav-stacked collapse in" aria-expanded="true">
                            <li role="presentation"><a href="<?= Url::toRoute(['message/index']) ?>">消息管理</a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                <?php } ?>

            </ul>
            <ul id="usercollapse2" class="collapse nav nav-pills nav-stacked">
                <li role="presentation"><a href="#">Profile2</a></li>
            </ul>
        </div>
        <?php } ?>

        <!-- 主题内容 -->
        <div class="col-md-9">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; 品观科技 <?= date('Y') ?></p>
    </div>
</footer>
<?php $this->beginBlock('javascript-block') ?>
        function resetSearchForm(formId) {
        	$('#'+formId).find(':text').each(function(){
        		$(this).val("");
        	})
        	$('#'+formId).find('select').each(function(){
				console.log($(this).get(0).selectedIndex=0)        	
        	})
        }
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
