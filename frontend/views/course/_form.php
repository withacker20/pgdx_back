<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AppAsset;

AppAsset::register($this);
AppAsset::addCss($this,'@web/assets/ueditor/themes/default/css/ueditor.css');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.config.js');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.all.js');
AppAsset::addScript($this,'@web/assets/ueditor/lang/zh-cn/zh-cn.js');

/* @var $this yii\web\View */
/* @var $model app\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= Html::hiddenInput('CourseSearch[id]',$model->id) ?>

	<?php if($type == 'updatecontent'){ ?>
		<?= Html::activeLabel($model, 'courseName') ?>
		<?= $form->field($model, 'courseName')->render(function ($field) {
			return Html::encode($field->model['courseName']);
															}) ?>
		<?= Html::hiddenInput('CourseSearch[courseName]',$model->courseName) ?>
		
	<?php }else{ ?>
		<?= $form->field($model, 'courseName')->textInput(['maxlength' => true]) ?>
	<?php } ?>
    

    <?= $form->field($model, 'courseContent')->textarea(['rows'=>6,'id'=>'courseContent','class'=>'col-sm-1 col-md-12','style'=>'margin-top: 10px;padding:0;margin:20px 0;width:100%;height:200px;border: none;'])?>
    
	<div class="form-group">
        <?= Html::submitButton('提交', ['class' => 'btn btn-success']) ?>
        <?= Html::a('取消', ['course/view-subs','courseMainId'=>$model->courseMainId], ['class' => 'btn btn-default']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
	
	<?php $this->beginBlock('javascript-block') ?>
	        $(function () {
	            var ue = UE.getEditor('courseContent');
	        });
	<?php $this->endBlock() ?>
	<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
		
</div>
