<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = $model->courseName;
$this->params['breadcrumbs'][] = ['label' => '子课程', 'url' => ['course/view-subs', 'courseMainId' => $model->courseMainId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">
    <p>
        <?= Html::a('返回', Url::to(['/course/view-subs', 'courseMainId' => $model['courseMainId']]), ['class' => 'btn btn-primary']) ?>
    </p>
    <?php if (Yii::$app->session['datasource']== 'app') {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'courseName',
                'shareTitle',
                'shareSummary',
                [
                    'label' => '文稿',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['courseContent'];
                    }
                ],
                [
                    'label' => '试听',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model['demoFlag'] == '1') {
                            return '是';
                        } else {
                            return '否';
                        }
                    }
                ]
            ],

        ]);
    } else {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'courseName',
                [
                    'label' => '文稿',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['courseContent'];
                    }
                ]
            ],

        ]);
    } ?>

    <strong>音频文件</strong><br>
    <audio src="<?=$model['attach']['attachFullPath']?>" controls="controls">您的浏览器不支持 video 标签。</audio>
</div>