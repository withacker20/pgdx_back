<?php
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

$this->title = '上传音频';
?>

<?php $form_upload = ActiveForm::begin(['action'=>Url::toRoute(['course/upload-attach']),'id'=>'uploadForm']) ?>
<?= $form_upload->field($model, 'attachId')->hiddenInput()->label(false) ?>
<?= $form_upload->field($model, 'id')->hiddenInput()->label(false) ?>
<?= $form_upload->field($model, 'courseMainId')->hiddenInput()->label(false) ?>
<?php 
	echo FileInput::widget([
		'name' => 'uploadfile',
		'options'=>[
				'multiple'=>false,
				'required'=>true,
				'accept' => 'audio/mpeg'
		],
			'pluginOptions' => [
					'allowedFileExtensions'=> ['mp3'],
					'allowedPreviewTypes' => ['audio'],
					'uploadUrl' => Url::toRoute(['course/upload-audio']),
					'maxFileCount' => 1,
					'dropZoneTitle' => '请上传文件',
					'progressUploadThreshold' => '99',
					'maxFileSize'=>71680,
					'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
			],
			'pluginEvents' => [
					'fileuploaded' => "function(event, data, previewId, index) {
											$('#coursesearch-attachid').val(data.response['id']);
											$('#uploadForm').submit();
		 								}",
			],
	]);
?>
<?php ActiveForm::end() ?>