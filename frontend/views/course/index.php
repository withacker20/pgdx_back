<?php

use yii\helpers\Html;
use kartik\editable\Editable;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '子课程列表';
$this->params['breadcrumbs'][] = ['label' => '精品课程', 'url' => ['course-main/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">
    <?= DetailView::widget([
        'model' => $courseMain,
        'attributes' => [
            'courseMainName',

        ],
    ]) ?>
    <p>
        <?= Html::a('添加小课', ['#'], [
            'id' => 'create',
            'data-toggle' => 'modal',
            'data-target' => '#course-new-modal',
            'class' => 'btn btn-success',
        ]) ?>
    </p>

    <?php if (Yii::$app->session['datasource']== 'app') {
    	echo GridView::widget([
    			'dataProvider' => $dataProvider,
    			'options' => [
    					'style' => 'overflow: auto; word-wrap: break-word;'
    			],
    			//'filterModel' => $searchModel,
    			'columns' => [
    					[
    							'attribute' => '顺序',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[courseOrder]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '2', 'courseMainId'=>$model['courseMainId']])],
    									'asPopover' => true,
    									'value' => $model['courseOrder'],
    									'header' => '顺序',
    									'size' => 'md',
    									'options' => ['class' => 'form-control']
    							]);
    					},
    					],
    					[
    							'header' => '小课名称',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[courseName]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '1'])],
    									'asPopover' => true,
    									'value' => Html::encode($model['courseName']),
    									'header' => '小课名称',
    									'size' => 'md',
    									'inputType' => Editable::INPUT_TEXTAREA,
    									'editableValueOptions' => ['class' => 'kv-editable-value kv-editable-link ellipsis', 'style' => 'width:100px;'],
    									'options' => ['class' => 'form-control','maxlength'=>64]
    							]);
    					},
    					],
    					[
    							'header' => '转发标题',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[shareTitle]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '4'])],
    									'asPopover' => true,
    									'value' => Html::encode($model['shareTitle']),
    									'header' => '转发标题',
    									'size' => 'md',
    									'editableValueOptions' => ['class' => 'kv-editable-value kv-editable-link ellipsis', 'style' => 'width:100px;'],
    									'options' => ['class' => 'form-control','maxlength'=>32]
    							]);
    					},
    					],
    					[
    							'header' => '转发摘要',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[shareSummary]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '5'])],
    									'asPopover' => true,
    									'value' => Html::encode($model['shareSummary']),
    									'header' => '转发摘要',
    									'size' => 'md',
    									'inputType' => Editable::INPUT_TEXTAREA,
    									'editableValueOptions' => ['class' => 'kv-editable-value kv-editable-link ellipsis', 'style' => 'width:100px;'],
    									'options' => ['class' => 'form-control','maxlength'=>64]
    							]);
    					},
    					],
    					[
    							'header' => '试听',
    							'options' => ['width' => '1'],
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[demoFlag]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '3'])],
    									'asPopover' => true,
    									'header' => '试听标志',
    									'value' => $model['demoFlag'] == '0' ? "否" : "是",
    									'inputType' => Editable::INPUT_DROPDOWN_LIST,
    									'data' => array('0' => '否', '1' => '是'), // any list of values
    									'options' => ['class' => 'form-control', 'prompt' => '请选择'],
    							]);
    					},
    					],
    					[
    							'header' => '音频',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							if ($model['attachId'] == null) {
    								
    								return Html::a('上传', '#', [
    										'id' => 'upload',
    										'class' => 'upload',
    										'data-toggle' => 'modal',
    										'data-target' => '#upload-modal',
    										'data-id' => $model['id'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]);
    							} else {
    								return Html::a('试听', '#', [
    										'id' => 'audio',
    										'class' => 'audio',
    										'data-toggle' => 'modal',
    										'data-target' => '#audio-modal',
    										'data-id' => $model['id'],
    										'data-src' => $model['attach']['attachFullPath'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]) . "  " . Html::a('重新上传', '#', [
    										'id' => 'upload',
    										'class' => 'upload',
    										'data-toggle' => 'modal',
    										'data-target' => '#upload-modal',
    										'data-id' => $model['id'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]);
    							}
    							
    					},
    					],
    					[
    							'header' => '文稿',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							if ($model['courseContent'] == null) {
    								$str = '去添加';
    							} else {
    								$str = '修改';
    							}
    							return Html::a($str, Url::to(['/course/update-content', 'id' => $model['id']]));
    							},
    							],
    							[
    									'attribute' => '发布时间',
    									'options' => ['width' => '10'],
    									'value' => function ($model) {
    									if (!empty($model['courseMain']['publishTime'])) {
    										return date('Y-m-d H:i:s', $model['courseMain']['publishTime'] / 1000);
    									} else {
    										return '';
    									}
    									},
    									],
    									[
    											'class' => 'yii\grid\ActionColumn',
    											'template' => '{view}',
    											'buttons' => [
    													'view' => function ($url, $model, $key) {
    													return Html::a('预览', ['view', 'id' => $model['id']], ['class' => 'btn btn-sm btn-info']);
    											}
    											],
    											'options' => [
    													'width' => 5
    											]
    											],
    											[
    													'class' => 'yii\grid\ActionColumn',
    													'template' => '{delete}',
    													'buttons' => [
    															'delete' => function ($url, $model, $key) {
    															return Html::a('删除', ['delete', 'id' => $model['id']],
    																	['class' => 'btn btn-sm btn-danger', 'data' => [
    																			'confirm' => '你确定删除这条课程信息吗?',
    																			'method' => 'post',
    																	],
    																	]);
    											}
    											],
    											'options' => [
    													'width' => 5
    											]
    											],
    											
    											],
    											]);
    } else {
    	echo GridView::widget([
    			'dataProvider' => $dataProvider,
    			'options' => [
    					'style' => 'overflow: auto; word-wrap: break-word;'
    			],
    			//'filterModel' => $searchModel,
    			'columns' => [
    					[
    							'attribute' => '顺序',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[courseOrder]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '2', 'courseMainId'=>$model['courseMainId']])],
    									'asPopover' => true,
    									'value' => $model['courseOrder'],
    									'header' => '小课名称',
    									'size' => 'md',
    									'options' => ['class' => 'form-control']
    							]);
    					},
    					],
    					[
    							'header' => '小课名称',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							return Editable::widget([
    									'name' => 'CourseSearch[courseName]',
    									'ajaxSettings' => ['url' => Url::toRoute(['course/edit-demo', 'id' => $model['id'], 'type' => '1'])],
    									'asPopover' => true,
    									'value' => Html::encode($model['courseName']),
    									'header' => '小课名称',
    									'size' => 'md',
    									'inputType' => Editable::INPUT_TEXTAREA,
    									'editableValueOptions' => ['class' => 'kv-editable-value kv-editable-link ellipsis', 'style' => 'width:200px;'],
    									'options' => ['class' => 'form-control','maxlength'=>64]
    							]);
    					},
    					],
    					[
    							'header' => '音频',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							if ($model['attachId'] == null) {
    								
    								return Html::a('上传', '#', [
    										'id' => 'upload',
    										'class' => 'upload',
    										'data-toggle' => 'modal',
    										'data-target' => '#upload-modal',
    										'data-id' => $model['id'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]);
    							} else {
    								return Html::a('试听', '#', [
    										'id' => 'audio',
    										'class' => 'audio',
    										'data-toggle' => 'modal',
    										'data-target' => '#audio-modal',
    										'data-id' => $model['id'],
    										'data-src' => $model['attach']['attachFullPath'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]) . "  " . Html::a('重新上传', '#', [
    										'id' => 'upload',
    										'class' => 'upload',
    										'data-toggle' => 'modal',
    										'data-target' => '#upload-modal',
    										'data-id' => $model['id'],
    										'data-courseMainId' => $model['courseMain']['id'],
    								]);
    							}
    							
    					},
    					],
    					[
    							'header' => '文稿',
    							'class' => 'yii\grid\Column',
    							'content' => function ($model) {
    							if ($model['courseContent'] == null) {
    								$str = '去添加';
    							} else {
    								$str = '修改';
    							}
    							return Html::a($str, Url::to(['/course/update-content', 'id' => $model['id']]));
    							},
    							],
    							[
    									'attribute' => '发布时间',
    									'options' => ['width' => '10'],
    									'value' => function ($model) {
    									if (!empty($model['courseMain']['publishTime'])) {
    										return date('Y-m-d H:i:s', $model['courseMain']['publishTime'] / 1000);
    									} else {
    										return '';
    									}
    									},
    									],
    									[
    											'class' => 'yii\grid\ActionColumn',
    											'template' => '{view}',
    											'buttons' => [
    													'view' => function ($url, $model, $key) {
    													return Html::a('预览', ['view', 'id' => $model['id']], ['class' => 'btn btn-sm btn-info']);
    											}
    											],
    											'options' => [
    													'width' => 5
    											]
    											],
    											[
    													'class' => 'yii\grid\ActionColumn',
    													'template' => '{delete}',
    													'buttons' => [
    															'delete' => function ($url, $model, $key) {
    															return Html::a('删除', ['delete', 'id' => $model['id']],
    																	['class' => 'btn btn-sm btn-danger', 'data' => [
    																			'confirm' => '你确定删除这条课程信息吗?',
    																			'method' => 'post',
    																	],
    																	]);
    											}
    											],
    											'options' => [
    													'width' => 5
    											]
    											],
    											
    											],
    											]);
    }?>

    <?php
    Modal::begin([
        'id' => 'course-new-modal',
        'header' => '<h4 class="modal-title">新增小课</h4>',

    ]);

    echo $this->render('_create', ['model' => $model]);

    Modal::end();
    ?>
    <?php
    Modal::begin([
        'id' => 'upload-modal',
        'header' => '<h4 class="modal-title">上传音频</h4>',
    ]);
	    echo $this->render('_upload', ['model'=>$model]);
    Modal::end();
    ?>
    <?php
    Modal::begin([
        'id' => 'audio-modal',
        'header' => '<h4 class="modal-title">音频试听</h4>',
    ]);



    Modal::end();
    ?>
</div>
<?php $this->beginBlock('javascript-block') ?>
        $(function () {
        	//上传音频
        	$('.upload').on('click',function(){
        		$('#coursesearch-id').val($(this).data('id'));
            });
            //试听音频
            $('.audio').on('click',function(){
            	$.get('<?=Url::toRoute('course/play-audio') ?>', { src: $(this).data('src') },
		            function (data) {
		                $('#audio-modal').find('.modal-body').html(data);
		            }  
        		);
            });
        });
<?php $this->endBlock() ?>
<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
	
