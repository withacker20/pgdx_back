<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = '修改文稿';
$this->params['breadcrumbs'][] = ['label' => '子课程列表', 'url' => ['course/view-subs','courseMainId'=>$model->courseMainId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">
    <?= $this->render('_form', [
        'model' => $model,
        'type' => 'updatecontent',
    ]) ?>

</div>
