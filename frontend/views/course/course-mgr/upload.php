<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = '课程发布';
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','action'=>Url::toRoute(['course/course-mgr/update'])]]) ?>

<?= $form->field($model, 'file')->fileInput() ?>

<button>Submit</button>

<?php ActiveForm::end() ?>