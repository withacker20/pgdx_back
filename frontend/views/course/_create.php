<?php
use yii\helpers\Html;

use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = '新增小课';
?>
<?php $form = ActiveForm::begin(['id'=>'course-search','action'=>Url::toRoute(['course/create']),'enableAjaxValidation' => true]) ?>

<?= $form->field($model, 'courseName')->textInput() ?>

<?php if (Yii::$app->session['datasource']== 'app') {
	echo $form->field($model, 'shareTitle')->textInput(['maxlength' => true]);
	echo $form->field($model, 'shareSummary')->textarea(['maxlength' => true]);
}?>


<?= $form->field($model, 'courseOrder')->textInput() ?>

<?php if (Yii::$app->session['datasource']== 'app') {
	echo $form->field($model, 'demoFlag')->dropDownList(array('0'=>'否','1'=>'是'));
}?>

<?= Html::hiddenInput('CourseSearch[courseType]', '1') ?>

<?= Html::hiddenInput('CourseSearch[courseMainId]', $model['courseMainId']) ?>

<?= Html::submitButton('新增小课', ['class' => 'btn btn-success']) ?>

 <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
 
<?php ActiveForm::end() ?>

