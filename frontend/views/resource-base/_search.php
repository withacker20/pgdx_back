<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-base-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'resource-base-search',
        'id' => 'get',
    		'validateOnChange'=> false,
    		'validateOnSubmit'=>false,
    		'validateOnBlur'=>false,
    ]); ?>

    <?= $form->field($model, 'baseName') ?>
    <?= $form->field($model, 'statusType')->dropDownList(array(''=>'所有','1'=>'已发布','0'=>'未发布'))?>

    <div class="form-group">
        <?= Html::submitButton('查询', ['class' => 'btn btn-primary']) ?>
        <?= Html::Button('重置', ['class' => 'btn btn-default','onclick'=>'resetSearchForm("resource-base-search");']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
