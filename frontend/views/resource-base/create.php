<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = '新增资料库';
$this->params['breadcrumbs'][] = ['label' => '资料库管理', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-base-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
