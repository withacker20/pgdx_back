<?php
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resource-base-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'baseName')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'coverId')->hiddenInput() ?>
	
	<?php 
		echo FileInput::widget([
				'name' => 'uploadfile',
				'options'=>[
						'multiple'=>false,
						'accept' => 'image/*'
				],
				'pluginOptions' => [
						'showClose' => false,
						'initialPreview'=>[
								isset($model->attach) ? $model->attach->attachFullPath : null
						],
						'initialPreviewAsData'=>true,
						'initialPreviewShowDelete'=>false,
						'initialCaption'=> isset($model->attach) ? $model->attach->orgFileName : null,
						'initialPreviewConfig' => [
								['caption' => isset($model->attach) ? $model->attach->orgFileName : null, 'size' => isset($model->attach) ? $model->attach->attachSize * 1024 *1024 : null],
						],
						'uploadUrl' => Url::toRoute(['course-main/upload']),
						'maxFileCount' => 1,//设置ajax上传文件是，一次最多可以上传的文件数量
						'dropZoneTitle' => '请上传图片',
						'allowedFileExtensions'=> ['jpg','jpeg','png','bmp','ico'],
						'fileActionSettings' => ['showRemove'=>false, 'showDrag'=>false,'showUpload'=>false],
				],
				'pluginEvents' => [
						'fileuploaded' => "function(event, data, previewId, index) {
											$('#resourcebasesearch-coverid').val(data.response['id']);
		 								}",
						'fileremoved' => "function(event, data, previewId, index) {
											$('#resourcebasesearch-coverid').val('');
		 								}",
						'change' => "function(event, data, previewId, index) {
											$('#resourcebasesearch-coverid').val('');
		 								}",
				],
		]);
	?>
	
	<br>
    <div class="form-group">
        <?= Html::submitButton('保存后上传附件', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
