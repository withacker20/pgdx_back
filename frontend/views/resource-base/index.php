<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '资料库管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resource-base-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('添加资料库', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				
    				[
    						'header'=>'资料库名称',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return Html::a(Html::encode($model['baseName']),Url::to(['/resource/index','baseId'=>$model['id']]));
                            },
                            
    				],
    				[
    						'attribute' => '发布状态',
    						'value' => function($model){
    							if($model['statusType']=='1'){
	    							return '已发布';
	    						}else{
	    							return '未发布';
	    						}
                            },
                    ],
    				[
    						'attribute' => '发布时间',
    						'value' => function($model){
    						if(!is_null($model['publishTime']) && $model['statusType']=='1'){
	    							return date('Y-m-d H:i:s',$model['publishTime']/1000);
	    						}else{
	    							return '';
	    						}
                            },
                            'options' => [
                            		'width' => '25%'
                            ]
                    ],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{update}',
    					'buttons' => [
    						'update' => function ($url, $model, $key) {
    						return Html::a('修改', ['update', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
    						}
    					],
    					'options' => [
    							'width' => 5
    					]
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{view}',
    					'buttons' => [
    						'view' => function ($url, $model, $key) {
    						if($model['statusType']=='1'){
    							return Html::a('取消发布', ['depublish', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
    						}else{
    							return Html::a('发布', ['publish', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
    						}
    					}
    					],
    					'options' => [
    									'width' => 5
    								]
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{delete}',
    					'buttons' => [
    							'delete' => function ($url, $model, $key) {
    								return Html::a('删除', ['delete', 'id' => $model['id']], 
    								['class'=>'btn btn-sm btn-danger','data' => [
	    								'confirm' => '你确定删除这条资料库吗?',
	    								'method' => 'post',
    									],
    								]);
    								}
    					],
    					'options' => [
    						'width' => 5
    						]
    				]
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
