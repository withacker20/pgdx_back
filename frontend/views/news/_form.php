<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AppAsset;

AppAsset::register($this);
AppAsset::addCss($this,'@web/assets/ueditor/themes/default/css/ueditor.css');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.config.js');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.all.js');
AppAsset::addScript($this,'@web/assets/ueditor/lang/zh-cn/zh-cn.js');

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'newsTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'newsContent')->textarea(['rows'=>6,'id'=>'newsContent','class'=>'col-sm-1 col-md-12','style'=>'margin-top: 10px;padding:0;margin:20px 0;width:100%;height:200px;border: none;'])?>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

	<?php $this->beginBlock('javascript-block') ?>
	        $(function () {
	            var ue = UE.getEditor('newsContent');
	        });
	<?php $this->endBlock() ?>
	<?php $this->registerJs($this->blocks['javascript-block'], \yii\web\View::POS_END); ?>
</div>
