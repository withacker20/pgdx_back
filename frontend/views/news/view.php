<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\assets\AppAsset;

AppAsset::register($this);
AppAsset::addCss($this,'@web/assets/ueditor/themes/default/css/ueditor.css');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.config.js');
AppAsset::addScript($this,'@web/assets/ueditor/ueditor.all.js');
AppAsset::addScript($this,'@web/assets/ueditor/lang/zh-cn/zh-cn.js');

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->newsTitle;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('返回', Url::to(['/news/index']), ['class' => 'btn btn-primary']) ?>
    </p>

	
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          	'newsTitle',
            'author',
        ],
    ]) ?>
    <?= Html::label("文稿") ?>
    <div><?=$model->newsContent ?></div>
</div>
