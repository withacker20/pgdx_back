<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '新闻';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <?php Pjax::begin(); ?>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('添加新闻', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
    		'dataProvider' => $dataProvider,
    		'columns' => [
    				['class' => 'yii\grid\SerialColumn','header'=>'序号'],
    				
    				[
    						'header'=>'新闻标题',
    						'class' => 'yii\grid\Column',
    						'content' => function($model){    						
    							return Html::a(Html::encode($model['newsTitle']),Url::to(['/news/update','id'=>$model['id']]),['title'=>$model['newsTitle'],'class'=>'ellipsis','style'=>'width:400px;']);
                            },
                            
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{update}',
    					'buttons' => [
    						'update' => function ($url, $model, $key) {
    						return Html::a('修改', ['update', 'id' => $model['id']], ['class'=>'btn btn-sm btn-primary']);
    						}
    					],
    					'options' => [
    							'width' => 5
    					]
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{view}',
    					'buttons' => [
    						'view' => function ($url, $model, $key) {
    						return Html::a('预览', ['view', 'id' => $model['id']], ['class'=>'btn btn-sm btn-info']);
    					}
    					],
    					'options' => [
    									'width' => 5
    								]
    				],
    				[
    					'class' => 'yii\grid\ActionColumn',
    					'template' => '{delete}',
    					'buttons' => [
    							'delete' => function ($url, $model, $key) {
    								return Html::a('删除', ['delete', 'id' => $model['id']], 
    								['class'=>'btn btn-sm btn-danger','data' => [
	    								'confirm' => '你确定删除这条新闻吗?',
	    								'method' => 'post',
    									],
    								]);
    								}
    					],
    					'options' => [
    						'width' => 5
    						]
    				]
    		]
      
    ]); ?>
    <?php 
    	echo LinkPager::widget([
    		'pagination' => $page,
	    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
