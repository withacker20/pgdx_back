<?php

namespace frontend\controllers;

use app\models\CourseMainSearch;
use app\models\ArticleSearch;
use app\models\MeetingSearch;
use app\models\Message;
use app\models\MessageSearch;
use common\page\Page;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class MessageController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'actions' => [],
										'allow' => true,
										'roles' => ['@'],
								]
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	public function actionIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		$page = Page::generatePage($req_params);
		
		$messageSearch= new MessageSearch();
		$dataProvider=$messageSearch->search($req_params,$page);
		$searchModel= new MessageSearch();
		$searchModel->load($req_params,'MessageSearch');
		
		$model = new MessageSearch();
		$model['msgStatus'] = "send";
		return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'model' => $model,
				'page' => $page,
		]);
	}
	public function actionCourseIndex($type='0')
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$courseSearch = new CourseMainSearch();
		if(!array_key_exists('CourseMainSearch', $req_params)){
			$req_params['CourseMainSearch']['courseMainStatus'] = '1';
		}
		if ($type == '1') {
			$req_params['CourseMainSearch']['courseMainType'] = '1';
		} else {
			$req_params['CourseMainSearch']['courseMainType'] = '0';
		}
		$dataProvider=$courseSearch->search($req_params, $page);
		
		$searchModel = new CourseMainSearch();
		$searchModel->load($req_params,'CourseMainSearch');
		
		return $this->renderAjax('course_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
	
	public function actionNewsIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$articleSearch = new ArticleSearch();
		$dataProvider=$articleSearch->search($req_params, $page);
		
		$categoryList = $articleSearch->searchCategory();
		
		$searchModel = new ArticleSearch();
		$searchModel->load($req_params,'ArticleSearch');
		if (!isset($searchModel->cid)) {
			$searchModel->cid = '23';
		}
		
		return $this->renderAjax('article_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'categoryList' => $categoryList,
				'page' => $page,
		]);
	}
	
	public function actionMeetingIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$MeetingSearch = new MeetingSearch();
		$dataProvider = $MeetingSearch->search($req_params, $page);
		
		$searchModel = new MeetingSearch();
		$searchModel->load($req_params, 'MeetingSearch');
		
		return $this->renderAjax('meeting_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
	
	/**
	 * Displays a single Message model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		$messageSearch = new MessageSearch();
		return $this->render('view', [
				'model' => $messageSearch->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Message model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new MessageSearch();
		$flag = $model->load(Yii::$app->request->post());
		if ($flag && $model->save()) {
			if ($model['submitType'] == 'send') {
				return $this->redirect(['send', 'id' => $model->id]);
			} else {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		} else {
			$messageSearch= new MessageSearch();
			return $this->render('create', [
					'model' => $model,
			]);
		}
	}
	
	public function actionUpdate($id)
	{
		$model = new MessageSearch();
		$model->id = $id;
		$flag = $model->load(Yii::$app->request->post());
		if ($flag && $model->update()) {
			if ($model['submitType'] == 'send') {
				return $this->redirect(['send', 'id' => $model->id]);
			} else {
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}else {
			$messageSearch= new MessageSearch();
			$model = $messageSearch->findModel($id);
			return $this->render('update', [
					'model' => $model,
			]);
		}
	}
	
	public function actionSend($id = null)
	{
		$model = new MessageSearch();
		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return \yii\widgets\ActiveForm::validate($model);
		}
		if (is_null($id)) {
			$messageSearch= new MessageSearch();
			$model = new MessageSearch();
			$model->load(Yii::$app->request->post());
			$dbModel = $messageSearch->findModel($model->id);
			$model['sendTime'] = strtotime($model['sendTime']) * 1000;
			$model['refUrl'] = $dbModel['refUrl'];
			$model['refId'] = $dbModel['refId'];
			$model->update();
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			$messageSearch= new MessageSearch();
			$model = $messageSearch->findModel($id);
			$model['msgStatus'] = "send";
			return $this->render('send_form', [
					'model' => $model,
			]);
		}
	}
	
	public function actionCancelSend($id)
	{
		$model = new MessageSearch();
		$model->load(Yii::$app->request->post());
		$model->id = $id;
		$model->msgStatus= 'init';
		$model->update();
		return $this->redirect(['index']);
	}
	
	public function actionDelete($id)
	{
		$messageSearch= new MessageSearch();
		$messageSearch->id = $id;
		$messageSearch->delete();
		
		return $this->redirect(['index']);
	}
}
