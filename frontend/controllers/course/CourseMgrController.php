<?php
namespace frontend\controllers\course;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use frontend\models\course\courseUploadForm;
use yii\httpclient\Client;
use yii\httpclient\Request;
use yii\httpclient\RequestEvent;  

class CourseMgrController extends Controller{
	
	
	public function actionIndex()
	{
		$model = new courseUploadForm();
		
		return $this->render('index', ['model' => $model]);
	}
	
	public function actionUpload()
	{
		$model = new courseUploadForm();
				
		if (Yii::$app->request->isPost) {
			$model->file = UploadedFile::getInstance($model, 'file');
			
			Yii::info('file info:'.$model->file->tempName.'#'.$model->file->type);
			if ($model->file && $model->validate()) {
				//$model->file->saveAs('E:/upload_temp/' . $model->file->baseName . '.' . $model->file->extension);
				$this->uploadFileToJava($model->file);
			}
			
			
			//$this->uploadFileToJava($model->file);
		}
		
				
		return $this->render('upload', ['model' => $model]);
	}
	
	private function uploadFileToJava($file){
		$client = new Client();
		
		$request = $client->createRequest()
		->setMethod('post')
		->setUrl('http://www.pgdx.com:8080/pgdx/test/uploadFile')
		->addFile('file', $file->tempName,['mimeType'=>$file->type,'fileName'=>$file->name])
		->setData(['param' => 'value']);
		
		// 发送前触发事件
		$request->on(Request::EVENT_BEFORE_SEND, function (RequestEvent $event) {
			$data = $event->request->getData();
			
			$signature = md5(http_build_query($data));
			$data['signature'] = $signature;
			
			$event->request->setData($data);
		});
			
		// 发送后响应数据
		$request->on(Request::EVENT_AFTER_SEND, function (RequestEvent $event) {
			$data = $event->response->getData();
			
			Yii::info('data response from java arg:'.$data['arg']);
			
			$data['content'] = base64_decode($data['arg']);
			
			 Yii::info('data response from java:'.$data['content']);
			
			//$event->response->setData($data);
			
			//Console::stdout($data['arg']);
		});
			
			//发送
			$response = $request->send();
	}
	
	private function call(){
		$client = new Client();
		
		$request = $client->createRequest()
		->setMethod('get')
		->setUrl('http://www.pgdx.com:8080/pgdx/test/hello/fromPHP')
		->setData(['param' => 'value']);
		
		// 发送前触发事件
		$request->on(Request::EVENT_BEFORE_SEND, function (RequestEvent $event) {
			$data = $event->request->getData();
			
			$signature = md5(http_build_query($data));
			$data['signature'] = $signature;
			
			$event->request->setData($data);
		});
			
		// 发送后响应数据
		$request->on(Request::EVENT_AFTER_SEND, function (RequestEvent $event) {
			$data = $event->response->getData();
			
			$data['content'] = base64_decode($data['arg']);
			
			$event->response->setData($data);
			
			//Console::stdout($data['arg']);
		});
			
		//发送
		$response = $request->send();
	}
}