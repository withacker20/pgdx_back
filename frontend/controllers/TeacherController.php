<?php

namespace frontend\controllers;

use Yii;
use app\models\Teacher;
use app\models\TeacherSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\page\Page;
use common\http\RestfulHttp;

class TeacherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	
    	$teacherSearch= new TeacherSearch();
    	$dataProvider=$teacherSearch->search($req_params,$page);
    	$searchModel= new TeacherSearch();
    	$searchModel->load($req_params,'TeacherSearch');
    	
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	]);
    	
    	
    }

    /**
     * Displays a single Teacher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$teacherSearch = new TeacherSearch();
        return $this->render('view', [
        		'model' => $teacherSearch->findModel($id),
        ]);
    }

    /**
     * Creates a new Teacher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeacherSearch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
    	$model = new TeacherSearch();
    	$model->id = $id;

    	if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
        	$teacherSearch= new TeacherSearch();
        	$model = $teacherSearch->findModel($id);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDelete($id)
    {
    	$teacherSearch= new TeacherSearch();
    	$teacherSearch->id = $id;
    	$teacherSearch->delete();
    	
    	return $this->redirect(['index']);
    }
}
