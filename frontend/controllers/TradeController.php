<?php

namespace frontend\controllers;

use Yii;
use app\models\Trade;
use app\models\TradeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\page\Page;
use common\http\RestfulHttp;

class TradeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	
    	$orderSearch= new TradeSearch();
    	$dataProvider=$orderSearch->search($req_params,$page);
    	$searchModel= new TradeSearch();
    	$searchModel->load($req_params,'TradeSearch');
    	
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	]);
    	
    	
    }

    /**
     * Displays a single Trade model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$orderSearch = new TradeSearch();
        return $this->render('view', [
        		'model' => $orderSearch->findModel($id),
        ]);
    }

    /**
     * Creates a new Trade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TradeSearch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
    	$model = new TradeSearch();
    	$model->id = $id;

    	if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
        	$orderSearch= new TradeSearch();
        	$model = $orderSearch->findModel($id);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDelete($id)
    {
    	$orderSearch= new TradeSearch();
    	$orderSearch->id = $id;
    	$orderSearch->delete();
    	
    	return $this->redirect(['index']);
    }
}
