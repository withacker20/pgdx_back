<?php

namespace frontend\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\UserLDAPSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\page\Page;
use common\http\RestfulHttp;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionRealIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	//查询问答信息
    	$userLDAPSearch= new UserLDAPSearch();
    	$dataProvider=$userLDAPSearch->searchUserLDAP($req_params, $page);
    	
    	$searchModel = new UserLDAPSearch();
    	$searchModel->load($req_params,'UserLDAPSearch');
    	
    		return $this->render('real_index', [
    				'userSearch' => $searchModel,
    				'dataProvider' => $dataProvider,
    				'page' => $page,
    		]);
    	
    	
    }
    
    public function actionIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	//查询问答信息
    	$userSearch= new UserLDAPSearch();
    	$dataProvider=$userSearch->searchUserLDAP($req_params, $page);
    	
    	$searchModel = new UserLDAPSearch();
    	$searchModel->load($req_params,'UserLDAPSearch');
    	
    	return $this->render('index', [
    			'userSearch' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
   		]);
    	
    }
    
    public function actionWxIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	
    	$userSearch= new UserSearch();
    	$dataProvider=$userSearch->search($req_params,$page);
    	
    	$searchModel = new UserSearch();
    	$searchModel->load($req_params,'UserSearch');
    	
    	return $this->render('wx_index', [
    			'userSearch' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
   		]);
    	
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$modelLDAP = new UserLDAPSearch();
        return $this->render('view', [
        		'model' => $modelLDAP->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDisableUser($id)
    {
    	$this->findModel($id)->disable();
    	return $this->redirect(['wx-index']);
    }
    
    public function actionEnableUser($id)
    {
    	$this->findModel($id)->enable();
    	return $this->redirect(['wx-index']);
    }
    
    public function actionEnableTeacher($id)
    {
    	$this->findModel($id)->enableTeacher();
    	return $this->redirect(['wx-index']);
    }
    
    public function actionDisableTeacher($id)
    {
    	$this->findModel($id)->disableTeacher();
    	return $this->redirect(['wx-index']);
    }
    
    public function actionRealPass($uid)
    {
    	$userSearch = new UserSearch();
    	$userSearch->updateRealStatusLDAP($uid, '2');
    	return $this->redirect(['wx-index']);
    }
    
    public function actionRealNopass($uid)
    {
    	$userSearch = new UserSearch();
    	$userSearch->updateRealStatusLDAP($uid, '3');
    	return $this->redirect(['wx-index']);
    }
    
    public function actionThinkAccess($id,$thinkAccess)
    {
    	$this->findModel($id)->updateThinkAccess($thinkAccess);
    	return $this->redirect(['wx-index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    	$model= new UserSearch();
    	
    	if($id != null){
    		$rhttp = new RestfulHttp('/user/'.$id);
    		$data = $rhttp->get();
    	}
    	
    	$result = $data['data'];
    	
    	if($data['success']=='true'){
    		$model->load($result,'result');
    		
    		return $model;
    	}else{
    		return new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
