<?php

namespace frontend\controllers;

use app\models\Attach;
use app\models\ResourceBaseBase;
use app\models\ResourceBaseSearch;
use app\models\UserSearch;
use common\controller\CommonController;
use common\page\Page;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Request;
use yii\web\NotFoundHttpException;

/**
 * ResourceBaseController implements the CRUD actions for ResourceBaseBase model.
 */
class ResourceBaseController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ResourceBaseBase models.
     * @return mixed
     */
    public function actionIndex()
    {
       
    	$req_params = Yii::$app->request->queryParams;
    	
    	//分页
    	$page = Page::generatePage($req_params);
    	
        //查询主课程信息
    	$ResourceBaseSearch = new ResourceBaseSearch();
        $dataProvider=$ResourceBaseSearch->search($req_params, $page);

    	$searchModel = new ResourceBaseSearch();
    	$searchModel->load($req_params,'ResourceBaseSearch');

    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	]);
    }

    /**
     * Displays a single ResourceBaseBase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	
        return $this->render('view', [
        	'model' => ResourceBaseSearch::findModel($id),
        ]);
    }

    /**
     * Creates a new ResourceBaseBase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	
        $model = new ResourceBaseSearch();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/resource/index', 'baseId' => $model->id]);
        } else {
            return $this->render('create', [
                	'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ResourceBaseBase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
    	Yii::info('actionUpdate id:'.$id);
    	$userSearch = new UserSearch();
    	
    	$model = new ResourceBaseSearch();
    	$model->id = $id;

    	if ($model->load(Yii::$app->request->post()) && $model->update()) {
    		return $this->redirect(['/resource/index', 'baseId' => $model->id]);
    	} else {
    		$model = ResourceBaseSearch::findModel($id);
    		
    		return $this->render('update', [
    				'model' => $model,
    		]);
    	}
    }
        
    public function actionPublish($id){
    	ResourceBaseSearch::findModel($id)->publish();
    	
    	return $this->redirect(['index']);
    }
    
    public function actionDepublish($id){
    	ResourceBaseSearch::findModel($id)->depublish();
    	
    	return $this->redirect(['index']);
    }

    /**
     * Deletes an existing ResourceBaseBase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	ResourceBaseSearch::findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ResourceBaseBase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ResourceBaseBase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
    	$model= new ResourceBaseSearch();
    	    	
    	$data = ResourceBaseSearch::findModel($id);

    	if($data['success']=='true'){
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		
    		return $model;
    	}else{
    		return new NotFoundHttpException('The requested page does not exist.');;
    	}
    	
    }
}
