<?php

namespace frontend\controllers;

use Yii;
use app\models\Boarding;
use app\models\BoardingSearch;
use common\controller\CommonController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\page\Page;
use app\models\CourseMainSearch;
use app\models\NewsSearch;
use yii\httpclient\Client;
use yii\helpers\ArrayHelper;

/**
 * BoardingController implements the CRUD actions for Boarding model.
 */
class BoardingController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Boarding models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	//分页
    	$page = Page::generatePage($req_params);
    	
    	//查询新闻信息
    	$boardingSearch= new BoardingSearch();
    	$dataProvider=$boardingSearch->search($req_params, $page);
    	
    	$searchModel = new BoardingSearch();
    	$searchModel->load($req_params,'BoardingSearch');
    	
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	]);
    	
    }

    /**
     * Displays a single Boarding model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Boarding model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BoardingSearch();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Boarding model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Boarding model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionPublish($id){
    	BoardingSearch::findModel($id)->publish();
    	
    	return $this->redirect(['index']);
    }
    
    public function actionDepublish($id){
    	BoardingSearch::findModel($id)->depublish();
    	
    	return $this->redirect(['index']);
    }
    
    public function actionDropDownList($boardingType){
    	Yii::$app->response->format = Client::FORMAT_JSON;
    	if($boardingType == 'cm'){
    		return ArrayHelper::map(CourseMainSearch::findPublishedModel(), 'id', 'courseMainName');
    	}else if($boardingType == 'cm1'){
    		return ArrayHelper::map(CourseMainSearch::findPublishedModel('1'), 'id', 'courseMainName');
    	}else if($boardingType == 'n'){
    		return ArrayHelper::map(NewsSearch::findPublishedModel(), 'id', 'newsTitle');
    	}
    	return '';
    }

    /**
     * Finds the Boarding model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Boarding the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    	if (($model = BoardingSearch::findModel($id)) !== null) {
    		return $model;
    	} else {
    		throw new NotFoundHttpException('The requested page does not exist.');
    	}
    }
}
