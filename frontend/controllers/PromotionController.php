<?php

namespace frontend\controllers;

use app\models\Promotion;
use app\models\PromotionSearch;
use app\models\CourseMainSearch;
use app\models\MeetingSearch;
use app\models\ArticleSearch;
use common\page\Page;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PromotionController implements the CRUD actions for Promotion model.
 */
class PromotionController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'actions' => [],
										'allow' => true,
										'roles' => ['@'],
								]
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	/**
	 * Displays a single Promotion model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView()
	{
		return $this->render('view', [
				'model' => $this->findModel(1),
		]);
	}
	
	
	/**
	 * Updates an existing Promotion model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate()
	{
		$model = new PromotionSearch();
		$model->id = 1;
		if ($model->load(Yii::$app->request->post()) && $model->update()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			$model = $this->findModel(1);
			return $this->render('update', [
					'model' => $model,
			]);
		}
	}
	
	/**
	 * Finds the Promotion model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Promotion the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = PromotionSearch::findModel($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	public function actionCourseIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$courseSearch = new CourseMainSearch();
		$dataProvider = $courseSearch->search($req_params, $page);
		
		$searchModel = new CourseMainSearch();
		$searchModel->load($req_params, 'CourseMainSearch');
		
		return $this->renderAjax('course_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
	
	public function actionNewsIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$ArticleSearch = new ArticleSearch();
		$dataProvider = $ArticleSearch->search($req_params, $page);
		
		$categoryList = $ArticleSearch->searchCategory();
		
		$searchModel = new ArticleSearch();
		$searchModel->load($req_params, 'ArticleSearch');
		
		return $this->renderAjax('article_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'categoryList' => $categoryList,
				'page' => $page,
		]);
	}
	
	public function actionMeetingIndex()
	{
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		
		//查询主课程信息
		$MeetingSearch = new MeetingSearch();
		$dataProvider = $MeetingSearch->search($req_params, $page);
		
		$searchModel = new MeetingSearch();
		$searchModel->load($req_params, 'MeetingSearch');
		
		return $this->renderAjax('meeting_index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
}
