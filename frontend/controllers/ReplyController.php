<?php

namespace frontend\controllers;

use Yii;
use app\models\Reply;
use app\models\ReplySearch;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use common\page\Page;
use common\http\RestfulHttp;
use app\models\CourseMainSearch;


/**
 * ReplyController 问答管理.
 */
class ReplyController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
				'access' => [
						'class' => AccessControl::className(),
						'rules' => [
								[
										'actions' => [],
										'allow' => true,
										'roles' => ['@'],
								]
						],
				],
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	/**
	 * Lists all Reply models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		
		$req_params = Yii::$app->request->queryParams;
		
		$req_params = Yii::$app->request->queryParams;
		
		//分页
		$page = Page::generatePage($req_params);
		$req_params['pgopr'] = 'reply';
		//查询主课程信息
		$courseSearch = new CourseMainSearch();
		$dataProvider = $courseSearch->search($req_params, $page);
		
		$searchModel = new CourseMainSearch();
		$searchModel->load($req_params, 'CourseMainSearch');
		
		return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
	
	public function actionReplyIndex($courseMainId)
	{
		$req_params = Yii::$app->request->queryParams;
		
		$courseMain = new CourseMainSearch();
		$courseMain = CourseMainSearch::findModel($courseMainId);
		
		$page = Page::generatePage($req_params);
		$req_params['pgopr'] = 'reply';
		$replySearch = new ReplySearch();
		$dataProvider = $replySearch->search($req_params, $page, $courseMainId);
		
		$searchModel = new ReplySearch();
		$searchModel->load($req_params, 'ReplySearch');
		
		$model = new ReplySearch();
		
		return $this->render('reply_index', [
				'searchModel' => $searchModel,
				'courseMain' => $courseMain,
				'model' => $model,
				'dataProvider' => $dataProvider,
				'page' => $page,
		]);
	}
	
	public function actionCreate($questoinId,$courseMainId)
	{
		$model = new ReplySearch(); // your model can be loaded here
		
		// Check if there is an Editable ajax request
		if (isset($_POST['hasEditable'])) {
			// use Yii's response format to encode output as JSON
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			
			// read your posted model attributes
			if ($model->load($_POST)) {
				// read or convert your posted information
				$value = $model->replyContent;
				$model->userId = 137;
				$model->courseMainId = $courseMainId;
				if (trim($value) == '') {
					return ['output' => $value, 'message' => '不能为空'];
				} else if (mb_strlen(trim($value)) > 1024) {
					return ['output' => $value, 'message' => '不能超过1024个字符'];
				}
				if ($model->save($questoinId)) {
					// return JSON encoded output in the below format
					return ['output' => $value, 'message' => '', 'id' => $questoinId, 'updateTime' => date('Y-m-d H:i:s', time())];
				} else {
					return ['output' => $value, 'message' => '回复失败'];
				}
				// alternatively you can return a validation error
				// return ['output'=>'', 'message'=>'Validation error'];
			} // else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => ''];
			}
		}
		
		// Else return to rendering a normal view
		return $this->render('view', ['model' => $model]);
	}
	
	public function actionUpdate($id, $questoinId,$courseMainId)
	{
		$model = new ReplySearch(); // your model can be loaded here
		
		// Check if there is an Editable ajax request
		if (isset($_POST['hasEditable'])) {
			// use Yii's response format to encode output as JSON
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			
			// read your posted model attributes
			if ($model->load($_POST)) {
				// read or convert your posted information
				$value = $model->replyContent;
				$model->userId = 137;
				$model->courseMainId = $courseMainId;
				if (trim($value) == '') {
					return ['output' => $value, 'message' => '不能为空'];
				} else if (mb_strlen(trim($value)) > 1024) {
					return ['output' => $value, 'message' => '不能超过1024个字符'];
				}
				if ($model->update($id)) {
					// return JSON encoded output in the below format
					return ['output' => $value, 'message' => '', 'id' => $questoinId, 'updateTime' => date('Y-m-d H:i:s', time())];
				} else {
					return ['output' => $value, 'message' => '回复失败'];
				}
				// alternatively you can return a validation error
				// return ['output'=>'', 'message'=>'Validation error'];
			} // else if nothing to do always return an empty JSON encoded output
			else {
				return ['output' => '', 'message' => ''];
			}
		}
		
		// Else return to rendering a normal view
		return $this->render('view', ['model' => $model]);
	}
	
	/**
	 * Deletes an existing Reply model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id, $courseMainId)
	{
		Yii::info('actionDelete reply id:' . $id);
		$model = new ReplySearch();
		$model->id = $id;
		$model->delete();
		
		return $this->redirect(['reply-index', 'courseMainId' => $courseMainId]);
		
	}
	
	/**
	 * Finds the Reply model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Reply the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		$model = new ReplySearch();
		
		if ($id != null) {
			$rhttp = new RestfulHttp('/reply/' . $id);
			$data = $rhttp->get();
		}
		
		$result = $data['data'];
		
		if ($data['success'] == 'true') {
			$model->load($result, 'result');
			$t = $result['result'];
			$user = new UserSearch();
			$user->load($t, 'user');
			$model->user = $user;
			
			return $model;
		} else {
			return new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
