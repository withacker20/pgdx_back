<?php

namespace frontend\controllers;

use Yii;
use app\models\Course;
use app\models\CourseSearch;
use app\models\AttachSearch;
use app\models\CourseMainSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\NotAcceptableHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\http\RestfulHttp;
use app\models\CourseMain;
use yii\httpclient\Client;
use common\controller\CommonController;
use SebastianBergmann\CodeCoverage\Report\Html\Renderer;


/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends CommonController
{
	public $enableCsrfValidation = false;
	
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actions()
    {
    	return [
    			'error' => [
    					'class' => 'yii\web\ErrorAction',
    			],
    			'captcha' => [
    					'class' => 'yii\captcha\CaptchaAction',
    					'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
    			],
    	];
    }
    
    public function actionEditDemo($id,$type,$courseMainId=0) {
    	$model = new CourseSearch(); // your model can be loaded here
    	
    	// Check if there is an Editable ajax request
    	if (isset($_POST['hasEditable'])) {
    		// use Yii's response format to encode output as JSON
    		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		
    		// read your posted model attributes
    		if ($model->load($_POST)) {
    			// read or convert your posted information
    			$model->id = $id;
    			if ($type == '1') {
    				$value = $model->courseName;
    			} else if ($type == '2') {
    				$value = $model->courseOrder;
    				$model->courseMainId = $courseMainId;
    				if (!$model->checkOrder()) {
    					return ['output'=>$value, 'message'=>'小课顺序已存在'];
    				}
    			} else if ($type == '3') {
    				$value = $model->demoFlag;
    				if ($value == '0') {
    					$value = "否";
    				} 
    				if ($value == '1') {
    					$value = "是";
    				}
    			} else if ($type == '4') {
    				$value = $model->shareTitle;
    			} else if ($type == '5') {
    				$value = $model->shareSummary;
    			}
    			
    			if(trim($value) == '') {
    				return ['output'=>$value, 'message'=>'不能为空'];
    			}
    			
    			if ($model->update()) {
	    			// return JSON encoded output in the below format
	    			return ['output'=>$value, 'message'=>''];
    			} else {
	    			return ['output'=>$value, 'message'=>'更新失败'];
    			}
    			// alternatively you can return a validation error
    			// return ['output'=>'', 'message'=>'Validation error'];
    		}
    		// else if nothing to do always return an empty JSON encoded output
    		else {
    			return ['output'=>'', 'message'=>''];
    		}
    	}
    	
    	// Else return to rendering a normal view
    	return $this->render('view', ['model'=>$model]);
    }
    
    public function actionViewSubs($courseMainId)
    {
    	$searchModel = new CourseSearch();
    	
    	//对应主课程信息
    	$courseMain= CourseMainSearch::findModel($courseMainId);
    	//var_dump($courseMain);
    	
    	//记录主课程ID
    	$model = new CourseSearch();
    	$model->courseMainId = $courseMainId;
    	
    	$courseSearch = new CourseSearch();
    	$dataProvider=$courseSearch->searchByCourseMainId($courseMainId);
    	
    	return $this->render('index', [
    			'model'=> $model,
    			'courseMain' => $courseMain,
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    	
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CourseSearch();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	return $this->redirect(['view-subs', 'courseMainId' => $model->courseMainId]);
        } else {
        	return $this->redirect(['view-subs', 'courseMainId' => $model->courseMainId]);
        }
    }
    
    public function actionUpdateContent($id){
    	$model = new CourseSearch();
    	$model->id = $id;
    	if ($model->load(Yii::$app->request->post()) && $model->updateContent()) {
    		$model = $this->findModel($id);
    		return $this->redirect(['view-subs', 'courseMainId' => $model->courseMainId]);
    	} else {
    		$model = $this->findModel($id);
    		return $this->render('updatecontent', [
    				'model' => $model,
    		]);
    	}
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUploadAttach(){
    	$model = new CourseSearch();
    	
    	if ($model->load(Yii::$app->request->post()) && $model->uploadAttach()) {
	    	return $this->redirect(['view-subs', 'courseMainId' => $model->courseMainId]);
    	} else {
    		throw new NotAcceptableHttpException();
    	}
    }
    
    public function actionPlayAudio($src){
    	return $this->renderAjax('_play',['src'=>$src]);
    	
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    	Yii::info('actionDelete course id:'.$id);
    	$model = new CourseSearch();
    	$model = $this->findModel($id);
    	
    	$courseMainId = $model->courseMainId;
    	Yii::info('actionDelete courseMainId:'.$model->courseMainId);
    	Yii::info('actionDelete course id:'.$model->id);
        
    	$model->delete();

    	return $this->redirect(['view-subs', 'courseMainId' => $courseMainId]);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
    	$model= new CourseSearch();
    	//$teacher= new UserSearch();
    	
    	
    	if($id != null){
    		$rhttp = new RestfulHttp('/course/sub/'.$id);
    		$data = $rhttp->get();
    	}
    	
    	$result = $data['data'];
    	
    	if($data['success']=='true'){
			$attach= new AttachSearch();
			$attach->load($result['result'],'attach');
			$model->load($result,'result');
			$model->attach=$attach;
    		return $model;
    	}else{
    		return new NotFoundHttpException('The requested page does not exist.');;
    	}
    }
}
