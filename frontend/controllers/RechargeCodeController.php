<?php

namespace frontend\controllers;

use Yii;
use app\models\RechargeCode;
use app\models\RechargeCodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\page\Page;
use common\http\RestfulHttp;

class RechargeCodeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	$page = Page::generatePage($req_params);
    	
    	$rechargeCodeSearch= new RechargeCodeSearch();
    	$dataProvider=$rechargeCodeSearch->search($req_params,$page);
    	$searchModel= new RechargeCodeSearch();
    	$searchModel->load($req_params,'RechargeCodeSearch');
    	
    	return $this->render('index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	]);
    	
    	
    }

    /**
     * Displays a single RechargeCode model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewCode($code)
    {
    	$rechargeCodeSearch = new RechargeCodeSearch();
        return $this->renderAjax('view_code', [
        		'model' => $rechargeCodeSearch->findModelByCode($code),
        ]);
    }
    
    /**
     * Displays a single RechargeCode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$rechargeCodeSearch = new RechargeCodeSearch();
        return $this->render('view', [
        		'model' => $rechargeCodeSearch->findModel($id),
        ]);
    }

    /**
     * Creates a new RechargeCode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RechargeCodeSearch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCancel($id)
    {
        $rechargeCodeSearch= new RechargeCodeSearch();
    	$rechargeCodeSearch->id = $id;
    	$rechargeCodeSearch->cancel();
    	
    	return $this->redirect(['index']);
    }
    
    public function actionDownload($id)
    {
        sleep(1);
        $rechargeCodeSearch= new RechargeCodeSearch();
        $models = $rechargeCodeSearch->searchRechargeCodeListByBatchId($id);
        $codes = array();
        foreach( $models  as $model) {
            $row = array($model['rechargeCode']);
            array_push($codes,$row );
        }
        $filename = 'test'.time();

        set_time_limit(0);  
        ini_set('memory_limit', '512M');  
        
        //为fputcsv()函数打开文件句柄  
        $output = fopen('php://output', 'w') or die("can't open php://output");  
        //告诉浏览器这个是一个csv文件  
        $filename = "充值码_" . time();  
        header("Content-Disposition: attachment; filename=$filename.csv");
        header("Content-Type: text/csv; charset=UTF-8");
        //输出表头  
        //输出每一行数据到文件中  
        foreach ($codes as $e) {  
        //    unset($e['xx']);//若有多余字段可以使用unset去掉  
        //    $e['xx'] = isset($e['xxx']) ? "xx" : 'x'; //可以根据需要做相应处理  
            //输出内容  
            fputcsv($output, array_values($e));  
        }  
        fclose($output) or die("can't close php://output");  
    }
}
