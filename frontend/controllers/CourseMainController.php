<?php

namespace frontend\controllers;

use app\models\Attach;
use app\models\CourseMain;
use app\models\CourseMainSearch;
use app\models\UserSearch;
use app\models\TeacherSearch;
use common\controller\CommonController;
use common\page\Page;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\httpclient\Request;
use yii\web\NotFoundHttpException;

/**
 * CourseMainController implements the CRUD actions for CourseMain model.
 */
class CourseMainController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseMain models.
     * @return mixed
     */
    public function actionIndex($type='0')
    {
    	$req_params = Yii::$app->request->queryParams;
    	
    	//分页
        $page = Page::generatePage($req_params);
        if (isset($req_params['CourseMainSearch'])) {
            $req_params['CourseMainSearch']['courseMainType'] = $type;
        } else {
            $req_params['CourseMainSearch'] = array("courseMainType" => $type);
        }

    	
        //查询主课程信息
        $courseSearch = new CourseMainSearch();
        $dataProvider=$courseSearch->search($req_params, $page);

        //查询讲师列表
        $teacherSearch= new TeacherSearch();
        $teacherList = $teacherSearch->searchTeacher();
        //讲师列表下拉框默认的第一个选项为所有
        array_splice($teacherList,0,0, array(array('id'=>'','teacherName'=>'所有')));
            	    	
    	$searchModel = new CourseMainSearch();
    	$searchModel->load($req_params,'CourseMainSearch');
    	if ($type == '0') {
            return $this->render('index', [
    			'searchModel' => $searchModel,
    			'teacherSelect' => $teacherList,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	    ]);
        } else {
            return $this->render('daka_index', [
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    			'page' => $page,
    	    ]);
        }
    }

    /**
     * Displays a single CourseMain model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($type='0', $id)
    {
    	if ($type == '1') {
            return $this->render('daka_view', [
                'model' => CourseMainSearch::findModel($id),
            ]);
        }
        return $this->render('view', [
        	'model' => CourseMainSearch::findModel($id),
        ]);
    }

    /**
     * Creates a new CourseMain model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type='0')
    {
    	
        $model = new CourseMainSearch();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return \yii\widgets\ActiveForm::validate($model);
        }
        
        $teacherSearch = new TeacherSearch();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	if ($model->courseMainType == '1') {
                return $this->redirect(['view', 'id' => $model->id, 'type' => '1']);
            } else {
                return $this->redirect(['/course/view-subs', 'courseMainId' => $model->id]);
            }
        } else {
            return $this->render('create', [
                	'model' => $model,
                	'type' => $type,
            		'teacherSelect' => $teacherSearch->searchTeacher(),
            ]);
        }
    }

    /**
     * Updates an existing CourseMain model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($type='0', $id)
    {
    	Yii::info('actionUpdate id:'.$id);
    	$teacherSearch = new TeacherSearch();
    	
    	$model = new CourseMainSearch();
    	if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
    		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		return \yii\widgets\ActiveForm::validate($model);
    	}
    	$model->id = $id;

    	if ($model->load(Yii::$app->request->post()) && $model->update()) {
            if ($model->courseMainType == '1') {
                return $this->redirect(['view', 'id' => $model->id, 'type' => '1']);
            }
    		return $this->redirect(['view', 'id' => $model->id]);
    	} else {
            $model = CourseMainSearch::findModel($id);
            if ($model->courseMainType == '1') {
                $model->courseMainDate = date('Y-m-d', $model['courseMainDate'] / 1000);
            }
    		
    		return $this->render('update', [
                    'model' => $model,
                    'type' => $type,
    				'teacherSelect' => $teacherSearch->searchTeacher(),
    		]);
    	}
    }
        
    public function actionPublish($type='0', $id){
    	CourseMainSearch::findModel($id)->publish();
    	
    	return $this->redirect(['index', 'type'=>$type]);
    }
    
    public function actionDepublish($type='0', $id){
    	CourseMainSearch::findModel($id)->depublish();
    	
    	return $this->redirect(['index', 'type'=>$type]);
    }

    /**
     * Deletes an existing CourseMain model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($type='0', $id)
    {
    	
    	CourseMainSearch::findModel($id)->delete();

        return $this->redirect(['index', 'type'=>$type]);
    }

    /**
     * Finds the CourseMain model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseMain the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
    	$model= new CourseMainSearch();
    	$teacher= new UserSearch();
    	    	
    	$data = CourseMainSearch::findModel($id);

    	if($data['success']=='true'){
    		$result = $data['data'];
    		$t = $result['result'];
    		$teacher->load($t,'teacher');
    		$model->teacher=$teacher;
    		$model->load($result,'result');
    		
    		return $model;
    	}else{
    		return new NotFoundHttpException('The requested page does not exist.');;
    	}
    	
    }
}
