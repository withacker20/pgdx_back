<?php

namespace frontend\controllers;

use app\models\Attach;
use app\models\ResourceBase;
use app\models\ResourceBaseSearch;
use app\models\ResourceFileSearch;
use common\controller\CommonController;
use common\page\Page;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Request;

/**
 * ResourceController implements the CRUD actions for ResourceBase model.
 */
class ResourceController extends CommonController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($baseId)
    {
    	$searchModel = new ResourceBaseSearch();
    	
    	$resourceBase= ResourceBaseSearch::findModel($baseId);
    	
    	$model = new ResourceFileSearch();
    	$model->baseId = $baseId;
    	
    	$resourceFileSearch = new ResourceFileSearch();
    	$dataProvider=$resourceFileSearch->searchByBaseId($baseId);
    	
    	return $this->render('index', [
    			'model'=> $model,
    			'resourceBase' => $resourceBase,
    			'searchModel' => $searchModel,
    			'dataProvider' => $dataProvider,
    	]);
    }
    
    public function actionView($id)
    {
    	
        return $this->render('view', [
        	'model' => ResourceBaseSearch::findModel($id),
        ]);
    }

    /**
     * Creates a new ResourceBase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($baseId)
    {
    	
        $model = new ResourceFileSearch();
        $model->baseId = $baseId;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        	Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        	return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	
            return $this->redirect(['/resource/index', 'baseId' => $model->baseId]);
        } else {
            return $this->render('create', [
            		'attach' => [],
                	'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ResourceBase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $baseId)
    {
    	$model = new ResourceFileSearch(); // your model can be loaded here
    	
    	// Check if there is an Editable ajax request
    	if (isset($_POST['hasEditable'])) {
    		// use Yii's response format to encode output as JSON
    		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		
    		// read your posted model attributes
    		if ($model->load($_POST)) {
    			// read or convert your posted information
    			$model->id = $id;
    			$model->baseId = $baseId;
    			$value = $model->fileOrder;
    			if (trim($value) == '') {
    				return ['output' => $value, 'message' => '不能为空'];
    			}
    			if (!$model->checkOrder()) {
    				return ['output'=>$value, 'message'=>'小课顺序已存在'];
    			}
    			if ($model->update()) {
    				// return JSON encoded output in the below format
    				return ['output' => $value, 'message' => ''];
    			} else {
    				return ['output' => $value, 'message' => '更新失败'];
    			}
    			// alternatively you can return a validation error
    			// return ['output'=>'', 'message'=>'Validation error'];
    		} // else if nothing to do always return an empty JSON encoded output
    		$model = ResourceFileSearch::findModel($id);
    		return $this->render('update', [
    				'model' => $model,
    		]);
    	}
    	
    	// Else return to rendering a normal view
    	return $this->render('view', ['model' => $model]);
    }

    public function actionDelete($id)
    {
    	$model = ResourceFileSearch::findModel($id);
    	$model->delete();

    	return $this->redirect(['/resource/index', 'baseId' => $model->baseId]);
    }
}
