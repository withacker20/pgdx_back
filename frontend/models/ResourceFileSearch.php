<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ResourceBase;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ResourceFileSearch represents the model behind the search form of `app\models\ResourceBase`.
 */
class ResourceFileSearch extends ResourceFile
{
    /**
     * @inheritdoc
     */
	public function rules()
	{
		return array_merge(parent::rules(), [
				[['fileOrder'], 'orderValid', 'skipOnEmpty' => false,'skipOnError' => false],
		]);
	}
	
	public function orderValid($attribute, $params)
	{
		$rhttp = new RestfulHttp('/resource/file_order');
		ArrayHelper::removeValue($this,'');
		
		$data = $rhttp->post($this);
		
		if($data['success']=='false'){
			$this->addError($attribute, "附件顺序已存在,请重新输入");
		}
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByBaseId($baseId)
    {
    	$rhttp = new RestfulHttp('/resource/base/'.$baseId.'/file');
    	$rrepo = $rhttp->get();

        $dataProvider= new ArrayDataProvider([
                'allModels' => $rrepo['data']['resultList'],
                'sort' => ['attributes' => ['id'],],
        ]);
    	
        return $dataProvider;
    }
    
    /**
     * 查询已发布的主课程信息列表
     * 
     */
    public static function findPublishedModel(){
    	$rhttp = new RestfulHttp('/course/main');
    	
    	$rrepo = $rhttp->get(['ResourceBaseStatus'=>'1','pagination'=>false]);
    	
    	if($rrepo['success']=='true'){
    		return $rrepo['data']['resultList'];
    	}else{
    		return '';
    	}
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new ResourceFileSearch();
    		$attach= new AttachSearch();
    		$rhttp = new RestfulHttp('/resource/file/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$attach->load($t,'attach');
    		$model->load($result,'result');
    		$model->attach=$attach;
    		
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function update(){
    	Yii::info("update id:".$this->id);
    	$rhttp = new RestfulHttp('/resource/file/'.$this->id);
    	//ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
		return true;
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/resource/file');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		$this->baseId = $data['data']['result']['baseId'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    /**
     * 发布主课程
     * @return boolean
     */
    public function publish(){
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'publish']);
    	
//     	if($data['success']=='true'){
//     		return true;
//     	}else{
//     		return false;
//     	}
    	
    	return true;
    }
    
    public function depublish(){
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'depublish']);
    	    	
    	return true;
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/resource/file/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function checkOrder()
    {
    	$rhttp = new RestfulHttp('/resource/file_order');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		return true;
    	} else {
    		return false;
    	}
    }
    
    
    public function attributeLabels()
    {
    	$attributeLabels = parent::attributeLabels();
    	return array_merge($attributeLabels,[
                'teacherId' => '讲师',
    			'publish_time_start' => '发布时间',
    			'publish_time_end' => '至',
                'cover' => '封面'
    	]);
    }
    
}
