<?php

namespace app\models;

use common\models\CommonModel;
/**
 * This is the model class for table "pgdx_reply".
 *
 * @property int $id
 * @property string $createTime 发表时间
 * @property string $updateTime
 * @property string $replyContent 内容
 * @property int $courseMainId 主课程ID
 * @property int $courseId 子课程ID
 * @property int $userId 用户ID
 * @property string $replyStatus 0(没有回复)1(有回复)2(已回答)
 * @property string $replyType q:提问 a:回答 r:回复,所有reply只针对大课
 * @property int $refId 如果type是回答,需要指明refId为提问的ID；如果type是回复,需要指明refId为提问/回答/回复的ID
 * @property string $isdel 是否已删除 0:未删除 1:已删除
 */
class Reply extends CommonModel
{
	public $id;
	public $createTime;			// 发表时间
	public $updateTime;
	public $replyContent;		// 内容
	public $courseMainId;		// 主课程ID
	public $courseId;			// 子课程ID
	public $userId;				// 用户ID
	public $replyStatus;			// 0(没有回复)1(有回复)2(已回答)
	public $replyType;			// q:提问 a:回答 r:回复,所有reply只针对大课
	public $refId;				// 如果type是回答,需要指明refId为提问的ID；如果type是回复,需要指明refId为提问/回答/回复的ID
	public $isdel;				// 是否已删除 0:未删除 1:已删除
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_reply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createTime', 'updateTime', 'replyContent', 'userId', 'replyStatus', 'replyType'], 'required'],
            [['createTime', 'updateTime'], 'safe'],
            [['courseMainId', 'courseId', 'userId', 'refId'], 'integer'],
            [['replyContent'], 'string', 'max' => 2048],
            [['replyStatus'], 'string', 'max' => 3],
            [['replyType', 'isdel'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'createTime' => '发表时间',
            'updateTime' => 'Update Time',
            'replyContent' => '内容',
            'courseMainId' => '主课程ID',
            'courseId' => '子课程ID',
            'replyStatus' => '留言状态',
            'replyType' => 'q:提问 a:回答 r:回复,所有reply只针对大课',
            'refId' => '如果type是回答,需要指明refId为提问的ID；如果type是回复,需要指明refId为提问/回答/回复的ID',
            'isdel' => '是否已删除 0:未删除 1:已删除',
        ];
    }
}
