<?php

namespace app\models;

use Yii;
use yii\base\Model;
use common\models\CommonModel;

/**
 * This is the model class for table "pgdx_course".
 *
 * @property int $id
 * @property string $courseName 小课名称
 * @property resource $courseContent 文稿
 * @property string $createTime
 * @property string $updateTime
 * @property string $courseStatus 状态 0:未发布  1:已发布 冗余pgdx_course_main的status
 * @property string $publishTime 冗余pgdx_course_main的publishTime
 * @property int $attachId 附件ID
 * @property double $attachSize 附件大小，单位M，冗余pgdx_attach的size
 * @property double $duration 时长(音频、视频播放长度)，冗余pgdx_attach的duration
 * @property int $statisticsId 统计数据ID
 * @property string $type  audio(音频),video(视频)
 * @property int $teacherId 讲师ID,冗余pgdx_course_main的teacherId
 * @property string $tags 标签，多个标签逗号(,)分隔
 * @property string $isdel 是否已删除 0:未删除 1:已删除
 */
class Course extends CommonModel
{
	public $id;
	
	public $courseName;
	
	public $createTime;
	
	public $updateTime;
	
	public $courseStatus;
	
	public $publishTime;
	
	public $attachId;
	
	public $attachSize;
	
	public $duration;
	
	public $statisticsId;
	
	public $courseType;
	
	public $teacherId;
	
	public $tags;
	
	public $isdel;
	
	public $courseOrder;
	
	public $courseContent;
	
	public $courseMainId;
	public $demoFlag;
	public $shareTitle; //
	public $shareSummary; //
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_course';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseName', 'courseContent', 'updateTime', 'courseStatus', 'teacherId', 'courseOrder','shareTitle', 'shareSummary'], 'required'],
            [['courseContent', 'demoFlag'], 'string'],
        	[['createTime', 'updateTime', 'publishTime'], 'safe'],
        	[['id', 'attachId', 'statisticsId', 'teacherId','courseMainId', 'courseOrder'], 'integer'],
            [['attachSize', 'duration'], 'number'],
            [['courseName'], 'string', 'max' => 64],
            [['shareTitle'], 'string', 'max' => 32],
            [['shareSummary'], 'string', 'max' => 64],
            [['courseStatus', 'isdel'], 'string', 'max' => 1],
            [['courseType'], 'string', 'max' => 5],
            [['tags'], 'string', 'max' => 2014],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseName' => '小课名称',
            'courseContent' => '文稿',
            'courseOrder' => '小课顺序',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'courseStatus' => '状态 0:未发布  1:已发布 冗余pgdx_course_main的status',
            'publishTime' => '冗余pgdx_course_main的publishTime',
            'attachId' => '附件ID',
            'attachSize' => '附件大小，单位M，冗余pgdx_attach的size',
            'duration' => '时长(音频、视频播放长度)，冗余pgdx_attach的duration',
            'statisticsId' => '统计数据ID',
            'type' => ' audio(音频),video(视频)',
            'teacherId' => '讲师ID,冗余pgdx_course_main的teacherId',
            'tags' => '标签，多个标签逗号(,)分隔',
            'isdel' => '是否已删除 0:未删除 1:已删除',
            'demoFlag' => '是否试听',
            'shareTitle' => '转发标题',
            'shareSummary' => '转发摘要',
        ];
    }

    /**
     * @inheritdoc
     * @return CourseQuery the active query used by this AR class.
     */
    public static function find()
    {
        //return new CourseQuery(get_called_class());
    }
}
