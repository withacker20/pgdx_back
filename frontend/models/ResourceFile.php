<?php

namespace app\models;

use common\models\CommonModel;

class ResourceFile extends CommonModel
{
	public $id;
	public $createTime;
	public $updateTime;
	public $attachId;
	public $baseId;
	public $isdel;
	public $fileOrder;
	public $attach;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_resource_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['baseId','attachId', 'fileOrder'], 'required'],
            [['baseId','attachId', 'fileOrder'], 'integer'],
        	[['id', 'createTime', 'updateTime','isdel','attach'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'baseName' => '资料库名称',
            'attachId' => '资料库附件',
            'statusType' => '发布状态',
            'fileOrder' => '顺序号',
        ];
    }
}
