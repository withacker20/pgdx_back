<?php
namespace frontend\models\course;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class courseUploadForm extends Model
{
	/**
	 * @var UploadedFile file attribute
	 */
	public $file;
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
				[['file'], 'file', 'extensions' => 'jpg, png, txt', 'mimeTypes' => 'image/jpeg, image/png, text/plain',],
		];
	}
}