<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ResourceBase;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ResourceBaseSearch represents the model behind the search form of `app\models\ResourceBase`.
 */
class ResourceBaseSearch extends ResourceBase
{
    public $attach;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
    	return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $page)
    {
        $rhttp = new RestfulHttp('/resource/base');
        if(array_key_exists('ResourceBaseSearch', $params)){
            //清空值为空的参数
            ArrayHelper::removeValue($params['ResourceBaseSearch'],'');
            $rrepo = $rhttp->get(array_merge($params['ResourceBaseSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));   
        }else{
        	$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
        }
        
        $page->totalCount=$rrepo['data']['total'];

        $dataProvider= new ArrayDataProvider([
                'allModels' => $rrepo['data']['resultList'],
                'sort' => ['attributes' => ['id'],],
        ]);
    	
        return $dataProvider;
    }
    
    /**
     * 查询已发布的主课程信息列表
     * 
     */
    public static function findPublishedModel(){
    	$rhttp = new RestfulHttp('/course/main');
    	
    	$rrepo = $rhttp->get(['ResourceBaseStatus'=>'1','pagination'=>false]);
    	
    	if($rrepo['success']=='true'){
    		return $rrepo['data']['resultList'];
    	}else{
    		return '';
    	}
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new ResourceBaseSearch();
    		$attach= new AttachSearch();
    		$rhttp = new RestfulHttp('/resource/base/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$attach->load($t,'cover');
    		$model->load($result,'result');
    		$model->attach=$attach;
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function update(){
    	Yii::info("update id:".$this->id);
    	$rhttp = new RestfulHttp('/resource/base/'.$this->id);
    	//ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
		return true;
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/resource/base');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    /**
     * 发布主课程
     * @return boolean
     */
    public function publish(){
    	$rhttp = new RestfulHttp('/resource/base/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'publish']);
    	
    	return true;
    }
    
    public function depublish(){
    	$rhttp = new RestfulHttp('/resource/base/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'depublish']);
    	    	
    	return true;
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/resource/base/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    
    public function attributeLabels()
    {
    	$attributeLabels = parent::attributeLabels();
    	return array_merge($attributeLabels,[
                'teacherId' => '讲师',
    			'publish_time_start' => '发布时间',
    			'publish_time_end' => '至',
                'cover' => '封面'
    	]);
    }
    
}
