<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 查询讲师列表
     * @return 
     */
    public function searchProduct(){
        //查询讲师列表
        $rhttp = new RestfulHttp('/product');
        $rrepo = $rhttp->get(['pagination'=>false]);
        $list  = $rrepo['data']['resultList'];
        return $list;
    }
    
    public function searchProductById($id){
    	//查询讲师列表
    	$rhttp = new RestfulHttp('/product/'.$id);
    	$rrepo = $rhttp->get();
    	if($rrepo['success']){
    		return $rrepo['data']['result'];
    	}else{
    		return null;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/product');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function enableProduct(){
    	$rhttp = new RestfulHttp('/Product/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isProduct'=>'1','pgopr'=>'product']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function disableProduct(){
    	$rhttp = new RestfulHttp('/product/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isProduct'=>'0','pgopr'=>'product']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/product/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/product');
    	if(array_key_exists('ProductSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['ProductSearch'],'');
    		if (array_key_exists('createTimeStart', $params['ProductSearch'])) {
	    		$params['ProductSearch']['createTimeStart'] = strtotime($params['ProductSearch']['createTimeStart']) * 1000;
    		}
    		if (array_key_exists('createTimeEnd', $params['ProductSearch'])) {
	    		$params['ProductSearch']['createTimeEnd'] = strtotime($params['ProductSearch']['createTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		$rrepo = $rhttp->get(array_merge($params['ProductSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new ProductSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/product/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		$avatar->load($t,'avatar');
    		$model->avatar=$avatar;
    		
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/product/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
}
