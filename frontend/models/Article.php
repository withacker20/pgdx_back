<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Article extends CommonModel
{
	public $id;
	public $title;
	public $pubtimeStart;
	public $pubtimeEnd;
	public $cid;
	
	public function rules()
	{
		return [
				[['title', 'pubtimeStart', 'pubtimeEnd','cid'], 'safe'],
		];
	}
		
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => '文章标题',
            'cid' => '栏目',
        ];
    }
}
