<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "pgdx_attach".
 *
 * @property int $id
 * @property string $createTime
 * @property string $updateTime
 * @property string $attachType 附件类型 audio(音频),image(图片),video(视频)
 * @property string $attachPath 文件路径
 * @property string $orgFileName 原始上传的文件名
 * @property string $fileName 服务器上的文件名
 * @property double $attachSize 附件大小，单位M
 * @property double $duration 时长(音频、视频播放长度)
 * @property string $mimeType
 */
class Attach extends Model
{
	public $id;
	public $createTime;
	public $updateTime;
	public $attachType; //附件类型 audio(音频),image(图片),video(视频)
	public $attachPath; //文件路径
	public $orgFileName; //原始上传的文件名
	public $fileName; //服务器上的文件名
	public $attachSize; //附件大小，单位M
	public $duration; //时长(音频、视频播放长度)
	public $mimeType;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_attach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['createTime', 'updateTime', 'attachType', 'attachPath', 'orgFileName', 'fileName', 'attachSize', 'mimeType'], 'required'],
            [['id', 'createTime', 'updateTime'], 'safe'],
            [['attachSize', 'duration'], 'number'],
            [['attachType'], 'string', 'max' => 5],
            [['attachPath'], 'string', 'max' => 1024],
            [['orgFileName', 'fileName'], 'string', 'max' => 512],
            [['mimeType'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'attachType' => '附件类型 audio(音频),image(图片),video(视频)',
            'attachPath' => '文件路径',
            'orgFileName' => '原始上传的文件名',
            'fileName' => '服务器上的文件名',
            'attachSize' => '附件大小，单位M',
            'duration' => '时长(音频、视频播放长度)',
            'mimeType' => 'Mime Type',
        ];
    }
}
