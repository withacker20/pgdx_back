<?php

namespace app\models;

use common\models\CommonModel;

class Promotion extends CommonModel
{
	public $id;
	public $icon;
	public $iconId;
	public $refId;
	public $refType;
	public $refName;
	public $refUrl;
	public $createTime;
	public $updateTime;
	public $urlFlag;
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'pgdx_promotion';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
				[['refName', 'refUrl', 'iconId'], 'required'],
				[['id', 'createTime', 'updateTime', 'iconId', 'refId', 'refUrl', 'refType'], 'safe'],
				['refUrl', 'url', 'defaultScheme' => 'http']
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
				'id' => 'ID',
				'refName' => '落地页',
				'refUrl' => '自定义链接',
				'iconId' => '推广icon',
				'refType' => '推广类型',
		];
	}
}
