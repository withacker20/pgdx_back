<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Teacher;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * TeacherSearch represents the model behind the search form of `app\models\Teacher`.
 */
class TeacherSearch extends Teacher
{
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 查询讲师列表
     * @return 
     */
    public function searchTeacher(){
        //查询讲师列表
        $rhttp = new RestfulHttp('/teacher');
        $rrepo = $rhttp->get(['pagination'=>false]);
        $list  = $rrepo['data']['resultList'];
        return $list;
    }
    
    public function searchTeacherById($id){
    	//查询讲师列表
    	$rhttp = new RestfulHttp('/teacher/'.$id);
    	$rrepo = $rhttp->get();
    	if($rrepo['success']){
    		return $rrepo['data']['result'];
    	}else{
    		return null;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/teacher');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function enableTeacher(){
    	$rhttp = new RestfulHttp('/Teacher/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isTeacher'=>'1','pgopr'=>'teacher']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function disableTeacher(){
    	$rhttp = new RestfulHttp('/teacher/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isTeacher'=>'0','pgopr'=>'teacher']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/teacher/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/teacher');
    	if(array_key_exists('TeacherSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['TeacherSearch'],'');
    		if (array_key_exists('createTimeStart', $params['TeacherSearch'])) {
	    		$params['TeacherSearch']['createTimeStart'] = strtotime($params['TeacherSearch']['createTimeStart']) * 1000;
    		}
    		if (array_key_exists('createTimeEnd', $params['TeacherSearch'])) {
	    		$params['TeacherSearch']['createTimeEnd'] = strtotime($params['TeacherSearch']['createTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		$rrepo = $rhttp->get(array_merge($params['TeacherSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new TeacherSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/teacher/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		$avatar->load($t,'avatar');
    		$model->avatar=$avatar;
    		
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/teacher/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
}
