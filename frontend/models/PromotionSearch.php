<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Promotion;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * PromotionSearch represents the model behind the search form of `app\models\Promotion`.
 */
class PromotionSearch extends Promotion
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return parent::rules();
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	
	public function update()
	{
		Yii::info("update id:" . $this->id);
		$this->createTime = null;
		$this->updateTime = null;
		$rhttp = new RestfulHttp('/promotion/' . $this->id);
		
		$data = $rhttp->put($this);
		
		if ($data['success'] == 'true') {
			$this->id = $data['data']['result']['id'];
			return true;
		} else {
			return false;
		}
		return true;
	}
	
	public static function findModel($id)
	{
		if ($id != null) {
			$model = new PromotionSearch();
			$attach = new AttachSearch();
			$rhttp = new RestfulHttp('/promotion');
			$data = $rhttp->get();
			$result = $data['data'];
			$t = $result['result'];
			$attach->load($t, 'icon');
			$model->load($result, 'result');
			$model->icon = $attach;
			if ($model->refUrl == '') {
				$model->urlFlag = false;
			} else {
				$model->urlFlag = true;
			}
			
			return $model;
		} else {
			return null;
		}
		
	}
}
