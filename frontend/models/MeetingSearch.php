<?php

namespace app\models;

use app\models\Article;
use common\http\RestfulHttp;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * MeetingSearch represents the model behind the search form of `app\models\Article`.
 */
class MeetingSearch extends Meeting
{
	
	public function rules()
	{
		return parent::rules();
	}
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/promotion/meetings');
    	if(array_key_exists('MeetingSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['MeetingSearch'],'');
    		if (array_key_exists('startTimeStart', $params['MeetingSearch'])) {
    			$params['MeetingSearch']['startTimeStart'] = strtotime($params['MeetingSearch']['startTimeStart']);
    		}
    		if (array_key_exists('startTimeEnd', $params['MeetingSearch'])) {
    			$params['MeetingSearch']['startTimeEnd'] = strtotime($params['MeetingSearch']['startTimeEnd'])  + 24 * 60 * 60;
    		}
    		$rrepo = $rhttp->get(array_merge($params['MeetingSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    	]);
    	
    	return $dataProvider;
    }
    
}
