<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "pgdx_user".
 *
 * @property int $id
 * @property string $username 用户名
 * @property string $email 邮箱
 * @property string $password 密码
 * @property string $weiid 微信的ID
 * @property string $weiphoto 微信的头像
 * @property string $weinickname 微信的昵称
 * @property int $weisex 微信的性别
 * @property string $weiprovince 微信的省份
 * @property string $weicity 微信的城市
 * @property string $weicountry 微信的国家
 * @property string $weiprivilege 微信的其他
 * @property string $unionid 微信的联合ID
 * @property string $createTime
 * @property string $updateTime
 * @property string $type 用户类型wei(微信用户)bk(后台管理用户)
 * @property int $isTeacher 是否是讲师 1(是讲师)  0(不是讲师)
 * @property string $realName 真实姓名
 * @property string $introduction 讲师自我介绍
 * @property string $userStatus 0:正常  1:受限用户
 * @property int $statisticsId 统计数据ID
 * @property int $orgId 部门ID
 */
class User extends CommonModel
{
	public $id;
	
	public $username;
	
	public $email;
	
	public $uPassword;
	
	public $weiid;
	
	public $weiphoto;
	
	public $weinickname;
	
	public $weisex;
	
	public $weiprovince;
	
	public $weicity;
	
	public $weicountry;
	
	public $weiprivilege;
	
	public $unionid;
	
	public $createTime;
	
	public $updateTime;
	
	public $type;
	
	public $isTeacher;
	
	public $realName;
	
	public $introduction;
	
	public $userStatus;
	
	public $statisticsId;
	
	public $orgId;
	
	public $isReal;
	
	public $thinkAccess;
	
	public $phoneNumber;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'updateTime', 'type', 'thinkAccess'], 'required'],
        	[['weisex', 'isTeacher', 'statisticsId', 'orgId', 'isReal'], 'integer'],
            [['createTime', 'updateTime', 'phoneNumber'], 'safe'],
            [['username'], 'string', 'max' => 16],
            [['email', 'weiphoto'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 32],
            [['weiid', 'weinickname'], 'string', 'max' => 100],
            [['weiprovince', 'weicity', 'weicountry', 'weiprivilege'], 'string', 'max' => 50],
            [['unionid'], 'string', 'max' => 200],
            [['type', 'userStatus'], 'string', 'max' => 3],
            [['realName'], 'string', 'max' => 30],
            [['introduction'], 'string', 'max' => 2048],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '用户名',
            'email' => '邮箱',
            'uPassword' => '密码',
            'weiid' => '微信的ID',
            'weiphoto' => '微信的头像',
            'weinickname' => '微信的昵称',
            'weisex' => '微信的性别',
            'weiprovince' => '微信的省份',
            'weicity' => '微信的城市',
            'weicountry' => '微信的国家',
            'weiprivilege' => '微信的其他',
            'unionid' => '微信的联合ID',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'type' => '用户类型',
            'isTeacher' => '是否是讲师 1(是讲师)  0(不是讲师)',
            'realName' => '真实姓名',
            'introduction' => '讲师自我介绍',
            'userStatus' => '0:正常  1:受限用户',
            'statisticsId' => '统计数据ID',
            'orgId' => '部门ID',
        	'isReal' => '实名认证状态',
        	'thinkAccess' => '思考权限',
        ];
    }
    
    /**
     * 获取部门详细信息
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization(){
    	return $this->hasOne(Organization::className(), ['id' => 'orgId']);
    }
}
