<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Message extends CommonModel
{
	public $id;
	public $msgTitle;
	public $summary;
	public $msgType;
	public $msgContent;
	public $refUrl;
	public $refId;
	public $refName;
	public $deviceType;
	public $userType;
	public $msgStatus;
	public $sendTime;
	public $createTime;
	public $updateTime;
	public $attachId;
	public $isdel;
	public $createTimeStart;
	public $createTimeEnd;
	public $sendTimeStart;
	public $sendTimeEnd;
	public $arrive;
	public $click;
	public $submitType;
	public $urlFlag;
	public $nameFlag;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_message';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'msgTitle',
								'msgType',
								'msgContent',
								'deviceType', 
								'summary', 
								'userType',
								'sendTime',
								'refName',
								'refUrl',
								
						],
						'required' 
				],
				[ 
						[ 
								'urlFlag',
								'nameFlag',
								'click',
								'arrive',
								'refUrl',
								'refId',
								'createTime',
								'attachId', 
								'updateTime',
								'isdel',
								'createTimeStart',
								'createTimeEnd',
								'sendTimeStart',
								'sendTimeEnd',
								'sendTime',
								'msgStatus',
								'id',
								'submitType',
						],
						'safe' 
				],
				[ 
						[ 
								'msgTitle' 
						],
						'string',
						'max' => 64
				],
				[ 
						[ 
								'summary' 
						],
						'string',
						'max' => 200
				],
				[ 
						[ 
								'msgContent' 
						],
						'string',
						'max' => 1024
				],
				[['sendTime'], 'sendTimeValid', 'skipOnEmpty' => false,'skipOnError' => false],
				['refUrl', 'url', 'defaultScheme' => 'http']
        ];
    }
    
    
    public function sendTimeValid($attribute, $params)
    {
    	if(strtotime($this['sendTime']) < time() + 300){
    		$this->addError($attribute, "定时发送时间只能设置当前时间5分钟以后的时间");
    	}
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msgTitle' => '消息标题',
            'summary' => '摘要',
            'msgType' => '类型',
            'msgContent' => '正文',
            'refUrl' => '自定义链接',
            'refId' => '落地页(课程/文章)',
            'refName' => '落地页(课程/文章/会议)',
            'deviceType' => '接收设备',
            'userType' => '用户类型',
            'msgStatus' => '消息状态',
            'attachId' => '封面图片',
        ];
    }
}
