<?php

namespace app\models;

use app\models\Article;
use common\http\RestfulHttp;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
	
	public function rules()
	{
		return parent::rules();
	}
	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return 
     */
    public function searchCategory(){
        //查询讲师列表
    	$rhttp = new RestfulHttp('/pgrouter/api/article/articletypeall', RestfulHttp::ldapHost);
        $rrepo = $rhttp->get();
        $list  = $rrepo['data'];
        return $list;
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/pgrouter/api/article/get', RestfulHttp::ldapHost);
    	if(array_key_exists('ArticleSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['ArticleSearch'],'');
    		if (array_key_exists('pubtimeStart', $params['ArticleSearch'])) {
    			$params['ArticleSearch']['pubtimeStart'] = strtotime($params['ArticleSearch']['pubtimeStart']);
    		}
    		if (array_key_exists('pubtimeEnd', $params['ArticleSearch'])) {
    			$params['ArticleSearch']['pubtimeEnd'] = strtotime($params['ArticleSearch']['pubtimeEnd'])  + 24 * 60 * 60;
    		}
    		$rrepo = $rhttp->post(array_merge($params['ArticleSearch'],['p'=>$page->getPage()+1]));
    	}else{
    		$rrepo = $rhttp->post(['p'=>$page->getPage()+1, 'cid'=>'23']);
    	}
    	
    	$page->totalCount=$rrepo['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data'],
    	]);
    	
    	return $dataProvider;
    }
    
}
