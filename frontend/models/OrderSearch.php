<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 查询讲师列表
     * @return 
     */
    public function searchOrder(){
        //查询讲师列表
        $rhttp = new RestfulHttp('/order');
        $rrepo = $rhttp->get(['pagination'=>false]);
        $list  = $rrepo['data']['resultList'];
        return $list;
    }
    
    public function searchOrderById($id){
    	//查询讲师列表
    	$rhttp = new RestfulHttp('/order/'.$id);
    	$rrepo = $rhttp->get();
    	if($rrepo['success']){
    		return $rrepo['data']['result'];
    	}else{
    		return null;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/order');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function enableOrder(){
    	$rhttp = new RestfulHttp('/Order/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isOrder'=>'1','pgopr'=>'order']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function disableOrder(){
    	$rhttp = new RestfulHttp('/order/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isOrder'=>'0','pgopr'=>'order']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/order/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/order');
    	if(array_key_exists('OrderSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['OrderSearch'],'');
    		if (array_key_exists('createTimeStart', $params['OrderSearch'])) {
	    		$params['OrderSearch']['createTimeStart'] = strtotime($params['OrderSearch']['createTimeStart']) * 1000;
    		}
    		if (array_key_exists('createTimeEnd', $params['OrderSearch'])) {
	    		$params['OrderSearch']['createTimeEnd'] = strtotime($params['OrderSearch']['createTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		$rrepo = $rhttp->get(array_merge($params['OrderSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	$list  = $rrepo['data']['resultList'];

    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
                'pagination' => false
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new OrderSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/order/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		$avatar->load($t,'avatar');
    		$model->avatar=$avatar;
    		
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/order/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
	}
	
	public function searchForExcel() {
		$rhttp = new RestfulHttp('/order');
		$rrepo = $rhttp->get(['pagination'=>false]);
		return $list  = $rrepo['data']['resultList'];
	}
    
}
