<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserLDAPSearch extends UserLDAP
{
	public $purchaseList;	
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
    	return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchUserLDAP($params, $page){
        $rhttp = new RestfulHttp();
        $cookie = $rhttp->loginLDAP();
        
        $rhttp = new RestfulHttp('/pgrouter/user/customer/list/page', RestfulHttp::ldapHost);
        if(array_key_exists('UserLDAPSearch', $params)){
        	//清空值为空的参数
        	ArrayHelper::removeValue($params['UserLDAPSearch'],'');
        	$rrepo = $rhttp->getWithCookie(array_merge($params['UserLDAPSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]), $cookie);
        }else{
        	$rrepo = $rhttp->getWithCookie(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()], $cookie);
        }
        $list  = $rrepo['data']['resultList'];
        $page->totalCount=$rrepo['data']['total'];
        $dataProvider= new ArrayDataProvider([
        		'allModels' => $list,
        ]);
        
        return $dataProvider;
    }
    
    /**
     * @return
     */
    public function updateRealStatusLDAP($uid, $status){
    	$rhttp = new RestfulHttp();
    	$cookie = $rhttp->loginLDAP();
    	$rhttp = new RestfulHttp('/pgrouter/user/update/realstatus', RestfulHttp::ldapHost);
    	$rrepo = $rhttp->postWithCookie(['uid'=>$uid, 'realstatus'=>$status], $cookie);
    	if($rrepo['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    
    public static function findModel($uid){
    	if($uid!= null){
    		$rhttp = new RestfulHttp();
    		$cookie = $rhttp->loginLDAP();
    		$rhttp = new RestfulHttp('/pgrouter/user/customer/'.$uid, RestfulHttp::ldapHost);
    		$rrepo = $rhttp->getWithCookie([], $cookie);
    		$model= new UserLDAPSearch();
    		$model->load($rrepo,'data');
    		$rhttp = new RestfulHttp('/user/'.$model['pgdxappuserid'].'/purchased');
    		$rrepo = $rhttp->get(['pagination'=>'false']);
    		$dataProvider = new ArrayDataProvider([
    				'allModels' => $rrepo['data']['resultList'],
    		]);
    		$model->purchaseList = $dataProvider;
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
}
