<?php

Namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use app\models\Course;
use common\http\RestfulHttp;
use yii\helpers\ArrayHelper;

/**
 * CourseSearch represents the model behind the search form of `app\models\Course`.
 */
class CourseSearch extends Course
{
	public $courseMain;
	public $attach;
	public $attachFile;
	
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
    	return array_merge(parent::rules(), [
    			[['courseOrder'], 'orderValid', 'skipOnEmpty' => false,'skipOnError' => false],
    	]);
    }

    public function orderValid($attribute, $params)
    {
    	$rhttp = new RestfulHttp('/course/course_order/');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='false'){
    		$this->addError($attribute, "小课顺序已存在,请重新输入");
    	}
    }
    
    public function attributeLabels(){
    	return array_merge(parent::attributeLabels(),[
    			'attachFile'=>'音频文件',
    	]);
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function searchByCourseMainId($courseMainId){
    	$rhttp = new RestfulHttp('/course/main/'.$courseMainId.'/sub');
    	$rrepo = $rhttp->get(['pagination' => false]);
    	
    	$data = [];
    	
    	foreach($rrepo['data']['resultList'] as $course){
    		$courseMainId = $course['courseMainId'];
    		//查询主课程信息
    		$course['courseMain'] = CourseMainSearch::findModel($courseMainId);
    		
    		$data[] = $course;
    	}
    	    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $data,
    			'sort' => ['attributes' => ['id','courseName'],],
    			'pagination' => false,
    	]);
    	
    	return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createTime' => $this->createTime,
            'updateTime' => $this->updateTime,
            'publishTime' => $this->publishTime,
            'attachId' => $this->attachId,
            'attachSize' => $this->attachSize,
            'duration' => $this->duration,
            'statisticsId' => $this->statisticsId,
            'teacherId' => $this->teacherId,
        ]);

        $query->andFilterWhere(['like', 'courseName', $this->courseName])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'courseStatus', $this->courseStatus])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'isdel', $this->isdel]);

        return $dataProvider;
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/course/sub/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/course/sub/');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/course/sub/'.$this->id);
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    	return true;
    }
    
    public function updateContent(){
    	$this->pgopr='updatecontent';
    	$rhttp = new RestfulHttp('/course/sub/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    	return true;
    }
    
    public function uploadAttach(){
    	$this->pgopr='uploadattach';
    	$rhttp = new RestfulHttp('/course/sub/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    	return true;
    }
    
    public function checkOrder()
    {
    	$rhttp = new RestfulHttp('/course/course_order/');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		return true;
    	} else {
    		return false;
    	}
    }
}
