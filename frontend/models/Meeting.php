<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Meeting extends CommonModel
{
	public $id;
	public $startTime;
	public $startTimeStart;
	public $startTimeEnd;
	public $name;
	
	public function rules()
	{
		return [
				[['name', 'startTime', 'startTimeStart','startTimeEnd'], 'safe'],
		];
	}
		
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => '会议标题',
        ];
    }
}
