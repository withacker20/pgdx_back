<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Message;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * MessageSearch represents the model behind the search form of `app\models\Message`.
 */
class MessageSearch extends Message
{
	public $attach;
	
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 查询讲师列表
     * @return 
     */
    public function searchMessage(){
        //查询讲师列表
        $rhttp = new RestfulHttp('/message');
        $rrepo = $rhttp->get(['pagination'=>false]);
        $list  = $rrepo['data']['resultList'];
        return $list;
    }
    
    public function searchMessageById($id){
    	//查询讲师列表
    	$rhttp = new RestfulHttp('/message/'.$id);
    	$rrepo = $rhttp->get();
    	if($rrepo['success']){
    		return $rrepo['data']['result'];
    	}else{
    		return null;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/message');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/message/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/message');
    	if(array_key_exists('MessageSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['MessageSearch'],'');
    		if (array_key_exists('createTimeStart', $params['MessageSearch'])) {
	    		$params['MessageSearch']['createTimeStart'] = strtotime($params['MessageSearch']['createTimeStart']) * 1000;
    		}
    		if (array_key_exists('createTimeEnd', $params['MessageSearch'])) {
	    		$params['MessageSearch']['createTimeEnd'] = strtotime($params['MessageSearch']['createTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		if (array_key_exists('sendTimeStart', $params['MessageSearch'])) {
	    		$params['MessageSearch']['sendTimeStart'] = strtotime($params['MessageSearch']['sendTimeStart']) * 1000;
    		}
    		if (array_key_exists('sendTimeEnd', $params['MessageSearch'])) {
	    		$params['MessageSearch']['sendTimeEnd'] = strtotime($params['MessageSearch']['sendTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		$rrepo = $rhttp->get(array_merge($params['MessageSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new MessageSearch();
    		$attach= new AttachSearch();
    		$rhttp = new RestfulHttp('/message/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		$attach->load($t,'attach');
    		$model->attach=$attach;
    		if ($model->msgType == 'pg') {
    			$model->urlFlag = true;
    			$model->nameFlag = false;
    		} else {
    			if (isset($model->refId)) {
    				$model->urlFlag = false;
    				$model->nameFlag = true;
    			} else {
    				$model->urlFlag = true;
    				$model->nameFlag = false;
    			}
    		}
    		
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/message/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
}
