<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Boarding;
use common\http\RestfulHttp;
use common\page\Page;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * BoardingSearch represents the model behind the search form of `app\models\Boarding`.
 */
class BoardingSearch extends Boarding
{
	public $attach;	
    /**
     * @inheritdoc
     */
    public function rules()
    {
    	return array_merge(parent::rules(), [
    			[['boardingOrder'], 'orderValid', 'skipOnEmpty' => false,'skipOnError' => false],
    	]);
    }
    
    public function orderValid($attribute, $params)
    {
    	$rhttp = new RestfulHttp('/boarding/boarding_order/');
    	$this->createTime = null;
    	$this->updateTime = null;
    	$this->publishTime= null;
    	$this->attach = null;
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='false'){
    		$this->addError($attribute, "轮播顺序已存在,请重新输入");
    	}
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $page)
    {
    	
    	$rhttp = new RestfulHttp('/boarding');
    	if(array_key_exists('BoardingSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['BoardingSearch'],'');
    		$rrepo = $rhttp->get(array_merge($params['BoardingSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['boardingOrder'],],
    	]);
    	
    	return $dataProvider;
    	
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/boarding');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	Yii::info("update id:".$this->id);
    	$this->createTime = null;
    	$this->updateTime = null;
    	$this->publishTime= null;
    	$this->attach = null;
    	$rhttp = new RestfulHttp('/boarding/'.$this->id);
    	//ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    	return true;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new BoardingSearch();
     		$attach= new AttachSearch();
    		$rhttp = new RestfulHttp('/boarding/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$attach->load($t,'attach');
    		$model->load($result,'result');
    		$model->attach=$attach;
    		if ($model->boardingType == 'cm' || $model->boardingType == 'cm1') {
    			$model->refObject = CourseMainSearch::findModel($model->refId);
    		} else {
    			$model->refObject =NewsSearch::findModel($model->refId);
    		}
    		
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/boarding/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function depublish(){
    	$rhttp = new RestfulHttp('/boarding/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'depublish']);
    	
    	return true;
    }
    
    public function publish(){
    	$rhttp = new RestfulHttp('/boarding/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'publish']);
    	
    	return true;
    }
}
