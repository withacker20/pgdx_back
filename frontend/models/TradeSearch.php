<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Trade;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * TradeSearch represents the model behind the search form of `app\models\Trade`.
 */
class TradeSearch extends Trade
{
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 查询讲师列表
     * @return 
     */
    public function searchTrade(){
        //查询讲师列表
        $rhttp = new RestfulHttp('/trade');
        $rrepo = $rhttp->get(['pagination'=>false]);
        $list  = $rrepo['data']['resultList'];
        return $list;
    }
    
    public function searchTradeById($id){
    	//查询讲师列表
    	$rhttp = new RestfulHttp('/trade/'.$id);
    	$rrepo = $rhttp->get();
    	if($rrepo['success']){
    		return $rrepo['data']['result'];
    	}else{
    		return null;
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/trade');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function enableTrade(){
    	$rhttp = new RestfulHttp('/Trade/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isTrade'=>'1','pgopr'=>'trade']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function disableTrade(){
    	$rhttp = new RestfulHttp('/trade/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['isTrade'=>'0','pgopr'=>'trade']);
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	$rhttp = new RestfulHttp('/trade/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }

    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/trade');
    	if(array_key_exists('TradeSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['TradeSearch'],'');
    		if (array_key_exists('tradeTimeStart', $params['TradeSearch'])) {
	    		$params['TradeSearch']['tradeTimeStart'] = strtotime($params['TradeSearch']['tradeTimeStart']) * 1000;
    		}
    		if (array_key_exists('tradeTimeEnd', $params['TradeSearch'])) {
	    		$params['TradeSearch']['tradeTimeEnd'] = strtotime($params['TradeSearch']['tradeTimeEnd']) * 1000 + 24 * 60 * 60 * 1000;
    		}
    		$rrepo = $rhttp->get(array_merge($params['TradeSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	$list  = $rrepo['data']['resultList'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new TradeSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/trade/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		$avatar->load($t,'avatar');
    		$model->avatar=$avatar;
    		
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/trade/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
}
