<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "pgdx_news".
 *
 * @property int $id
 * @property string $newsTitle 新闻标题
 * @property resource $newsContent 文稿
 * @property string $createTime
 * @property string $updateTime
 * @property int $statisticsId 统计数据ID
 * @property string $author 作者姓名
 * @property string $tags 标签，多个标签逗号(,)分隔
 * @property string $isdel 是否已删除 0:未删除 1:已删除
 */
class News extends CommonModel
{
	public $id;
	public $newsTitle;
	public $createTime;
	public $updateTime;
	public $statisticsId;
	public $author;
	public $tags;
	public $isdel;
	public $newsContent;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsTitle', 'newsContent','author'], 'required'],
            [['newsContent'], 'string'],
            [['id','createTime', 'updateTime'], 'safe'],
            [['statisticsId'], 'integer'],
            [['newsTitle'], 'string', 'max' => 64, 'min'=>1],
            [['author'], 'string', 'max' => 50],
            [['tags'], 'string', 'max' => 2014],
            [['isdel'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'newsTitle' => '新闻标题',
            'newsContent' => '图文(690*419)',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'statisticsId' => '统计数据ID',
            'author' => '作者姓名',
            'tags' => '标签，多个标签逗号(,)分隔',
            'isdel' => '是否已删除 0:未删除 1:已删除',
        ];
    }
}
