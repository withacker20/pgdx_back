<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RechargeCode;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * RechargeCodeSearch represents the model behind the search form of `app\models\RechargeCode`.
 */
class RechargeCodeSearch extends RechargeCode
{
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function save(){
    	$rhttp = new RestfulHttp('/rechargecode');
		ArrayHelper::removeValue($this,'');
		$this['expireDate'] = strtotime($this['expireDate']) * 1000 + 24 * 60 * 60 * 1000 - 1;
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function cancel(){
    	$rhttp = new RestfulHttp('/rechargecode/cancel/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->post();
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/rechargecode');
    	if(array_key_exists('RechargeCodeSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['RechargeCodeSearch'],'');
    		$rrepo = $rhttp->get(array_merge($params['RechargeCodeSearch'],
    				['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
	}
	
	public function searchRechargeCodeListByBatchId($id)
    {
    	$rhttp = new RestfulHttp('/rechargecode/download/'.$id);
		$rrepo = $rhttp->get();
    	return $rrepo['data']['resultList'];
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new RechargeCodeSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/rechargecode/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		return $model;
    	}else{
    		return null;
    	}
	}
	
	public static function findModelByCode($code){
    	if($code != null){
    		$model= new RechargeCodeSearch();
    		$avatar= new AttachSearch();
    		$rhttp = new RestfulHttp('/rechargecode/detail/'.$code);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$model->load($result,'result');
    		return $model;
    	}else{
    		return null;
    	}
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/teacher/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
}
