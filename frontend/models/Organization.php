<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pgdx_organization".
 *
 * @property int $id
 * @property string $create_time
 * @property string $update_time
 * @property string $name 组织名称
 * @property string $parent_id 父级组织ID
 * @property int $level 部门级别,根组织为0
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_organization';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time', 'update_time'], 'safe'],
            [['level'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['parent_id'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'name' => '组织名称',
            'parent_id' => '父级组织ID',
            'level' => '部门级别,根组织为0',
        ];
    }
}
