<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Trade extends CommonModel
{
	public $id;
	public $tradeNumber;
	public $tradeTime;
	public $tradeAmount;
	public $tradeStatus;
	public $deviceNumber;
	public $deviceIp;
	public $buyerId;
	public $orderId;
	public $payType;
	public $createTime;
	public $updateTime;
	public $isdel;
	public $tradeTimeStart;
	public $tradeTimeEnd;
	public $buyer;
	public $productName;
	public $phoneNumber;
	public $username;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'tradeNumber',
								'tradeTime',
								'tradeAmount',
								'tradeStatus',
								'deviceNumber',
								'deviceIp',
								'buyerId',
								'orderId',
								'payType',
								'createTime',
								'updateTime',
								'isdel',
								'tradeTimeStart',
								'tradeTimeEnd',
								'buyer',
								'productName',
								'phoneNumber',
								'username',
						],
						'safe' 
				],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        		'tradeNumber' => '交易号',
        		'productName' => '产品名称',
        		'phoneNumber' => '手机号',
        		'username' => '昵称',
        		'payType' => '支付方式',
        ];
    }
}
