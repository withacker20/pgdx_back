<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Order extends CommonModel
{
	public $id;
	public $orderNumber;
	public $orderStatus;
	public $totalAmount;
	public $product;
	public $productId;
	public $productName;
	public $phoneNumber;
	public $username;
	public $userId;
	public $buyer;
	public $createTime;
	public $updateTime;
	public $isdel;
	public $createTimeStart;
	public $createTimeEnd;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'id',
								'orderNumber',
								'orderStatus',
								'totalAmount',
								'product',
								'productId',
								'username',
								'phoneNumber',
								'productName',
								'createTime',
								'updateTime',
								'isdel',
								'createTimeStart',
								'createTimeEnd',
						],
						'safe' 
				],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderNumber' => '订单号',
            'orderStatus' => '状态',
            'totalAmount' => '总价格',
            'username' => '用户昵称',
            'phoneNumber' => '手机号',
            'productName' => '商品名称',
        ];
    }
}
