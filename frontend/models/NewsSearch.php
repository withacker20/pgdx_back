<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * NewsSearch represents the model behind the search form of `app\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return parent::rules();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $page)
    {
    	$rhttp = new RestfulHttp('/news');
    	if(array_key_exists('NewsSearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['NewsSearch'],'');
    		$rrepo = $rhttp->get(array_merge($params['NewsSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','newsTitle'],],
    	]);
    	
    	return $dataProvider;
    	
    }
    
    public static function findPublishedModel(){
    	$rhttp = new RestfulHttp('/news');
    	
    	$rrepo = $rhttp->get(['pagination'=>false]);
    	
    	if($rrepo['success']=='true'){
    		return $rrepo['data']['resultList'];
    	}else{
    		return '';
    	}
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/news');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	var_dump($data);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update(){
    	Yii::info("update id:".$this->id);
    	$this->createTime = null;
    	$this->updateTime = null;
    	$rhttp = new RestfulHttp('/news/'.$this->id);
    	//ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    	return true;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new NewsSearch();
    		//$teacher= new UserSearch();
    		//$attach= new AttachSearch();
    		$rhttp = new RestfulHttp('/news/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
//     		$teacher->load($t,'teacher');
//     		$attach->load($t,'attach');
    		$model->load($result,'result');
//     		$model->teacher=$teacher;
//     		$model->attach=$attach;
    		
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/news/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
}
