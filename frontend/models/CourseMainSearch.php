<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CourseMain;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CourseMainSearch represents the model behind the search form of `app\models\CourseMain`.
 */
class CourseMainSearch extends CourseMain
{
	public $teacherId;
    public $publishTimeStart;
    public $publishTimeEnd;
    public $ppt;
    public $teacher;
    public $attach;
    public $detailAttach;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
        		[['teacherId'], 'required'],
            [['id', 'teacherId', 'coverId'], 'integer'],
        	[['publishTimeStart', 'publishTimeEnd','attach','teacher', 'teacherId','ppt'], 'safe'],
        		[['orderNumber'], 'orderValid','skipOnError' => false],
        ]);
    }
    
    public function orderValid($attribute, $params)
    {
    	$rhttp = new RestfulHttp('/course/course_main_order/');
		ArrayHelper::removeValue($this,'');
		if ($this['courseMainType'] == '1') {
			$this['courseMainDate'] = strtotime($this['courseMainDate']) * 1000;
		}
		$condition = new CourseMainSearch();
		$condition->id = $this['id'];
		$condition->courseMainType = $this['courseMainType'];
		$condition->orderNumber = $this['orderNumber'];
    	$data = $rhttp->post($condition);
    	
    	if($data['success']=='false'){
    		$this->addError($attribute, "顺序已存在,请重新输入");
    	}
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $page)
    {
        $rhttp = new RestfulHttp('/course/main');
        if(array_key_exists('CourseMainSearch', $params)){
            //清空值为空的参数
            ArrayHelper::removeValue($params['CourseMainSearch'],'');
            $rrepo = $rhttp->get(array_merge($params['CourseMainSearch'],['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize(), 'pgopr'=>array_key_exists('pgopr', $params) ? $params['pgopr'] : ""]));   
        }else{
        	$rrepo = $rhttp->get(['pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize(),'pgopr'=>array_key_exists('pgopr', $params) ? $params['pgopr'] : ""]);
        }
        
        $page->totalCount=$rrepo['data']['total'];
        $list  = $rrepo['data']['resultList'];

        $dataProvider= new ArrayDataProvider([
        		'allModels' => $list,
                'sort' => ['attributes' => ['id','name'],],
        ]);
    	
        return $dataProvider;
    }
    
    /**
     * 查询已发布的主课程信息列表
     * 
     */
    public static function findPublishedModel($type='0'){
    	$rhttp = new RestfulHttp('/course/main');
    	
    	$rrepo = $rhttp->get(['courseMainStatus'=>'1','pagination'=>false, 'courseMainType'=>$type]);
    	
    	if($rrepo['success']=='true'){
    		return $rrepo['data']['resultList'];
    	}else{
    		return '';
    	}
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new CourseMainSearch();
    		$teacher= new TeacherSearch();
    		$attach= new AttachSearch();
    		$ppt= new AttachSearch();
    		$detailAttach= new AttachSearch();
    		$rhttp = new RestfulHttp('/course/main/'.$id);
    		$data = $rhttp->get();
    		$result = $data['data'];
    		$t = $result['result'];
    		$teacher->load($t,'teacher');
    		$attach->load($t,'attach');
    		$ppt->load($t,'ppt');
    		$detailAttach->load($t,'detailAttach');
    		$model->load($result,'result');
    		$model->teacher=$teacher;
    		$model->attach=$attach;
    		$model->ppt=$ppt;
    		$model->detailAttach=$detailAttach;
    		
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function update(){
		Yii::info("update id:".$this->id);
		if ($this->courseMainType=='1') {
			$this->courseMainDate = strtotime($this->courseMainDate) * 1000;
		}
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	//ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
		return true;
    }
    
    public function save(){
    	$rhttp = new RestfulHttp('/course/main');
		ArrayHelper::removeValue($this,'');
		$this['courseMainDate'] = strtotime($this['courseMainDate']) * 1000;
    	
    	$data = $rhttp->post($this);
    	    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    /**
     * 发布主课程
     * @return boolean
     */
    public function publish(){
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'publish']);
    	
//     	if($data['success']=='true'){
//     		return true;
//     	}else{
//     		return false;
//     	}
    	
    	return true;
    }
    
    public function depublish(){
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->put(['pgopr'=>'depublish']);
    	    	
    	return true;
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/course/main/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    
    public function attributeLabels()
    {
    	$attributeLabels = parent::attributeLabels();
    	return array_merge($attributeLabels,[
                'teacherId' => '讲师',
    			'publish_time_start' => '发布时间',
    			'publish_time_end' => '至',
                'cover' => '封面'
    	]);
    }
    
    public function searchForMessage($cm, $page)
    {
    	$rhttp = new RestfulHttp('/course/main');
    	$rrepo = $rhttp->get(['pageNum'=>$page,'courseMainName'=>$cm]);
    	$out = ['results' => ['id' => '', 'text' => '']];
    	$list  = $rrepo['data']['resultList'];
    	$out['results'] = array_values($list);
    	$out['total_count'] = $rrepo['data']['total'];
    	return $out;
    }
    
}
