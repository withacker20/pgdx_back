<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Attach;
use common\http\RestfulHttp;

/**
 * AttachSearch represents the model behind the search form of `app\models\Attach`.
 */
class AttachSearch extends Attach
{
	public $attachFullPath;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['attachFullPath'], 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attach::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create_time' => $this->create_time,
            'update_time' => $this->update_time,
            'attach_size' => $this->attach_size,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'attach_type', $this->attach_type])
            ->andFilterWhere(['like', 'attach_path', $this->attach_path])
            ->andFilterWhere(['like', 'org_file_name', $this->org_file_name])
            ->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'mimeType', $this->mimeType]);

        return $dataProvider;
    }
    
    public function findOne($id){
    	$model= new AttachSearch();
    	
    	if($id != null){
    		$rhttp = new RestfulHttp('/attach/'.$id);
    		$data = $rhttp->get();
    	}
    	
    	//var_dump($data);
    	
//     	if($data['success']=='true'){
//     		$model->load($data,'data');
//     		return $model;
//     	}else{
//     		return new NotFoundHttpException('The requested page does not exist.');;
//     	}
    }
}
