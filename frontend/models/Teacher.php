<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Teacher extends CommonModel
{
	public $id;
	public $teacherName;
	public $companyName;
	public $title;
	public $intro;
	public $avatarId;
	public $refId;
	public $createTime;
	public $updateTime;
	public $isdel;
	public $createTimeStart;
	public $createTimeEnd;
	public $avatar;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'teacherName',
								'companyName',
								'title',
								'intro',
								'avatarId' 
						],
						'required' 
				],
				[ 
						[ 
								'id',
								'createTime',
								'updateTime',
								'isdel',
								'createTimeStart',
								'createTimeEnd',
								'avatar' 
						],
						'safe' 
				],
				[ 
						[ 
								'teacherName' 
						],
						'string',
						'max' => 64
				],
				
				[ 
						[ 
								'companyName' 
						],
						'string',
						'max' => 256
				],
				[ 
						[ 
								'title' 
						],
						'string',
						'max' => 14
				],
				[ 
						[ 
								'intro' 
						],
						'string',
						'max' => 17
				],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacherName' => '讲师姓名',
            'companyName' => '公司',
            'title' => '职务',
            'intro' => '介绍',
            'avatarId' => '头像',
            'createTime' => '录入时间',
            'updateTime' => 'Update Time',
        ];
    }
}
