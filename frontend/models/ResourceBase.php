<?php

namespace app\models;

use common\models\CommonModel;

class ResourceBase extends CommonModel
{
	public $id;
	public $createTime;
	public $updateTime;
	public $baseName;
	public $coverId;
	public $isdel;
	public $statusType;
	public $publishTime;
	public $fileAttachIds;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_resource_base';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['baseName', 'coverId'], 'required'],
            [['baseName'], 'string', 'max' => 20],
        	[['id', 'createTime', 'updateTime','isdel','statusType','publishTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'baseName' => '资料库名称',
            'coverId' => '封面',
            'statusType' => '发布状态',
            'fileAttachIds' => '资料库附件',
        ];
    }
}
