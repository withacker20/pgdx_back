<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class UserLDAP extends CommonModel
{
	public $telephoneNumber;
	
	public $unionid;
	
	public $realstatus;
	
	public $sex;
	
	public $cn;
	
	public $birthdayyear;
	
	public $realimgurl;
	
	public $realname;
	
	public $uid;
	
	public $realtype;
	
	public $headimgurl;
	
	public $weinickname;
	
	public $pgdxappuserid;
	
	public $realcompany;
	
	public $pgwuserid;
	
	public $realposition;
	public $createTimeStart;
	public $createTimeEnd;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        		[['telephoneNumber', 'unionid', 'realstatus',
        				'sex','cn','birthdayyear','realimgurl',
        				'realname','uid','realtype','headimgurl',
        				'weinickname','pgdxappuserid','realcompany','pgwuserid','realposition','createTimeStart','createTimeEnd'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cn' => '用户昵称',
            'telephoneNumber' => '手机号',
            'realstatus' => '认证状态',
            'sex' => '性别',
            'birthdayyear' => '出生年份',
            'realimgurl' => '名片/执照',
            'realname' => '真实姓名',
            'realtype' => '公司类型',
            'headimgurl' => '头像',
            'weinickname' => '微信昵称',
            'realcompany' => '公司名称',
            'realposition' => '职位',
        ];
    }
}
