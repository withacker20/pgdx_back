<?php

namespace app\models;

use app\models\User;
use common\models\CommonModel;

/**
 * This is the model class for table "pgdx_course_main".
 *
 * @property int $id
 * @property string $name 课程名称
 * @property string $courseMainDesc 课程介绍
 * @property string $createTime
 * @property string $updateTime
 * @property string $courseMainStatus 状态 0:未发布  1:已发布
 * @property string $publishTime 发布时间
 * @property int $teacher_id 讲师ID
 * @property string $isdel 是否已删除 0:未删除 1:已删除
 * @property int $coverId 封面
 */
class CourseMain extends CommonModel
{
	
	public $id;
	public $courseMainName; //课程名称
	public $courseMainDesc; //课程介绍
	public $createTime;
	public $updateTime;
	public $courseMainStatus; //状态 0:未发布  1:已发布
	public $publishTime; //发布时间
    public $isdel; //是否已删除 0:未删除 1:已删除
    public $coverId; //封面
    public $detailCoverId; //详情页封面
    public $pptId; //课程ppt/视频
    public $amount; //价格
    public $productId; //产品id
    public $replyCount; //留言数量
    public $replyLatestTime; //留言最新时间
    public $shareTimes; //分享次数
    public $orderNumber; //顺序号
    public $shareTitle; //
    public $shareSummary; //
    public $appleProductId; //苹果产品id
    public $courseMainType; //大课类型 0 精品课， 1 大咖说
    public $courseMainRemark; //一句话介绍
    public $courseMainDate; //大咖说日期

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        		[['courseMainName', 'courseMainDesc', 'courseMainStatus','detailCoverId','amount','orderNumber','shareTimes', 'shareTitle', 'shareSummary', 'courseMainDate', 'courseMainType', 'courseMainRemark'], 'required'],
        		[['createTime', 'updateTime', 'publishTime','courseMainName', 'id','appleProductId'], 'safe'],
                [['coverId', 'pptId','productId','orderNumber','shareTimes'], 'integer'],
                [['pptId'], 'pptIdValid', 'skipOnEmpty' => false,'skipOnError' => false],
                [['coverId'], 'coverIdValid', 'skipOnEmpty' => false,'skipOnError' => false],
            [['courseMainName'], 'string', 'max' => 64],
            [['courseMainRemark'], 'string', 'max' => 17],
            [['courseMainDesc'], 'string', 'max' => 1024],
            [['shareTitle'], 'string', 'max' => 32],
            [['shareSummary'], 'string', 'max' => 64],
            [['courseMainStatus', 'isdel'], 'string', 'max' => 1],
        	[['amount'], 'match', 'pattern' => '/(^[1-9](\d+)?(\.\d{1,2})?$)|(^(0){1}$)|(^\d\.\d{1,2}?$)/','message'=>'必须大于等于0且最多两位小数点的金额'],
        	[['amount'], 'compare', 'compareValue' => 9999999999, 'operator' => '<' , 'message'=>'不能大于999999999.99'],
        		[['orderNumber','shareTimes'], 'compare', 'compareValue' => 0, 'operator' => '>=' , 'message'=>'不能小于0'],
        ];
    }

    public function pptIdValid($attribute, $params)
    {
        //当课程类型为大咖说时，pptid 代表视频 不能为空
        if ($this['courseMainType'] == '1' && $this['pptId'] == '') {
            $this->addError($attribute, "视频文件不能为空");
        }
    }

    public function coverIdValid($attribute, $params)
    {
        if ($this['coverId'] == '') {
            if ($this['courseMainType'] == '1') {
                $this->addError($attribute, "首页视频封面不能为空");
            } else {
                $this->addError($attribute, "课程详情页顶部图不能为空");
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseMainName' => '名称',
            'courseMainDesc' => '课程介绍',
            'createTime' => '新增时间',
            'updateTime' => '修改时间',
            'courseMainStatus' => '状态',
            'publishTime' => '发布时间',
            'teacher_id' => '讲师',
            'isdel' => 'Isdel',
            'coverId' => '课程详情页顶部图',
            'detailCoverId' => '详情页视频封面',
            'pptId' => '课程PPT',
            'amount' => '价格(元)',
            'shareTimes' => '免费分享(人数)',
            'orderNumber' => '顺序',
            'shareTitle' => '转发标题',
            'shareSummary' => '转发摘要',
            'courseMainType' => '大课类型',
            'courseMainRemark' => '一句话简介',
            'courseMainDate' => '选择日期',
        ];
    }
}
