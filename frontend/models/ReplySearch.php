<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reply;
use common\http\RestfulHttp;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ReplySearch represents the model behind the search form of `app\models\Reply`.
 */
class ReplySearch extends Reply
{
	public $replyList;
	public $courseMain;
	public $createTimeStart;
	public $createTimeEnd;
	public $user;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'courseMainId', 'courseId', 'userId', 'refId'], 'integer'],
            [['createTime', 'updateTime', 'replyContent', 'replyStatus', 'replyType', 'isdel'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $page,$courseMainId)
    {
    	$rhttp = new RestfulHttp('/course/main/'.$courseMainId.'/reply');
    	if(array_key_exists('ReplySearch', $params)){
    		//清空值为空的参数
    		ArrayHelper::removeValue($params['ReplySearch'],'');
    		$rrepo = $rhttp->get(array_merge($params['ReplySearch'],
    				['pgopr'=>'ref','pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]));
    	}else{
    		$rrepo = $rhttp->get(['pgopr'=>'ref','pageNum'=>$page->getPage()+1,'pageSize'=>$page->getPageSize()]);
    	}
    	
    	$page->totalCount=$rrepo['data']['total'];
    	$dataProvider= new ArrayDataProvider([
    			'allModels' => $rrepo['data']['resultList'],
    			'sort' => ['attributes' => ['id','name'],],
    	]);
    	
    	return $dataProvider;
    }
    
    public static function findModel($id){
    	if($id != null){
    		$model= new ReplySearch();
    		$rhttp1 = new RestfulHttp('/reply/'.$id);//查询单调回复信息
    		$rhttp2 = new RestfulHttp('/reply/'.$id.'/reply');//查询回复下所有的回复列表
    		$data1 = $rhttp1->get();
    		$data2 = $rhttp2->get();
    		$result1 = $data1['data'];
    		$result2 = $data2['data'];
    		$model->replyList=$result2['resultList'];
    		$model->courseMain=$result1['result']['courseMain'];
    		$model->load($result1,'result');
    		return $model;
    	}else{
    		return null;
    	}
    	
    }
    
    public function delete(){
    	$rhttp = new RestfulHttp('/reply/'.$this->id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->delete();
    	
    	if($data['success']=='true'){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function save($questionId){
    	$rhttp = new RestfulHttp('/reply/'.$questionId.'/reply');
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
    
    public function update($id){
    	$rhttp = new RestfulHttp('/reply/'.$id);
    	ArrayHelper::removeValue($this,'');
    	
    	$data = $rhttp->post($this);
    	
    	if($data['success']=='true'){
    		$this->id = $data['data']['result']['id'];
    		return true;
    	}else{
    		return false;
    	}
    }
}
