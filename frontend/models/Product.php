<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class Product extends CommonModel
{
	public $id;
	public $productType;
	public $productName;
	public $amount;
	public $refId;
	public $createTime;
	public $updateTime;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_product';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'productType',
								'refId',
								'productName',
								'amount',
								'createTime',
								'updateTime' 
						],
						'safe' 
				],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productType' => '产品类型',
            'productName' => '产品名称',
            'amount' => '价格',
        ];
    }
}
