<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

class RechargeCode extends CommonModel
{
	public $id;
	public $amount;
	public $expireDate;
	public $batchStatus;
	public $createTime;
	public $updateTime;
	public $codeCount;
	public $verifyCount;
	public $totalAmount;
	public $codeStatus;
	public $verifyTime;
	public $rechargeCode;
	public $username;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
		return [ 
				[ 
						[ 
								'expireDate',
								'amount',
								'codeCount',
						],
						'required' 
				],
				['codeCount', 'integer', 'integerOnly' => true, 'min' => 1],
				[['amount'], 'match', 'pattern' => '/(^[1-9](\d+)?(\.\d{1,2})?$)|(^(0){1}$)|(^\d\.\d{1,2}?$)/','message'=>'必须大于等于0且最多两位小数点的金额'],
				[ 
						[ 
								'id',
								'batchStatus',
								'createTime',
								'updateTime',
								'verifyCount',
								'rechargeCode',
								'codeStatus',
								'verifyTime',
								'username',
						],
						'safe' 
				],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '批次',
            'amount' => '每份面额(元)',
            'expireDate' => '有效期',
            'codeCount' => '总份数',
            'avatarId' => '头像',
            'createTime' => '创建时间',
            'rechargeCode' => '充值码',
        ];
    }
}
