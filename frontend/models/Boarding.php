<?php

namespace app\models;

use Yii;
use common\models\CommonModel;

/**
 * This is the model class for table "pgdx_boarding".
 *
 * @property int $id
 * @property string $boardingName 名称
 * @property int $boardingOrder 排序
 * @property string $createTime
 * @property string $updateTime
 * @property string $boardingStatus 状态 0:未发布  1:已发布
 * @property string $publishTime 发布时间
 * @property int $refId 相关ID
 * @property string $boardingType cm:大课 c:小课 n:新闻
 * @property int $attachId 图片ID
 * @property string $isdel 是否已删除 0:未删除 1:已删除
 */
class Boarding extends CommonModel
{
	public $id;
	public $boardingName;					// 名称
	public $boardingOrder;					// 排序
	public $createTime;
	public $updateTime;
	public $boardingStatus;					// 状态 0:未发布  1:已发布
	public $publishTime;
	public $refId;
	public $refObject;
	public $boardingType;					// cm:大课 c:小课 n:新闻
	public $attachId;						// 图片id
	public $isdel;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pgdx_boarding';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boardingName', 'boardingStatus', 'boardingType','boardingOrder','attachId','refId'], 'required'],
            [['id','boardingOrder', 'refId', 'attachId'], 'integer'],
            [['createTime', 'updateTime', 'publishTime'], 'safe'],
            [['boardingName'], 'string', 'max' => 64],
            [['boardingStatus', 'isdel'], 'string', 'max' => 1],
            [['boardingType'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'boardingName' => '名称',
            'boardingOrder' => '排序',
            'createTime' => 'Create Time',
            'updateTime' => 'Update Time',
            'boardingStatus' => '是否发布',
            'publishTime' => '发布时间',
            'refId' => '关联信息',
            'boardingType' => '轮播类型',
            'attachId' => '封面',
            'isdel' => '是否已删除 0:未删除 1:已删除',
        ];
    }
}
