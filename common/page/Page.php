<?php
namespace common\page;

use yii\data\Pagination;

Class Page{
	public static $pageSize = 10;
	public static function generatePage($param){
		if(isset($param['page'])&&isset($param['per-page'])){
			$page = new Pagination(['page' => $param['page']-1,'pageSize' =>$param['per-page']]);
		}else{
			$page = new Pagination(['page' => 0,'pageSize' => Page::$pageSize]);
		}
		return $page;
	}
}
