<?php 
namespace common\http;

/**
* 
*/
class AESUtil
{
	public function pkcs5_unpad($text)  
    {  
        $pad = ord($text{strlen($text) - 1});  
        if ($pad > strlen($text)) return false;  
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) return false;  
        return substr($text, 0, -1 * $pad);  
	}  
	
	public function decrypt($encryptedText) {
		$key = "my5k8n7g3jd8c7f6";
		$iv = "0102030405060708";
		$str = base64_decode($encryptedText);
		$dec = mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$key,$str,MCRYPT_MODE_CBC,$iv);
		return $this->pkcs5_unpad($dec);
	}
}

?>