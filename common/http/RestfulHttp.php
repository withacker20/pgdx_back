<?php 
namespace common\http;

use yii\httpclient\Client;
use yii\httpclient\Request;
use Yii;
use yii\web\Session;
use common\http\AESUtil;
/**
* 
*/
class RestfulHttp
{
	const ldapHost = "http://127.0.0.1:8081";
	
	private $_baseurl;
	
	private $_uri;
	
	private $_method;
	
	private $_file;
	
	private $_params;
	
	private $_header;
	
	private $_appid = 'app';
	
	private $_pwd = 'QLCJTF8btMhRIKOc';
	
	private $_date;
	
	private $_contextPath = "/pgdx_app";
	
	
	function __construct($uri = '', $baseurl = ''){
		$this->_baseurl = 'http://127.0.0.1:8080/pgdx_app';
		if ($baseurl != '') {
			$this->_baseurl = $baseurl;
		}
		$this->_contextPath = "/pgdx_app";
		$this->_uri = $uri;
	}

	public function get($params=[]){
		$this->_method = 'get';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->request();
	}

	public function getWithCookie($params=[], $cookie){
		$this->_method = 'get';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->requestInCookie($cookie);
	}

	public function postWithCookie($params=[], $cookie){
		$this->_method = 'post';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->requestInCookie($cookie);
	}
	
	public function put($params=[]){
		$this->_method = 'post';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->request();
	}

	public function post($params=[]){
		$this->_method = 'post';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->request();
	}
	
	public function delete($params=[]){
		$this->_method = 'delete';
		$this->_params = $params;
		$this->_date = time() * 1000;
		return $this->request();
	}
	
	public function postFile($file, $params=[]){
		$this->_method = 'post';
		$this->_file = $file;
		$this->_params = $params;
		$this->_date = time() * 1000;
		
		return $this->requestFile();
	}
	
	private function request(){
		if (strpos($this->_uri, "pgrouter")) {
			$sign = md5($this->_appid.strtoupper($this->_method).$this->_uri.$this->_date.$this->_pwd);
		} else {
			$sign = md5($this->_appid.strtoupper($this->_method).$this->_contextPath.$this->_uri.$this->_date.$this->_pwd);
		}
		$client = new Client();
    	$request = $client->createRequest();
    	$url=$this->_baseurl.$this->_uri;
    	$request->setMethod($this->_method)
    	->setHeaders(['pgdate'=>$this->_date, 'pgsign'=>$this->_appid.':'.$sign])
    	->setUrl($url)
    	->setData($this->_params);
    	
		$response = $request->send();
		$data = $response->data;
		$rtnData = null;
		if (isset($data['encrypted']) && $data['encrypted'] == 'true') {
			$aes = new AESUtil();
			$rtnData = $aes->decrypt($data['encryptedData']); 
			return json_decode($rtnData, true);
		} else {
			return $data;
		}

	}
	
	private function requestFile(){
		$sign = md5($this->_appid.strtoupper($this->_method).$this->_contextPath.$this->_uri.$this->_date.$this->_pwd);
		$client = new Client();
		$request = $client->createRequest();
		$url=$this->_baseurl.$this->_uri;
		$request->setMethod($this->_method)
    	->setHeaders(['pgdate'=>$this->_date, 'pgsign'=>$this->_appid.':'.$sign, 'datasource'=>Yii::$app->session['datasource']])
		->setUrl($url)
		->addFile('file', $this->_file['tmp_name'],['mimeType'=>$this->_file['type'],'fileName'=>$this->_file['name']])
		->setData($this->_params);
		
		$response = $request->send();
		$data = $response->data;
		$rtnData = null;
		if (isset($data['encrypted']) && $data['encrypted'] == 'true') {
			$aes = new AESUtil();
			$rtnData = $aes->decrypt($data['encryptedData']); 
			return json_decode($rtnData, true);
		} else {
			return $data;
		}
	}
	
	private function requestInCookie($cookie){
		$sign = md5("app".strtoupper($this->_method).$this->_contextPath.$this->_uri.$this->_date."QLCJTF8btMhRIKOc");
		$client = new Client();
		$request = $client->createRequest();
		$url=RestfulHttp::ldapHost.$this->_uri;
		$request->setMethod($this->_method)
		->setUrl($url)
		->setData($this->_params)
		->setCookies($cookie);
		
		$response = $request->send();
		$data = $response->data;
		$rtnData = null;
		if (isset($data['encrypted']) && $data['encrypted'] == 'true') {
			$aes = new AESUtil();
			$rtnData = $aes->decrypt($data['encryptedData']); 
			return json_decode($rtnData, true);
		} else {
			return $data;
		}
	}
	
	public function loginLDAP() {
		$client = new Client();
		$request = $client->createRequest();
		$url= RestfulHttp::ldapHost.'/pgrouter/login';
		$request->setMethod('post')
		->setUrl($url)
		->setData(['telephoneNumber'=>'100000000001','userPassword'=>'pgdx']);
		$response = $request->send();
		return $response->getCookies();
	}
}

?>
