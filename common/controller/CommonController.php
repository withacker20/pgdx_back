<?php
namespace common\controller;

use yii\web\Controller;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Request;
use common\http\RestfulHttp;
use \GetId3\GetId3Core as GetId3;

Class CommonController extends Controller{
	
	/**
	 * 上传附件(供异步调用)
	 * @return 
	 */
	public function actionUpload()
	{
				
		if (Yii::$app->request->isPost) {
			Yii::$app->response->format = Client::FORMAT_JSON;
			
			
			if (array_key_exists('uploadfile', $_FILES)) {
				
				$upload = $_FILES['uploadfile'];
				$filename = $upload["tmp_name"];
				$md5Value = md5_file($filename);
				$rhttp = new RestfulHttp('/attach/uploadFile');
				$attach = $rhttp->postFile($upload,['md5Value' => $md5Value]);
				
				if($attach['success']=="true"){
					$upload_result = $attach['data']['result'];
					return $upload_result;
				}else{
					return array('error'=> '上传文件失败，请稍后重试.');
				}
				
			}else{
				return array('error'=> '你没有选择上传文件或者重复上传.');
			}
		}else{
			return null;
		}
		
	}
	
	public function actionUploadAudio()
	{
		
		if (Yii::$app->request->isPost) {
			Yii::$app->response->format = Client::FORMAT_JSON;
			
			
			if (array_key_exists('uploadfile', $_FILES)) {
				$upload = $_FILES['uploadfile'];
				$mp3File = $upload['tmp_name'];
				$md5Value = md5_file($mp3File);
				$subfix = "mp3";
				if (isset($upload['name'])) {
					$subfix = explode(".", $upload['name'])[1];
					if (!isset($subfix)) {
						$subfix = "mp3";
					}
				}
				if (strpos($mp3File, ".")) {
					$newFile = substr($mp3File, 0,strlen($mp3File)-3).$subfix;
				} else {
					$newFile = $mp3File.".".$subfix;
				}
				copy($mp3File, $newFile);
				$getId3 = new GetId3();
				$audio = $getId3
				->setOptionMD5Data(true)
				->setOptionMD5DataSource(true)
				->setEncoding('UTF-8')
				->analyze($newFile);
				     unlink($newFile);
				
				$rhttp = new RestfulHttp('/attach/uploadFile');
				if (isset($audio['playtime_seconds'])) {
					     $attach = $rhttp->postFile($upload,['md5Value' => $md5Value, 'duration'=>floor($audio['playtime_seconds'])]);
				} else {
					     $attach = $rhttp->postFile($upload,['md5Value' => $md5Value]);
				}
				
				if($attach['success']=="true"){
					$upload_result = $attach['data']['result'];
					return $upload_result;
				}else{
					return array('error'=> '上传文件失败，请稍后重试.');
				}
				
			}else{
				return array('error'=> '你没有选择上传文件或者重复上传.');
			}
		}else{
			return null;
		}
		
	}

	public function actionCheckMd5() {
		$post = $_POST;
		$rhttp = new RestfulHttp('/attach/checkMd5');
		$response = $rhttp->post(['md5value'=> $post['md5']]);
		return json_encode($response);
	}

	public function actionMergeFile() {
		$post = $_POST;
		$rhttp = new RestfulHttp('/attach/megerFile');
		$attach = $rhttp->post(['md5value'=> $post['md5value'], 'fileName' => $post['fileName']]);
		if($attach['success']=="true"){
			$upload_result = $attach['data']['result'];
			return json_encode($upload_result);
		}else{
			return json_encode(['error'=> '上传文件失败，请稍后重试.']);
		}
	}
}

?>